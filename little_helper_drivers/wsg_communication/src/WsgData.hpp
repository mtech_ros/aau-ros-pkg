/***************************************************************************
 * Software License Agreement (BSD License)                                *
 *                                                                         *
 *  Copyright (c) 2012, Mikkel Hvilsh�j, Christian Car�e, Casper Schou     *
 *	Department of Mechanical and Manufacturing Engineering                 *
 *  Aalborg University, Denmark                                            *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  Redistribution and use in source and binary forms, with or without     *
 *  modification, are permitted provided that the following conditions     *
 *  are met:                                                               *
 *                                                                         *
 *  - Redistributions of source code must retain the above copyright       *
 *     notice, this list of conditions and the following disclaimer.       *
 *  - Redistributions in binary form must reproduce the above              *
 *     copyright notice, this list of conditions and the following         *
 *     disclaimer in the documentation and/or other materials provided     *
 *     with the distribution.                                              *
 *  - Neither the name of Aalborg University nor the names of              *
 *     its contributors may be used to endorse or promote products derived *
 *     from this software without specific prior written permission.       *
 *                                                                         *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS    *
 *  'AS IS' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT      *
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS      *
 *  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE         *
 *  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,    *
 *  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,   *
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;       *
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER       *
 *  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT     *
 *  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN      *
 *  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE        *
 *  POSSIBILITY OF SUCH DAMAGE.                                            *
 ***************************************************************************
 *
 * This header file is a shared file between the action server side and the action client side, that is the "wsg_command_node" and
 * 		whatever user-node communicates with the "wsg_command_node".
 * This file contains shared enumerators for states, commands and errors.
 * This file is implemented only for clarification purposes only, hence to make the code easier to understand.
 */


#ifndef WSGDATA_HPP_
#define WSGDATA_HPP_

namespace wsg_definitions {

enum TStat
{
	E_SUCCESS = 0,
	E_NOT_AVAILABLE,
	E_NO_SENSOR,
	E_NOT_INITIALIZED,
	E_ALREADY_RUNNING,
	E_FEATURE_NOT_SUPPORTED,
	E_INCONSISTENT_DATA,
	E_TIMEOUT,
	E_READ_ERROR,
	E_WRITE_ERROR,
	E_INSUFFICIENT_RESOURCES,
	E_CHECKSUM_ERROR,
	E_NO_PARAM_EXPECTED,
	E_NOT_ENOUGH_PARAMS,
	E_CMD_UNKNOWN,
	E_CMD_FORMAT_ERROR,
	E_ACCESS_DENIED,
	E_ALREADY_OPEN,
	E_CMD_FAILED,
	E_CMD_ABORTED,
	E_INVALID_HANDLE,
	E_NOT_FOUND,
	E_NOT_OPEN,
	E_IO_ERROR,
	E_INVALID_PARAMETER,
	E_INDEX_OUT_OF_BOUNDS,
	E_CMD_PENDING,
	E_OVERRUN,
	E_RANGE_ERROR,
	E_AXIS_BLOCKED,
	E_FILE_EXISTS,
	E_ROS_WRITE_ERROR,
	E_ROS_READ_ERROR
};

enum wsgCommandID {
	INIT_CHECK = 0x04,
	INIT = 0x05,
	LOOP = 0x06,
	DISCONNECT = 0x07,
	HOME = 0x20,
	MOVE = 0x21,
	STOP = 0x22,
	FAST_STOP = 0x23,
	ACK_STOP = 0x24,
	GRASP = 0x25,
	RELEASE = 0x26,
	SET_ACC = 0x30,
	GET_ACC = 0x31,
	SET_FORCE_LIMIT = 0x32,
	GET_FORCE_LIMIT = 0x33,
	SET_LIMITS = 0x34,
	GET_LIMITS = 0x35,
	CLEAR_LIMITS = 0x36,
	OVERDRIVE_ON = 0x37,
	OVERDRIVE_OFF = 0x39, 
	TARE_FORCE = 0x38,
	GET_SYS_STATE = 0x40,
	GET_GRASP_STATE = 0x41,
	GET_GRASP_STATIS = 0x42,
	RESET_GRASP_STATIS = 0x46,
	GET_WIDTH = 0x43,
	GET_SPEED = 0x44,
	GET_FORCE = 0x45
};

enum graspState {
	IDLE = 0,
	GRASPING,
	NO_PART_FOUND,
	PART_LOST,
	HOLDING,
	RELEASING,
	POSITIONING,
	UNDEFINED
};

} //End namespace wsg_definitions


#endif /* WSGDATA_HPP_ */
