/***************************************************************************
 * Software License Agreement (BSD License)                                *
 *                                                                         *
 *  Copyright (c) 2012, Mikkel Hvilshoj, Christian Caroe, Casper Schou     *
 *	Department of Mechanical and Manufacturing Engineering                 *
 *  Aalborg University, Denmark                                            *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  Redistribution and use in source and binary forms, with or without     *
 *  modification, are permitted provided that the following conditions     *
 *  are met:                                                               *
 *                                                                         *
 *  - Redistributions of source code must retain the above copyright       *
 *     notice, this list of conditions and the following disclaimer.       *
 *  - Redistributions in binary form must reproduce the above              *
 *     copyright notice, this list of conditions and the following         *
 *     disclaimer in the documentation and/or other materials provided     *
 *     with the distribution.                                              *
 *  - Neither the name of Aalborg University nor the names of              *
 *     its contributors may be used to endorse or promote products derived *
 *     from this software without specific prior written permission.       *
 *                                                                         *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS    *
 *  'AS IS' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT      *
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS      *
 *  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE         *
 *  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,    *
 *  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,   *
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;       *
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER       *
 *  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT     *
 *  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN      *
 *  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE        *
 *  POSSIBILITY OF SUCH DAMAGE.                                            *
 ***************************************************************************
 *
 * This is the header file of the WsgCommands class from the wsg_communication ROS package.
 * The class is used in the wsg_command_node.cpp, which is the main file of the wsg_command_node ROS-node
 * This class contains all the functions for communication with the wsg50 gripper.
 * 		both functions for each command type
 * 		and functions for the building, sending, receiving and decoding packages via an UDP connection.
 *
 * 	The class also includes the necessary code for the UDP connection.
 */


#ifndef WSG50_HPP_
#define WSG50_HPP_

#include "WsgClient/WsgData.hpp"
#include "WsgReceiver.hpp"

#define OUTPORT 1500 					// Port for outgoing messages via UDP - should correspond to the port defined on the gripper
#define IPADDRESS "192.168.0.30" //12		// IP-address of the gripper

#define MSG_NUM_HEADER_BYTES 3			//Defines the number of bytes for the header on the message passed to the gripper
#define MSG_HEADER_BYTE	0xAA			//Defines the data of the header bytes. This is in reference to the WSG-manual

class WsgCommands
{

public:

	//constructor / destructor
	WsgCommands(const ros::NodeHandle& nh);

	~WsgCommands();

	//********* Functions  (gripper commands)*******************

	int InitCheck();												//check if the gripper is online and initialised

	int Init();														//check if the gripper is initialised, if not do initialisation.

	int Loop();														//check communication with gripper

	int Disconnect();												//announce disconnect

	int Home();														//home the gripper (initialise)

	int Move(float position, float speed);							//position fingers

	int Stop();														//stop movements

	int FastStop();													//emergency stop

	int AckStop();													//acknowledge a fast stop

	int Grasp(float width, float speed);							//grasp a part, width i approx. width of part

	int Release(float width, float speed);							//release part, width is the release width

	int SetAcc(float acc);											//set acceleration on gripper

	int GetAcc(float * acc);										//get current acceleration on gripper

	int SetForceLimit(float force);									//set force limit on gripper

	int GetForceLimit(float * force);								//get current force limit on gripper

	int SetLimits(float limit_minus, float limit_plus);				//set soft limits on gripper (work envelope)

	int GetLimits(float * limit_minus, float * limit_plus);			//get current soft limits on gripper

	int ClearLimits();												//clear current soft limits on gripper

	int OverdriveOn();												//activate overdrive mode to allow higher grasping force

	int OverdriveOff();												//deactivate overdrive mode

	int TareForce();												//tare the force sensor

	int GetSysState(float * Bits);									//get the system state. The system state is a 32 bit vector, but will be returned encoded as a float. To decode, use decode_sys_state() in wsg_data.cpp

	int GetGraspState(wsg_definitions::graspState *state);			//get the grasp state. A state acccording to wsg_data.hpp will be returned

	int GetGraspStatis(int * total, int * no_part, int * lost_part); //get grasping statistics.

	int ResetGraspStatis();											//reset the grasping statistics on the gripper

	int GetWidth(float * width);									//get the current opening width (finger position)

	int GetSpeed(float * speed);									//get the current speed

	int GetForce(float * force);									//get the current force

	int AutoWidth(int mode, int frequency);							//start automatic transmission of width

	int AutoSpeed(int mode, int frequency);							//start automatic transmission of speed

	int AutoForce(int mode, int frequency);							//start automatic transmission of force

	int AutoGraspState(int mode, int frequency);					//start automatic transmission of grasp state

	int AutoSysState(int mode, int frequency);						//start automatic transmission of system state

	void StartJSPublisher(double frequency);						//Start publishing on JointStates

	void StopJSPublisher();											//Stop publishing on JointStates

	void PrintError(int error_code);								//print the error code to screen

private:

	//********* Declarations *******************

	ros::NodeHandle n;
	WsgReceiver* myWsgReceiver;
	ProtectedBuffer* myProtectedBuffer;

	int sockfd;														//socket for UDP connection
	struct sockaddr_in in_addr;										//struct to hold information about connection for incoming data
	struct sockaddr_in out_addr;									//struct to hold information about connection for outgoing data

	struct payloadInfo												//Struct to hold the payload and info for a outgoing packet.
	{
		unsigned short length; 										//Length of the message's payload in bytes (0, if the message has no payload)

		unsigned char id; 											//ID of the message

		unsigned char *data; 										//Pointer to the message's payload
	};

	union UStuff
	{
		float   f;
		unsigned char   c[4];
	};

	union UStuff2
	{
		short i;
		unsigned char c[2];
	};

	union UStuff3
	{
		int i;
		unsigned char c[4];
	};

	//********* Functions *******************

	unsigned char* BuildPackage(payloadInfo *msg, unsigned int *size);		//Builds the packet from the payload

	int GetErrorCode(unsigned char *buf_in);								//decodes the error code from a received packet from the wsg

	int SendMessage(payloadInfo * payload);									//sends the packet to the gripper via UDP connection

	int ReceiveMessage(unsigned char * buf_in, int length_in, payloadInfo * payload);	//Receives a packet from the gripper via UDP

	int ExecuteCommand(payloadInfo * payload, unsigned char * buf_in, int length_in);	//Executes a command, uses send_message and receive message

	void DecodeSysState(float sys_state, char * Bits);						//Decode the systems state from a float to an array of 32 bools

};


#endif /* WSG50_HPP_ */
