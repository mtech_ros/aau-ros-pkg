/***************************************************************************
 * Software License Agreement (BSD License)                                *
 *                                                                         *
 *  Copyright (c) 2012, Mikkel Hvilshoj, Christian Caroe, Casper Schou     *
 *	Department of Mechanical and Manufacturing Engineering                 *
 *  Aalborg University, Denmark                                            *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  Redistribution and use in source and binary forms, with or without     *
 *  modification, are permitted provided that the following conditions     *
 *  are met:                                                               *
 *                                                                         *
 *  - Redistributions of source code must retain the above copyright       *
 *     notice, this list of conditions and the following disclaimer.       *
 *  - Redistributions in binary form must reproduce the above              *
 *     copyright notice, this list of conditions and the following         *
 *     disclaimer in the documentation and/or other materials provided     *
 *     with the distribution.                                              *
 *  - Neither the name of Aalborg University nor the names of              *
 *     its contributors may be used to endorse or promote products derived *
 *     from this software without specific prior written permission.       *
 *                                                                         *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS    *
 *  'AS IS' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT      *
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS      *
 *  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE         *
 *  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,    *
 *  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,   *
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;       *
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER       *
 *  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT     *
 *  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN      *
 *  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE        *
 *  POSSIBILITY OF SUCH DAMAGE.                                            *
 ***************************************************************************
 *
 * This file is the main file of the wsg_command_node ROS-node (executable)
 *
 * This file is mainly an action server.
 * Upon receiving a goal from a client, a callback function is called.
 * This function is primarily a case structure switched by the command ID of the goal.
 * Depending on the command ID the corresponding function from the WsgCommands class is executed.

 * The result is returned to the client. The result consists of: a terminal state (error code) and the data returned from the gripper (if any requested)
 *
 * Even though the WsgClient class, which is intended for use on the action client side, will ensure the parameters send to the gripper is within range,
 * 		this is also checked and ensured here (on the server side).
 * 		The reason is, that the WsgClient class is not needed in order to use the action server.
 * 		Example of out of range parameter: Receiving a goal with a speed of 700 mm/s will result in the speed being set to maximum (420 mm/s)
 *
 * The code for the communication with the gripper is found in the WsgCommands class.
 *
 * The name of the action server is "gripper_action"
 */
#include <boost/thread.hpp>
#include "ros/ros.h"
#include <ros/console.h>
#include <tf/tf.h>
#include <iostream>
#include <sstream>
#include <stdio.h>
#include <string.h>
#include <vector>
#include <fstream>
#include <sys/types.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <string>
#include <actionlib/server/simple_action_server.h>
#include <wsg_communication/GripperAction.h>
#include "WsgCommands.hpp"
#include "WsgClient/WsgData.hpp"
#include "LoggerSys.hpp"

using namespace std;

//Object of the WsgCommands class, which contains all the gripper functions
WsgCommands* myWsgCommands;

//Declaration of the action server
actionlib::SimpleActionServer<wsg_communication::GripperAction>* myGripperServer;

//Declaration of mutex. A mutex is used to assure that the data that are published is consistent
boost::mutex mymutex;

//Publishing frequence [hz] of joint state publisher.
double JSfrequency = 20;

//Declaration of variables to be set according to data received from the gripper
float wsg_width, wsg_speed, wsg_acc, wsg_force, wsg_minus, wsg_plus, wsg_sys_state;
int state, total_part, no_part, lost_part;
wsg_definitions::graspState wsg_grasp_state;

void GripperCB(const wsg_communication::GripperGoalConstPtr& goal)
{
	/**
	 * This is the callback function of the action server.
	 * It will be called when receiving a new goal.
	 * It mainly consists of a case structure switching on the command id.
	 * It is from this case structure the gripper functions from the WsgCommands class are called.
	 * Several of the functions contain a check for whether the data received from the action client are within range of the gripper.
	 */
	info(FFL,"**************************************");
	log_msg.str("");
	log_msg << "Received goal from action client. Goal ID: " << goal->goalID;
	info(FFL, log_msg.str());

	wsg_communication::GripperResult myResult;

	log_msg.str("");

	mymutex.lock();

	float width, speed, acc, force, minus, plus;
	int frequency, mode;

	switch (goal->CommandID) {

	case wsg_definitions::INIT_CHECK:
		state = myWsgCommands->InitCheck();
		break;

	case wsg_definitions::INIT:
		state = myWsgCommands->Init();
		break;

	case wsg_definitions::LOOP:
		state = myWsgCommands->Loop();
		break;

	case wsg_definitions::DISCONNECT:
		info(FFL, "Issuing disconnect");
		state = myWsgCommands->Disconnect();
		break;

	case wsg_definitions::HOME:
		info(FFL, "Homing...");
		state = myWsgCommands->Home();
		break;

	case wsg_definitions::MOVE:
		width = goal->width;
		speed = goal->speed;
		if (goal->width < 0) {width = 0; warn(FFL,"Invalid width, interval is 0 - 110. Width set to 0.");};
		if (goal->width > 110) {width = 110; warn(FFL,"Invalid width, interval is 0 - 110. Width set to 110.");};
		if (goal->speed < 5) {speed = 5; warn(FFL,"Invalid speed, interval is 5 - 420. Speed set to 5.");};
		if (goal->speed > 420) {speed = 420; warn(FFL,"Invalid speed, interval is 5 - 420. Speed set to 420.");};
		log_msg << "Moving to width: " << width << " at speed: " << speed << " ...";
		info(FFL, log_msg.str());
		state = myWsgCommands->Move(width, speed);
		break;

	case wsg_definitions::STOP:
		info(FFL, "Setting stop");
		state = myWsgCommands->Stop();
		break;

	case wsg_definitions::FAST_STOP:
		warn(FFL, "Setting fast stop");
		state = myWsgCommands->FastStop();
		break;

	case wsg_definitions::ACK_STOP:
		info(FFL, "Acknowledging stop");
		state = myWsgCommands->AckStop();
		break;

	case wsg_definitions::GRASP:
		width = goal->width;
		speed = goal->speed;
		if (goal->width < 0) {width = 0; warn(FFL,"Invalid width, interval is 0 - 110. Width set to 0.");};
		if (goal->width > 110) {width = 110; warn(FFL,"Invalid width, interval is 0 - 110. Width set to 110.");};
		if (goal->speed < 5) {speed = 5; warn(FFL,"Invalid speed, interval is 5 - 420. Speed set to 5.");};
		if (goal->speed > 420) {speed = 420; warn(FFL,"Invalid speed, interval is 5 - 420. Speed set to 420.");};
		log_msg << "Grasping object of width: " << width << " at speed: " << speed << " ...";
		info(FFL, log_msg.str());
		state = myWsgCommands->Grasp(width, speed);
		break;

	case wsg_definitions::RELEASE:
		width = goal->width;
		speed = goal->speed;
		if (goal->width < 0) {width = 0; warn(FFL, "Invalid width, interval is 0 - 110. Width set to 0.");};
		if (goal->width > 110) {width = 110; warn(FFL,"Invalid width, interval is 0 - 110. Width set to 110.");};
		if (goal->speed < 5) {speed = 5; warn(FFL, "Invalid speed, interval is 5 - 420. Speed set to 5.");};
		if (goal->speed > 420) {speed = 420; warn(FFL, "Invalid speed, interval is 5 - 420. Speed set to 420.");};
		log_msg << "Releasing to width: " << width << " at speed: " << speed << " ...";
		info(FFL, log_msg.str());
		state = myWsgCommands->Release(width, speed);
		break;

	case wsg_definitions::SET_ACC:
		acc = goal->acc;
		if (goal->acc < 100) {acc = 100; warn(FFL, "Invalid acceleration, interval is 100 - 5000. Acceleration set to 100.");};
		if (goal->acc > 5000) {acc = 5000; warn(FFL, "Invalid acceleration, interval is 100 - 5000. Acceleration set to 5000.");};
		log_msg << "Setting acceleration to : " << acc;
		info(FFL, log_msg.str());
		state = myWsgCommands->SetAcc(acc);
		break;

	case wsg_definitions::GET_ACC:
		info(FFL, "Getting acceleration from WSG");
		state = myWsgCommands->GetAcc(&wsg_acc);
		myResult.wsg_acc = wsg_acc;
		break;

	case wsg_definitions::SET_FORCE_LIMIT:
		force = goal->force;
		if (goal->force < 5) {force = 5; warn(FFL, "Invalid force, interval is 5 - 80. Force set to 5.");};
		if (goal->force > 80) {force = 80; warn(FFL, "Invalid force, interval is 5 - 80. Force set to 80.");};
		log_msg << "Setting force limit to : " << force;
		info(FFL, log_msg.str());
		state = myWsgCommands->SetForceLimit(force);
		break;

	case wsg_definitions::GET_FORCE_LIMIT:
		info(FFL, "Getting force limit from WSG");
		state = myWsgCommands->GetForceLimit(&wsg_force);
		myResult.wsg_force = wsg_force;
		break;

	case wsg_definitions::SET_LIMITS:
		minus = goal->limit_minus;
		plus = goal->limit_plus;
		if (goal->limit_minus < 0) {minus = 0; warn(FFL, "Invalid lower limit, interval is 100 - 5000. Lower limit set to 0.");};
		if (goal->limit_minus > 110) {minus = 110; warn(FFL, "Invalid lower limit, interval is 100 - 5000. Lower limit set to 110.");};
		if (goal->limit_plus < 0) {plus = 0; warn(FFL, "Invalid upper limit, interval is 100 - 5000. Upper limit set to 0.");};
		if (goal->limit_plus > 110) {plus = 110; warn(FFL, "Invalid upper limit, interval is 100 - 5000. Upper limit set to 110.");};
		if (plus < minus) warn(FFL, "Inadmissible limits, upper limit smaller than lower limit.");
		log_msg << "Setting soft limits, lower: " << plus << " upper: " << minus;
		info(FFL, log_msg.str());
		state = myWsgCommands->SetLimits(minus, plus);
		break;

	case wsg_definitions::GET_LIMITS:
		info(FFL, "Getting soft limits from WSG");
		state = myWsgCommands->GetLimits(&wsg_minus, &wsg_plus);
		myResult.wsg_minus = wsg_minus;
		myResult.wsg_plus = wsg_plus;
		break;

	case wsg_definitions::CLEAR_LIMITS:
		warn(FFL, "Clearing the soft limits");
		state = myWsgCommands->ClearLimits();
		break;

	case wsg_definitions::OVERDRIVE_ON:
		warn(FFL, "Setting overdrive on");
		state = myWsgCommands->OverdriveOn();
		break;

	case wsg_definitions::OVERDRIVE_OFF:
		info(FFL, "Setting overdrive off");
		state = myWsgCommands->OverdriveOff();
		break;

	case wsg_definitions::TARE_FORCE:
		warn(FFL, "Taring the force sensor");
		state = myWsgCommands->TareForce();
		break;

	case wsg_definitions::GET_SYS_STATE:
		info(FFL, "Getting system state from WSG");
		state = myWsgCommands->GetSysState(&wsg_sys_state);
		myResult.wsg_sys_state = wsg_sys_state;
		break;

	case wsg_definitions::GET_GRASP_STATE:
		info(FFL, "Getting grasp state from WSG");
		state = myWsgCommands->GetGraspState(&wsg_grasp_state);
		myResult.grasp_state = wsg_grasp_state;
		break;

	case wsg_definitions::GET_GRASP_STATIS:
		info(FFL, "Getting grasping statistic from WSG");
		state = myWsgCommands->GetGraspStatis(&total_part, &no_part, &lost_part);
		myResult.total_part = total_part;
		myResult.no_part = no_part;
		myResult.lost_part = lost_part;
		break;

	case wsg_definitions::RESET_GRASP_STATIS:
		warn(FFL, "Resetting grasping statistic");
		state = myWsgCommands->ResetGraspStatis();
		break;

	case wsg_definitions::GET_WIDTH:
		info(FFL, "Getting current width from WSG");
		state = myWsgCommands->GetWidth(&wsg_width);
		myResult.wsg_width = wsg_width;
		break;

	case wsg_definitions::GET_SPEED:
		info(FFL, "Getting current speed from WSG");
		state = myWsgCommands->GetSpeed(&wsg_speed);
		myResult.wsg_speed = wsg_speed;
		break;

	case wsg_definitions::GET_FORCE:
		info(FFL, "Getting current force from WSG");
		state = myWsgCommands->GetForce(&wsg_force);
		myResult.wsg_force = wsg_force;
		break;

	case wsg_definitions::AUTO_WIDTH:
		info(FFL, "Starting automatic transmission of width from WSG");
		frequency = goal->frequency;
		if (goal->frequency < 2) {frequency = 2; warn(FFL, "Inadmissible low frequency - frequency set to 2 hz.");};
		if (goal->frequency > 100) {frequency = 100; warn(FFL, "Inadmissible high frequency - frequency set to 100 hz.");};
		mode = goal->mode;
		if(goal->mode < 0) {mode = 0; warn(FFL, "Invalid mode. Options are '0' for continuous update and '1' for update on change only. Mode set to 0.");};
		if(goal->mode > 1) {mode = 1; warn(FFL, "Invalid mode. Options are '0' for continuous update and '1' for update on change only. Mode set to 1.");};
		log_msg << "Activating automatic transmission of width with frequency: " << frequency << " and with mode: " << goal->mode;
		info(FFL, log_msg.str());
		state = myWsgCommands->AutoWidth(mode, frequency);
		break;

	case wsg_definitions::AUTO_SPEED:
		info(FFL, "Starting automatic transmission of speed from WSG");
		frequency = goal->frequency;
		if (goal->frequency < 2) {frequency = 2; warn(FFL, "Inadmissible low frequency - frequency set to 2 hz.");};
		if (goal->frequency > 100) {frequency = 100; warn(FFL, "Inadmissible high frequency - frequency set to 100 hz.");};
		mode = goal->mode;
		if(goal->mode < 0) {mode = 0; warn(FFL, "Invalid mode. Options are '0' for continuous update and '1' for update on change only. Mode set to 0.");};
		if(goal->mode > 1) {mode = 1; warn(FFL, "Invalid mode. Options are '0' for continuous update and '1' for update on change only. Mode set to 1.");};
		log_msg << "Activating automatic transmission of width with frequency: " << frequency << " and with mode: " << goal->mode;
		info(FFL, log_msg.str());
		state = myWsgCommands->AutoSpeed(mode, frequency);
		break;

	case wsg_definitions::AUTO_FORCE:
		info(FFL, "Starting automatic transmission of force from WSG");
		frequency = goal->frequency;
		if (goal->frequency < 2) {frequency = 2; warn(FFL, "Inadmissible low frequency - frequency set to 2 hz.");};
		if (goal->frequency > 100) {frequency = 100; warn(FFL, "Inadmissible high frequency - frequency set to 100 hz.");};
		mode = goal->mode;
		if(goal->mode < 0) {mode = 0; warn(FFL, "Invalid mode. Options are '0' for continuous update and '1' for update on change only. Mode set to 0.");};
		if(goal->mode > 1) {mode = 1; warn(FFL, "Invalid mode. Options are '0' for continuous update and '1' for update on change only. Mode set to 1.");};
		log_msg << "Activating automatic transmission of width with frequency: " << frequency << " and with mode: " << goal->mode;
		info(FFL, log_msg.str());
		state = myWsgCommands->AutoForce(mode, frequency);
		break;

	case wsg_definitions::AUTO_GRASP_STATE:
		info(FFL, "Starting automatic transmission of grasp state from WSG");
		frequency = goal->frequency;
		if (goal->frequency < 2) {frequency = 2; warn(FFL, "Inadmissible low frequency - frequency set to 2 hz.");};
		if (goal->frequency > 100) {frequency = 100; warn(FFL, "Inadmissible high frequency - frequency set to 100 hz.");};
		mode = goal->mode;
		if(goal->mode < 0) {mode = 0; warn(FFL, "Invalid mode. Options are '0' for continuous update and '1' for update on change only. Mode set to 0.");};
		if(goal->mode > 1) {mode = 1; warn(FFL, "Invalid mode. Options are '0' for continuous update and '1' for update on change only. Mode set to 1.");};
		log_msg << "Activating automatic transmission of width with frequency: " << frequency << " and with mode: " << goal->mode;
		info(FFL, log_msg.str());
		state = myWsgCommands->AutoGraspState(mode, frequency);
		break;

	case wsg_definitions::AUTO_SYS_STATE:
		info(FFL, "Starting automatic transmission of system state from WSG");
		frequency = goal->frequency;
		if (goal->frequency < 2) {frequency = 2; warn(FFL, "Inadmissible low frequency - frequency set to 2 hz.");};
		if (goal->frequency > 100) {frequency = 100; warn(FFL, "Inadmissible high frequency - frequency set to 100 hz.");};
		mode = goal->mode;
		if(goal->mode < 0) {mode = 0; warn(FFL, "Invalid mode. Options are '0' for continuous update and '1' for update on change only. Mode set to 0.");};
		if(goal->mode > 1) {mode = 1; warn(FFL, "Invalid mode. Options are '0' for continuous update and '1' for update on change only. Mode set to 1.");};
		log_msg << "Activating automatic transmission of width with frequency: " << frequency << " and with mode: " << goal->mode;
		info(FFL, log_msg.str());
		state = myWsgCommands->AutoSysState(mode, frequency);
		break;

	case wsg_definitions::START_JS_PUBLISHER:
		info(FFL, "Starting Joint State publisher");
		myWsgCommands->StartJSPublisher(JSfrequency);
		state = 0;
		break;

	case wsg_definitions::STOP_JS_PUBLISHER:
		info(FFL, "Stopping Joint State publisher");
		myWsgCommands->StopJSPublisher();
		state = 0;
		break;

	default:
		log_msg << "Invalid Command ID: " << goal->CommandID;
		error(FFL, log_msg.str());
		myGripperServer->setAborted();								//set goal to aborted
		return;
	}

	myResult.state = state;
	myResult.goalID = goal->goalID;

	mymutex.unlock();

	myGripperServer->setSucceeded(myResult);						//set goal to succeeded and send the result to the action client

	if (state == 0)
	{
		log_msg.str("");
		log_msg << "Execution of goal: " << goal->goalID << " succeeded";
		info(FFL, log_msg.str());
	}
	else
	{
		log_msg.str("");
		log_msg << "Execution of goal: " << goal->goalID << " failed";
		error(FFL, log_msg.str());
		myWsgCommands->PrintError(state);
	}
	info(FFL, "**************************************");
}


int main(int argc, char *argv[])
{

	ros::init(argc, argv, "wsg_commands_node");

	ros::NodeHandle n;

	InitLogger("wsg_communication", true, true, true);

	info(FFL, "wsg_command_node started...");

	//Obejct of WsgCommands class
	myWsgCommands = new WsgCommands(n);

	//Mutex to ensure the consistense of the data.
	boost::mutex::scoped_lock mylock(mymutex, boost::defer_lock); // defer_lock makes it initially unlocked

	//Declare and start the ROS action server
	debug(FFL,"Starting ROS Action Server");
	myGripperServer = new actionlib::SimpleActionServer<wsg_communication::GripperAction>(n, "gripper_action", boost::bind(&GripperCB, _1), false);
	myGripperServer->start();

	//Loop continiously until program is shutdown
	ros::spin();

	ros::waitForShutdown();

}


