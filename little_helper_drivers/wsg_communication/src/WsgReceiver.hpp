/***************************************************************************
 * Software License Agreement (BSD License)                                *
 *                                                                         *
 *  Copyright (c) 2012, Mikkel Hvilshoj, Christian Caroe, Casper Schou     *
 *	Department of Mechanical and Manufacturing Engineering                 *
 *  Aalborg University, Denmark                                            *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  Redistribution and use in source and binary forms, with or without     *
 *  modification, are permitted provided that the following conditions     *
 *  are met:                                                               *
 *                                                                         *
 *  - Redistributions of source code must retain the above copyright       *
 *     notice, this list of conditions and the following disclaimer.       *
 *  - Redistributions in binary form must reproduce the above              *
 *     copyright notice, this list of conditions and the following         *
 *     disclaimer in the documentation and/or other materials provided     *
 *     with the distribution.                                              *
 *  - Neither the name of Aalborg University nor the names of              *
 *     its contributors may be used to endorse or promote products derived *
 *     from this software without specific prior written permission.       *
 *                                                                         *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS    *
 *  'AS IS' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT      *
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS      *
 *  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE         *
 *  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,    *
 *  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,   *
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;       *
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER       *
 *  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT     *
 *  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN      *
 *  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE        *
 *  POSSIBILITY OF SUCH DAMAGE.                                            *
 ***************************************************************************
 */

#ifndef WSGRECEIVER_HPP_
#define WSGRECEIVER_HPP_

#include <boost/thread.hpp>
#include "ros/ros.h"
#include <ros/console.h>

#define INPORT 1501						// Port for incoming messages via UDP - should correspond to the port defined on the gripper

#define MSG_NUM_HEADER_BYTES 3			//Defines the number of bytes for the header on the message passed to the gripper
#define MSG_HEADER_BYTE	0xAA			//Defines the data of the header bytes. This is in reference to the WSG-manual

class ProtectedBuffer
{

public:

	ProtectedBuffer();

	~ProtectedBuffer();

	void set(unsigned char* buffer, int bytes);

	int get(unsigned char*& pointer);


private:

	boost::mutex mymutex;

	unsigned char *_buf;											//Buffer to hold the entire received package
	int _bytes;
};


class WsgReceiver
{

public:

	WsgReceiver(const ros::NodeHandle& nh, ProtectedBuffer* buf);

	~WsgReceiver();

	void run();

	void setBuf(unsigned char* buffer, int bytes);

	void getBuf();

	void setCmdID(int cmdID)
	{
		this->_cmd_ID = cmdID;
	}

	int getCmdID() const
	{
		return _cmd_ID;
	}

	void setLastWidth(float width)
	{
		this->_last_width = width;
	}

	void setLastSpeed(float speed)
	{
		this->_last_speed = speed;
	}

	void setLastForce(float force)
	{
		this->_last_force = force;
	}

	float getLastWidth() const
	{
		return _last_width;
	}

	float getLastSpeed() const
	{
		return _last_speed;
	}

	float getLastForce() const
	{
		return _last_force;
	}

	void startJSPublisher(double frequency);

	void stopJSPublisher();

private:

	void init();

	void ReceiveMessage();

	float decodeFloat(unsigned char* buf_in);

	int decodeGraspState(unsigned char* buf_in);

	void decodeSysState(unsigned char* buf_in, char * Bits);

	void JSPublisher(double frequency);


	int sockfd;														//socket for UDP connection

	struct sockaddr_in in_addr;										//struct to hold information about connection for incoming data

	int _cmd_ID;													//Cmd ID is used to determine if the user has requested a command that is being automatically received (width, force, speed, grasp state or system state)

	float _last_width;
	float _last_speed;
	float _last_force;

	bool doPublishJS;

	ProtectedBuffer* myBuffer;

	boost::thread receiverThread_;
	boost::thread jsPublisherThread_;

	ros::NodeHandle n;

	ros::Publisher pubWsgWidth;
	ros::Publisher pubWsgSpeed;
	ros::Publisher pubWsgForce;
	ros::Publisher pubWsgGraspState;
	ros::Publisher pubWsgSystemState;
	ros::Publisher pubJointState;

	union UStuff
	{
		float   f;
		unsigned char   c[4];
	};

};


#endif //WSGRECEIVER_HPP_
