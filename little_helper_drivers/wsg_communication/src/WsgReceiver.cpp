#include "ros/ros.h"
#include "ros/console.h"
#include "std_msgs/Float32.h"
#include "std_msgs/Int16.h"
#include "std_msgs/MultiArrayLayout.h"
#include "std_msgs/MultiArrayDimension.h"
#include "std_msgs/UInt8MultiArray.h"
#include <sensor_msgs/JointState.h>
#include <iostream>
#include <sstream>
#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <string>
#include <boost/thread.hpp>

#include "WsgClient/WsgData.hpp"
#include "WsgReceiver.hpp"
#include "LoggerSys.hpp"

WsgReceiver::WsgReceiver(const ros::NodeHandle& nh, ProtectedBuffer* buf) : n(nh), myBuffer(buf)
{
	debug(FFL, "Instantiating object of WsgReceiver");

	init();

	receiverThread_ = boost::thread(&WsgReceiver::run, this);
}

WsgReceiver::~WsgReceiver()
{
	stopJSPublisher();					//stop the JSPublisher thread

	receiverThread_.interrupt();		//interrupt the receiver thread
	receiverThread_.join(); 			//wait for it to terminate
}

void WsgReceiver::init()
{
	//Struct for incoming data
	in_addr.sin_family = AF_INET;						//IPv4
	in_addr.sin_port = htons(INPORT);					//Port number
	in_addr.sin_addr.s_addr = INADDR_ANY;				//My IP-address, whatever it is
	memset(in_addr.sin_zero, '\0', sizeof in_addr.sin_zero);

	//Bind to socket for incoming data.
	sockfd = socket(PF_INET, SOCK_DGRAM, IPPROTO_UDP);

	debug(FFL, "Binding to socket");

	bind(sockfd, (struct sockaddr *)&in_addr, sizeof(in_addr));

	info(FFL, "Connecting to gripper...");

	//Set the cmdID to zero initially.
	setCmdID(0);

	//Initialisation and advertising of the publishers
	pubWsgWidth = n.advertise<std_msgs::Float32>("WsgData/Width",100);
	pubWsgSpeed = n.advertise<std_msgs::Float32>("WsgData/Speed",100);
	pubWsgForce = n.advertise<std_msgs::Float32>("WsgData/Force",100);
	pubWsgGraspState = n.advertise<std_msgs::Int16>("WsgData/GraspState",100);
	pubWsgSystemState = n.advertise<std_msgs::UInt8MultiArray>("WsgData/SystemState",100);
	pubJointState = n.advertise<sensor_msgs::JointState>("WsgData/JointState",100);

	_last_width = 0;
	_last_speed = 0;
	_last_force = 0;

}

void WsgReceiver::run()
{
	try
	  {
		while(1)
		{
			ReceiveMessage();
		}
	  }
	  catch (boost::thread_interrupted& interruption)
	  {
	    // thread was interrupted, this is expected.

	  }
	  catch (std::exception& e)
	  {
	    // an unhandled exception reached this point, this constitutes an error

	  }
}

void WsgReceiver::ReceiveMessage()
{
	unsigned char temp_buf[100];

	// Receive packet
	struct sockaddr_storage addr;
	socklen_t fromlen = sizeof(addr);
	int bytes_received;

	bytes_received = recvfrom(sockfd, temp_buf, 100, 0, (struct sockaddr *)&addr, &fromlen);

	//Check if receive succeeded
	if (bytes_received == -1)
	{
		error(FFL, "No data received from gripper - receive timeout");
	}


	//now check if the received package is to be published on a topic or to be passed into protected buff

	if(temp_buf[3] == wsg_definitions::GET_WIDTH)
	{
		if(getCmdID() == wsg_definitions::GET_WIDTH)
		{
			//pass data into protected buffer
			myBuffer->set(temp_buf, bytes_received);
		}

		std_msgs::Float32 temp;
		temp.data = decodeFloat(temp_buf);
		pubWsgWidth.publish(temp);
		setLastWidth(temp.data);
	}
	else if(temp_buf[3] == wsg_definitions::GET_SPEED)
	{
		if(getCmdID() == wsg_definitions::GET_SPEED)
		{
			//pass data into protected buffer
			myBuffer->set(temp_buf, bytes_received);
		}

		std_msgs::Float32 temp;
		temp.data = decodeFloat(temp_buf);
		pubWsgSpeed.publish(temp);
		setLastSpeed(temp.data);
	}
	else if(temp_buf[3] == wsg_definitions::GET_FORCE)
	{
		if(getCmdID() == wsg_definitions::GET_FORCE)
		{
			//pass data into protected buffer
			myBuffer->set(temp_buf, bytes_received);
		}

		std_msgs::Float32 temp;
		temp.data = decodeFloat(temp_buf);
		pubWsgForce.publish(temp);
		setLastForce(temp.data);
	}
	else if(temp_buf[3] == wsg_definitions::GET_GRASP_STATE)
	{
		if(getCmdID() == wsg_definitions::GET_GRASP_STATE)
		{
			//pass data into protected buffer
			myBuffer->set(temp_buf, bytes_received);
		}

		std_msgs::Int16 temp;
		temp.data = decodeGraspState(temp_buf);
		pubWsgGraspState.publish(temp);
	}
	else if(temp_buf[3] == wsg_definitions::GET_SYS_STATE)
	{
		if(getCmdID() == wsg_definitions::GET_SYS_STATE)
		{
			//pass data into protected buffer
			myBuffer->set(temp_buf, bytes_received);
		}

		char * bits;
		bits = (char *) malloc(32*sizeof(char));
		decodeSysState(temp_buf, bits);

		std_msgs::UInt8MultiArray temp;

		for(int i=0;i<32;i++)
		{
			temp.data.push_back(bits[i]);
		}

		pubWsgSystemState.publish(temp);
		free(bits);
	}
	else
	{
		//pass data into protected buffer
		myBuffer->set(temp_buf, bytes_received);
	}

	//Set CmdID to 0 IF the received package is the response to the user-requested command.
	if(getCmdID() == temp_buf[3])		//Only if the message received has the same CmdID as the user has requested will the CmdID-variable be set to zero.
	{
		setCmdID(0);
	}

}

float WsgReceiver::decodeFloat(unsigned char* buf_in)
{
	UStuff a;

	for (int i = 0; i < sizeof(UStuff); ++i)
	{
		a.c[i] = buf_in[i+8];
	}

	float temp = a.f;

	return temp;
}

int WsgReceiver::decodeGraspState(unsigned char* buf_in)
{
	int grasp_state;

	switch(buf_in[8])
	{
	case 0:
		grasp_state = wsg_definitions::IDLE;
		break;

	case 1:
		grasp_state = wsg_definitions::GRASPING;
		break;

	case 2:
		grasp_state = wsg_definitions::NO_PART_FOUND;
		break;

	case 3:
		grasp_state = wsg_definitions::PART_LOST;
		break;

	case 4:
		grasp_state = wsg_definitions::HOLDING;
		break;

	case 5:
		grasp_state = wsg_definitions::RELEASING;
		break;

	case 6:
		grasp_state = wsg_definitions::POSITIONING;
		break;

	default:
		grasp_state = wsg_definitions::UNDEFINED;
		break;

	}

	return grasp_state;
}

void WsgReceiver::decodeSysState(unsigned char* buf_in, char * Bits)
{
	UStuff a;

	for (int i = 8; i < sizeof(UStuff); ++i)
	{
		a.c[i] = buf_in[i];
	}

	unsigned char mask = 1; // Bit mask

	// Extract the bits
	for (int i = 0; i < 8; i++) {
		// Mask each bit in the byte and store it
		Bits[i] = (a.c[0] >> i) & mask;
		Bits[i+8] = (a.c[1] >> i) & mask;
		Bits[i+16] = (a.c[2] >> i) & mask;
		Bits[i+24] = (a.c[3] >> i) & mask;
	}
}

void WsgReceiver::startJSPublisher(double frequency)
{
	doPublishJS = true;
	jsPublisherThread_ = boost::thread(&WsgReceiver::JSPublisher, this, frequency);
}

void WsgReceiver::stopJSPublisher()
{
		doPublishJS = false;				//tell the thread to terminate
		jsPublisherThread_.join();			//wait for the thread to terminate (if not running will proceed)
}

void WsgReceiver::JSPublisher(double frequency)
{
	//this an extra thread that continuous publishes at joint states at a specific rate.

	ros::Rate rate(frequency);

	ros::Time lastMessageTime;

	sensor_msgs::JointState jsmsg = sensor_msgs::JointState();

	//static information in the sensor_msgs::JointState
	jsmsg.header.frame_id = "base_link";
	jsmsg.name.push_back("gripper_joint");

	while(doPublishJS)
	{
		lastMessageTime = ros::Time::now();

		jsmsg.header.stamp = lastMessageTime;

		jsmsg.position.clear();
		jsmsg.velocity.clear();
		jsmsg.effort.clear();

		jsmsg.position.push_back((getLastWidth()/1000)); //in meters
		jsmsg.velocity.push_back((getLastSpeed()/1000)); //in meters per second
		jsmsg.effort.push_back(getLastForce());

		pubJointState.publish(jsmsg);

		//now sleep for the rest of the rate:
		rate.sleep();
	}
}

ProtectedBuffer::ProtectedBuffer()
{
	boost::mutex::scoped_lock mylock(mymutex, boost::defer_lock); 	// defer_lock makes it initially unlocked

	_buf = new unsigned char[50];
	_bytes = 0;
}

ProtectedBuffer::~ProtectedBuffer()
{
	delete _buf;
}

void ProtectedBuffer::set(unsigned char* buffer, int bytes)
{
	mymutex.lock();

	for(int i=0;i<bytes;i++)
	{
		_buf[i] = buffer[i];
	}

	_bytes = bytes;

	mymutex.unlock();

}

int ProtectedBuffer::get(unsigned char*& pointer)
{
	mymutex.lock();

	pointer = new unsigned char[_bytes];

	for(int i=0;i<_bytes;i++)
	{
		pointer[i] = _buf[i];
	}

	int return_bytes = _bytes;

	mymutex.unlock();

	return return_bytes;
}
