This is a short introduction to the wsg-communication package. 
-

	Building the package will generate the wsg_command_node, which is a ROS Action Server and main executable for controlling the wsg50 gripper.

	It will also generate an executable named test_wsg_node, which is a small test/demo program to demonstrate the use of the wsg_command_node
	and a client help class called WsgClient class.

	This WsgClient class is for use on the ROS Action Client side, hence this class is intended for including in a user program that will control the gripper

	The communication to the WSG is based on the WSG Command Set Reference manual by Schunk

	-----HOW TO USE THE WsgClient CLASS-----
	* To use this class for communicating with the "wsg_command_node" and the gripper:
	*  -Include the WsgClient class in user-package (found in src/WsgClient)
	*  -Include the WsgData.hpp in user-package (found in src/WsgClient)
	* 	-Add dependency to user-package.

	-----INITIAL SETUP OF THE GRIPPER-----
	The grippers IP-address is defined in the WsgCommands.hpp file - currently set to 192.168.0.12. 
	
	When first connecting the gripper (before running this ROS node) please access the gripper via the webpage interface and...
	...disable the checksum evaluation
	...set the connection to UDP, ports according to WsgCommands.hpp (standard 1500 and 1501)

	When everything is set up correctly, starting the wsg_command_node should print "Gripper Online", if nothing is printed, the gripper is offline or the setup is incorrect. 



VT4 - AAU
2012
