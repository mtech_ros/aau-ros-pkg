/********************************************************************************************
 * WsgClient.cpp - source of class "WsgClient"												*
 * 																							*
 * Part of the "wsg_communication" ROS-package 												*
 * 																							*
 * Created: Jan 2012 																		*
 * 																							*
 * Author: VT4, Department of Mechanical and Manufacturing engineering, Aalborg University	*
 * 																							*
 * Property of VT4 - AAU. All rights reserved, distribution only by permit by the authors 	*
 * 																							*
 * Beta software - absolutely no guarantees are given										*
 ********************************************************************************************
 *
 * This is the source file of the class "WsgClient"
 *
 * The purpose and use of this class is documented in the corresponding header file.
 */

#include <iostream>
#include <sstream>
#include <stdio.h>
#include <stdlib.h>
#include "WsgClient.hpp"

using namespace std;

WsgClient::WsgClient()
{
	debug(FFL, "::WsgClient - Obejct of WsgClient class created");

	// Action client needed to communicate with the "wsg_command_node" (the action server)
	gripper_object = "none";
	goalID = 0;

	debug(FFL, "Creating ROS Action Client");
	myGripperActionClient = new actionlib::SimpleActionClient<GripperAction>("gripper_action", true);

	debug(FFL, "Waiting for ROS Action Server...");
	myGripperActionClient->waitForServer();
	debug(FFL, "Connected to ROS Action Server");

	//Initialise the autoMode structs to zero
	_auto_width.frequency = 0;
	_auto_speed.frequency = 0;
	_auto_force.frequency = 0;
	_auto_grasp.frequency = 0;
	_auto_sys.frequency = 0;
}

WsgClient::~WsgClient()
{

}

WsgClient* WsgClient::pWsgClient = NULL;

WsgClient* WsgClient::GetInstance()
{
	debug(FFL, "::GetInstance");

	if (pWsgClient== NULL)
	{
		pWsgClient = new WsgClient();
	}

	debug(FFL, "Instance exists - no new created");
	return pWsgClient;
}

//Gripper functions

int WsgClient::InitCheck()
{
	debug(FFL, "::InitCheck");

	//fill in the data of the object
	myGoal.CommandID = wsg_definitions::INIT_CHECK;

	return SendGoal(myGoal);
}

int WsgClient::Init()
{
	debug(FFL, "::Init");

	myGoal.CommandID = wsg_definitions::INIT;

	return SendGoal(myGoal);
}

int WsgClient::Loop()
{
	debug(FFL, "::Loop");

	myGoal.CommandID = wsg_definitions::LOOP;

	return SendGoal(myGoal);
}

int WsgClient::Disconnect()
{
	debug(FFL, "::Disconnect");

	myGoal.CommandID = wsg_definitions::DISCONNECT;

	return SendGoal(myGoal);
}

int WsgClient::Home()
{
	debug(FFL, "::Home");

	myGoal.CommandID = wsg_definitions::HOME;

	return SendGoal(myGoal);
}

int WsgClient::Move(float width, float speed)
{
	log_msg.str("");
	log_msg << "::Move <Width: " << width << " Speed: " << speed << ">";
	debug(FFL, log_msg.str());

	if (width < 0) {width = 0; warn(FFL,"Invalid width, interval is 0 - 110. Width set to 0.");};
	if (width > 110) {width = 110; warn(FFL,"Invalid width, interval is 0 - 110. Width set to 110.");};
	if (speed < 5) {speed = 5; warn(FFL,"Invalid speed, interval is 5 - 420. Speed set to 5.");};
	if (speed > 420) {speed = 420; warn(FFL,"Invalid speed, interval is 5 - 420. Speed set to 420.");};

	myGoal.width = width;
	myGoal.speed = speed;
	myGoal.CommandID = wsg_definitions::MOVE;

	return SendGoal(myGoal);
}

int WsgClient::Stop()
{
	debug(FFL, "::Stop");

	myGoal.CommandID = wsg_definitions::STOP;

	return SendGoal(myGoal);
}

int WsgClient::FastStop()
{
	debug(FFL, "::FastStop");

	myGoal.CommandID = wsg_definitions::FAST_STOP;

	return SendGoal(myGoal);
}

int WsgClient::AckStop()
{
	debug(FFL, "::AckStop");

	myGoal.CommandID = wsg_definitions::ACK_STOP;

	return SendGoal(myGoal);
}

int WsgClient::Grasp(float width, float speed)
{
	log_msg.str("");
	log_msg << "::Grasp <Width: " << width << " Speed: " << speed << ">";
	debug(FFL, log_msg.str());

	if (width < 0) {width = 0; warn(FFL,"Invalid width, interval is 0 - 110. Width set to 0.");};
	if (width > 110) {width = 110; warn(FFL,"Invalid width, interval is 0 - 110. Width set to 110.");};
	if (speed < 5) {speed = 5; warn(FFL,"Invalid speed, interval is 5 - 420. Speed set to 5.");};
	if (speed > 420) {speed = 420; warn(FFL,"Invalid speed, interval is 5 - 420. Speed set to 420.");};

	myGoal.width = width;
	myGoal.speed = speed;
	myGoal.CommandID = wsg_definitions::GRASP;

	return SendGoal(myGoal);
}

int WsgClient::Release(float width, float speed)
{
	log_msg.str("");
	log_msg << "::Release <Width: " << width << " Speed: " << speed << ">";
	debug(FFL, log_msg.str());

	if (width < 0) {width = 0; warn(FFL,"Invalid width, interval is 0 - 110. Width set to 0.");};
	if (width > 110) {width = 110; warn(FFL,"Invalid width, interval is 0 - 110. Width set to 110.");};
	if (speed < 5) {speed = 5; warn(FFL,"Invalid speed, interval is 5 - 420. Speed set to 5.");};
	if (speed > 420) {speed = 420; warn(FFL,"Invalid speed, interval is 5 - 420. Speed set to 420.");};

	myGoal.width = width;
	myGoal.speed = speed;
	myGoal.CommandID = wsg_definitions::RELEASE;

	gripper_object = "none";

	return SendGoal(myGoal);
}

int WsgClient::SetAcc(float acc)
{
	log_msg.str("");
	log_msg << "::SetAcc <Acc: " << acc << ">";
	debug(FFL, log_msg.str());

	//ensuring the data is within range of the gripper
	if (acc < 100) {acc = 100; warn(FFL, "Invalid acceleration, interval is 100 - 5000. Acceleration set to 100.");};
	if (acc > 5000) {acc = 5000; warn(FFL, "Invalid acceleration, interval is 100 - 5000. Acceleration set to 5000.");};

	myGoal.acc = acc;
	myGoal.CommandID = wsg_definitions::SET_ACC;

	return SendGoal(myGoal);
}

int WsgClient::GetAcc(float * acc)
{
	debug(FFL, "::GetAcc");

	myGoal.CommandID = wsg_definitions::GET_ACC;

	int state = SendGoal(myGoal);
	*acc = myGripperActionClient->getResult()->wsg_acc;
	log_msg.str("");
	log_msg << "Acc returned: " << myGripperActionClient->getResult()->wsg_acc;
	debug(FFL, log_msg.str());

	return state;
}

int WsgClient::SetForceLimit(float force)
{
	log_msg.str("");
	log_msg << "::SetForceLimit <Force limit: " << force << ">";
	debug(FFL, log_msg.str());


	if (force < 5) {force = 5; warn(FFL, "Invalid force, interval is 5 - 80. Force set to 5.");};
	if (force > 80) {force = 80; warn(FFL, "Invalid force, interval is 5 - 80. Force set to 80.");};

	myGoal.force = force;
	myGoal.CommandID = wsg_definitions::SET_FORCE_LIMIT;

	return SendGoal(myGoal);
}

int WsgClient::GetForceLimit(float * force_limit)
{
	debug(FFL, "::GetForceLimit");

	myGoal.CommandID = wsg_definitions::GET_FORCE_LIMIT;

	int state = SendGoal(myGoal);
	*force_limit = myGripperActionClient->getResult()->wsg_force;
	log_msg.str("");
	log_msg << "Force returned: " << myGripperActionClient->getResult()->wsg_force;
	debug(FFL, log_msg.str());

	return state;
}

int WsgClient::SetLimits(float minus, float plus)
{
	log_msg.str("");
	log_msg << "::SetForceLimits <Lower: " << minus << " Upper: " << plus << ">";
	debug(FFL, log_msg.str());

	if (minus < 0) {minus = 0; warn(FFL, "Invalid lower limit, interval is 100 - 5000. Lower limit set to 0.");};
	if (minus > 110) {minus = 110; warn(FFL, "Invalid lower limit, interval is 100 - 5000. Lower limit set to 110.");};
	if (plus < 0) {plus = 0; warn(FFL, "Invalid upper limit, interval is 100 - 5000. Upper limit set to 0.");};
	if (plus > 110) {plus = 110; warn(FFL, "Invalid upper limit, interval is 100 - 5000. Upper limit set to 110.");};
	if (plus < minus) warn(FFL, "Inadmissible limits, upper limit smaller than lower limit.");

	myGoal.limit_minus = minus;
	myGoal.limit_plus = plus;
	myGoal.CommandID = wsg_definitions::SET_LIMITS;

	return SendGoal(myGoal);
}

int WsgClient::GetLimits(float * minus, float * plus)
{
	debug(FFL, "::GetLimits");

	myGoal.CommandID = wsg_definitions::GET_LIMITS;

	int state = SendGoal(myGoal);
	*minus = myGripperActionClient->getResult()->wsg_minus;
	*plus = myGripperActionClient->getResult()->wsg_plus;
	log_msg.str("");
	log_msg << "Lower limit returned: " << myGripperActionClient->getResult()->wsg_minus <<
			" Upper limit returned: " << myGripperActionClient->getResult()->wsg_plus;
	debug(FFL, log_msg.str());

	return state;
}

int WsgClient::ClearLimits()
{
	debug(FFL, "::ClearLimits");

	myGoal.CommandID = wsg_definitions::CLEAR_LIMITS;

	return SendGoal(myGoal);
}

int WsgClient::OverdriveOn()
{
	debug(FFL, "::OverdriveOn");

	myGoal.CommandID = wsg_definitions::OVERDRIVE_ON;

	return SendGoal(myGoal);
}

int WsgClient::OverdriveOff()
{
	debug(FFL, "::OverdriveOff");

	myGoal.CommandID = wsg_definitions::OVERDRIVE_OFF;

	return SendGoal(myGoal);
}

int WsgClient::TareForce()
{
	debug(FFL, "::TareForce");

	myGoal.CommandID = wsg_definitions::TARE_FORCE;

	return SendGoal(myGoal);
}

int WsgClient::GetSysState(bool * sys_state_bits)
{
	debug(FFL, "::GetSysState");

	myGoal.CommandID = wsg_definitions::GET_SYS_STATE;

	int state = SendGoal(myGoal);
	float sys_state = myGripperActionClient->getResult()->wsg_sys_state;

	DecodeSysState(sys_state, sys_state_bits);

	if(_auto_sys.frequency > 0)
	{
		debug(FFL, "Re-enabling automatic publishing...");
		AutoSysState(_auto_sys.mode, _auto_sys.frequency);
	}

	return state;
}

int WsgClient::GetGraspState(wsg_definitions::graspState * grasp_state)
{
	debug(FFL, "::GetGraspState");

	myGoal.CommandID = wsg_definitions::GET_GRASP_STATE;

	int state = SendGoal(myGoal);
	int graspst = myGripperActionClient->getResult()->grasp_state;

	/**
	 * The grasp state is returned from the action server as an unsigned int.
	 * Using this case structure to correspond that int-value to the grasp state declared in "WsgData.hpp"
	 * Purpose is simply to clarify the code.
	 */
	switch (graspst)
	{
	case 0:
		*grasp_state = wsg_definitions::IDLE;
		debug(FFL, "GraspState returned: IDLE");
		break;

	case 1:
		*grasp_state = wsg_definitions::GRASPING;
		debug(FFL, "GraspState returned: GRASPING");
		break;

	case 2:
		*grasp_state = wsg_definitions::NO_PART_FOUND;
		debug(FFL, "GraspState returned: NO PART FOUND");
		break;

	case 3:
		*grasp_state = wsg_definitions::PART_LOST;
		debug(FFL, "GraspState returned: PART LOST");
		break;

	case 4:
		*grasp_state = wsg_definitions::HOLDING;
		debug(FFL, "GraspState returned: HOLDING");
		break;

	case 5:
		*grasp_state = wsg_definitions::RELEASING;
		debug(FFL, "GraspState returned: RELEASING");
		break;

	case 6:
		*grasp_state = wsg_definitions::POSITIONING;
		debug(FFL, "GraspState returned: POSITIONING");
		break;

	default:
		state = 1;
		debug(FFL, "GraspState returned: UNKNOWN GRASP STATE");
		break;

	}

	if(_auto_grasp.frequency > 0)
	{
		debug(FFL, "Re-enabling automatic publishing...");
		AutoGraspState(_auto_grasp.mode, _auto_grasp.frequency);
	}

	return state;
}

int WsgClient::GetGraspStatis(int * total, int * no_part, int * lost_part)
{
	debug(FFL, "::GetGraspStatis");

	myGoal.CommandID = wsg_definitions::GET_GRASP_STATIS;

	int state =	SendGoal(myGoal);
	*total = myGripperActionClient->getResult()->total_part;
	*no_part = myGripperActionClient->getResult()->no_part;
	*lost_part = myGripperActionClient->getResult()->lost_part;

	log_msg.str("");
	log_msg << "Total returned: " << myGripperActionClient->getResult()->total_part <<
			" No part returned: " << myGripperActionClient->getResult()->no_part <<
			" Lost part returned: " << myGripperActionClient->getResult()->lost_part;
	debug(FFL, log_msg.str());


	return state;
}

int WsgClient::ResetGraspStatis()
{
	debug(FFL, "::ResetGraspStatis");

	myGoal.CommandID = wsg_definitions::RESET_GRASP_STATIS;

	return SendGoal(myGoal);
}

int WsgClient::GetWidth(float * width)
{
	debug(FFL, "::GetWidth");

	myGoal.CommandID = wsg_definitions::GET_WIDTH;

	int state = SendGoal(myGoal);
	*width = myGripperActionClient->getResult()->wsg_width;
	log_msg.str("");
	log_msg << "Width returned: " << myGripperActionClient->getResult()->wsg_width;
	debug(FFL, log_msg.str());

	if(_auto_width.frequency > 0)
	{
		debug(FFL, "Re-enabling automatic publishing...");
		AutoWidth(_auto_width.mode, _auto_width.frequency);
	}

	return state;
}

int WsgClient::GetSpeed(float * speed)
{
	debug(FFL, "::GetSpeed");

	myGoal.CommandID = wsg_definitions::GET_SPEED;

	int state =	SendGoal(myGoal);
	*speed = myGripperActionClient->getResult()->wsg_speed;
	log_msg.str("");
	log_msg << "Speed returned: " << myGripperActionClient->getResult()->wsg_speed;
	debug(FFL, log_msg.str());

	if(_auto_speed.frequency > 0)
	{
		debug(FFL, "Re-enabling automatic publishing...");
		AutoSpeed(_auto_speed.mode, _auto_speed.frequency);
	}

	return state;
}

int WsgClient::GetForce(float * force)
{
	debug(FFL, "::GetForce");

	myGoal.CommandID = wsg_definitions::GET_FORCE;

	int state =	SendGoal(myGoal);
	*force = myGripperActionClient->getResult()->wsg_force;
	log_msg.str("");
	log_msg << "Force returned: " << myGripperActionClient->getResult()->wsg_force;
	debug(FFL, log_msg.str());

	if(_auto_force.frequency > 0)
	{
		debug(FFL, "Re-enabling automatic publishing...");
		AutoForce(_auto_force.mode, _auto_force.frequency);
	}

	return state;
}

int WsgClient::AutoWidth(int mode, int frequency)
{
	log_msg.str("");
	log_msg << "::AutoWidth <Mode: " << mode << " frequency: " << frequency << ">";
	debug(FFL, log_msg.str());

	if (frequency < 2) {frequency = 2; warn(FFL, "Inadmissible low frequency - frequency set to 2 hz.");};
	if (frequency > 100) {frequency = 100; warn(FFL, "Inadmissible high frequency - frequency set to 100 hz.");};
	if(mode < 0) {mode = 0; warn(FFL, "Invalid mode. Options are '0' for continuous update and '1' for update on change only. Mode set to 0.");};
	if(mode > 1) {mode = 1; warn(FFL, "Invalid mode. Options are '0' for continuous update and '1' for update on change only. Mode set to 1.");};

	myGoal.mode = mode;
	myGoal.frequency = frequency;
	myGoal.CommandID = wsg_definitions::AUTO_WIDTH;

	int state =	SendGoal(myGoal);

	_auto_width.frequency = frequency;
	_auto_width.mode = mode;

	return state;
}

int WsgClient::AutoSpeed(int mode, int frequency)
{
	log_msg.str("");
	log_msg << "::AutoSpeed <Mode: " << mode << " frequency: " << frequency << ">";
	debug(FFL, log_msg.str());

	if (frequency < 2) {frequency = 2; warn(FFL, "Inadmissible low frequency - frequency set to 2 hz.");};
	if (frequency > 100) {frequency = 100; warn(FFL, "Inadmissible high frequency - frequency set to 100 hz.");};
	if(mode < 0) {mode = 0; warn(FFL, "Invalid mode. Options are '0' for continuous update and '1' for update on change only. Mode set to 0.");};
	if(mode > 1) {mode = 1; warn(FFL, "Invalid mode. Options are '0' for continuous update and '1' for update on change only. Mode set to 1.");};

	myGoal.mode = mode;
	myGoal.frequency = frequency;
	myGoal.CommandID = wsg_definitions::AUTO_SPEED;

	int state =	SendGoal(myGoal);

	_auto_speed.frequency = frequency;
	_auto_speed.mode = mode;

	return state;
}

int WsgClient::AutoForce(int mode, int frequency)
{
	log_msg.str("");
	log_msg << "::AutoForce <Mode: " << mode << " frequency: " << frequency << ">";
	debug(FFL, log_msg.str());

	if (frequency < 2) {frequency = 2; warn(FFL, "Inadmissible low frequency - frequency set to 2 hz.");};
	if (frequency > 100) {frequency = 100; warn(FFL, "Inadmissible high frequency - frequency set to 100 hz.");};
	if(mode < 0) {mode = 0; warn(FFL, "Invalid mode. Options are '0' for continuous update and '1' for update on change only. Mode set to 0.");};
	if(mode > 1) {mode = 1; warn(FFL, "Invalid mode. Options are '0' for continuous update and '1' for update on change only. Mode set to 1.");};

	myGoal.mode = mode;
	myGoal.frequency = frequency;
	myGoal.CommandID = wsg_definitions::AUTO_FORCE;

	int state =	SendGoal(myGoal);

	_auto_force.frequency = frequency;
	_auto_force.mode = mode;

	return state;
}

int WsgClient::AutoGraspState(int mode, int frequency)
{
	log_msg.str("");
	log_msg << "::AutoGraspState <Mode: " << mode << " frequency: " << frequency << ">";
	debug(FFL, log_msg.str());

	if (frequency < 2) {frequency = 2; warn(FFL, "Inadmissible low frequency - frequency set to 2 hz.");};
	if (frequency > 100) {frequency = 100; warn(FFL, "Inadmissible high frequency - frequency set to 100 hz.");};
	if(mode < 0) {mode = 0; warn(FFL, "Invalid mode. Options are '0' for continuous update and '1' for update on change only. Mode set to 0.");};
	if(mode > 1) {mode = 1; warn(FFL, "Invalid mode. Options are '0' for continuous update and '1' for update on change only. Mode set to 1.");};

	myGoal.mode = mode;
	myGoal.frequency = frequency;
	myGoal.CommandID = wsg_definitions::AUTO_GRASP_STATE;

	int state =	SendGoal(myGoal);

	_auto_grasp.frequency = frequency;
	_auto_grasp.mode = mode;

	return state;
}

int WsgClient::AutoSysState(int mode, int frequency)
{
	log_msg.str("");
	log_msg << "::AutoSysState <Mode: " << mode << " frequency: " << frequency << ">";
	debug(FFL, log_msg.str());

	if (frequency < 2) {frequency = 2; warn(FFL, "Inadmissible low frequency - frequency set to 2 hz.");};
	if (frequency > 100) {frequency = 100; warn(FFL, "Inadmissible high frequency - frequency set to 100 hz.");};
	if(mode < 0) {mode = 0; warn(FFL, "Invalid mode. Options are '0' for continuous update and '1' for update on change only. Mode set to 0.");};
	if(mode > 1) {mode = 1; warn(FFL, "Invalid mode. Options are '0' for continuous update and '1' for update on change only. Mode set to 1.");};

	myGoal.mode = mode;
	myGoal.frequency = frequency;
	myGoal.CommandID = wsg_definitions::AUTO_SYS_STATE;

	int state =	SendGoal(myGoal);

	_auto_sys.frequency = frequency;
	_auto_sys.mode = mode;

	return state;
}

int WsgClient::CancelAutoWidth()
{
	debug(FFL, "::CancelAutoWidth");

	_auto_width.frequency = 0;

	float temp;
	int state = GetWidth(&temp);

	return state;
}

int WsgClient::CancelAutoSpeed()
{
	debug(FFL, "::CancelAutoSpeed");

	_auto_speed.frequency = 0;

	float temp;
	int state = GetSpeed(&temp);

	return state;
}

int WsgClient::CancelAutoForce()
{
	debug(FFL, "::CancelAutoWidth");

	_auto_force.frequency = 0;

	float temp;
	int state = GetForce(&temp);

	return state;
}

int WsgClient::CancelAutoGraspState()
{
	debug(FFL, "::CancelAutoGraspState");

	_auto_grasp.frequency = 0;

	wsg_definitions::graspState temp;
	int state = GetGraspState(&temp);

	return state;
}

int WsgClient::CancelAutoSysState()
{
	debug(FFL, "::CancelAutoSysState");

	_auto_sys.frequency = 0;

	bool * bits;
	bits = (bool*)malloc(32*sizeof(bool));

	int state = GetSysState(bits);

	free(bits);

	return state;
}

int WsgClient::StartAutoPublishers(int mode, int frequency, bool width, bool speed, bool force, bool graspState, bool sysState)
{
	debug(FFL, "StartPublishers");

	int number_of_publishers = 0;

	if(width) number_of_publishers++;
	if(speed) number_of_publishers++;
	if(force) number_of_publishers++;
	if(graspState) number_of_publishers++;
	if(sysState) number_of_publishers++;

	int possible_frequency;
	if(number_of_publishers == 1) possible_frequency = 100;
	else if(number_of_publishers == 2) possible_frequency = 90;
	else if(number_of_publishers == 3) possible_frequency = 82;
	else if(number_of_publishers == 4) possible_frequency = 75;
	else if(number_of_publishers == 5) possible_frequency = 72;

	if(frequency > possible_frequency)
	{
		log_msg.str("");
		log_msg << "A frequency of " << frequency << " cannot be obtained with " << number_of_publishers << " auto publishers! The maximum frequency obtainable is " << possible_frequency << ", which will be used.";
		warn(FFL, log_msg.str());

		frequency = possible_frequency;
	}

	int state;

	if(width)
	{
		state = AutoWidth(mode, frequency);

		if(state != 0)
		{
			error(FFL, "Failed to start publisher: Width");
			return state;
		}
	}

	if(speed)
	{
		state = AutoSpeed(mode, frequency);

		if(state != 0)
		{
			error(FFL, "Failed to start publisher: Speed");
			return state;
		}
	}

	if(force)
	{
		state = AutoForce(mode, frequency);

		if(state != 0)
		{
			error(FFL, "Failed to start publisher: Force");
			return state;
		}
	}

	if(graspState)
	{
		state = AutoGraspState(mode, frequency);

		if(state != 0)
		{
			error(FFL, "Failed to start publisher: GraspState");
			return state;
		}
	}

	if(sysState)
	{
		state = AutoSysState(mode, frequency);

		if(state != 0)
		{
			error(FFL, "Failed to start publisher: SystemState");
			return state;
		}
	}

	return state;
}

void WsgClient::CancelAutoPublishers()
{
	CancelAutoWidth();

	CancelAutoSpeed();

	CancelAutoForce();

	CancelAutoGraspState();

	CancelAutoSysState();
}

int WsgClient::StartJSPublisher()
{
	debug(FFL, "::StartJSPublisher");

	myGoal.CommandID = wsg_definitions::START_JS_PUBLISHER;

	return SendGoal(myGoal);
}

int WsgClient::StartJSPublisher(int mode, int frequency)
{
	debug(FFL, "::StartJSPublisher - overloaded function");

	int state = StartAutoPublishers(mode, frequency,true,true,true,false,false);

	if(state != 0)
	{
		error(FFL, "Failed to start automatic publishing of width, speed and force");
		return state;
	}

	state = StartJSPublisher();

	return state;
}

int WsgClient::StopJSPublisher()
{
	debug(FFL, "::StopJSPublisher");

	myGoal.CommandID = wsg_definitions::STOP_JS_PUBLISHER;

	return SendGoal(myGoal);
}

//other functions

void WsgClient::PrintError(int error_code)
{
	/**
	 * This function is used to print an error code to the screen
	 */

	log_msg.str("");
	log_msg << "::PrintError <Error code: " << error_code << ">";
	debug(FFL, log_msg.str());

	switch(error_code)
	{
	case 0:
		info(FFL, "Error code 0 : No error occurred, operation was successful.");
		break;
	case 1:
		error(FFL, "Error code 1 : Function or data is not available.");
		break;
	case 2:
		error(FFL, "Error code 2 : No measurement converter is connected.");
		break;
	case 3:
		error(FFL, "Error code 3 : Device was not initialized.");
		break;
	case 4:
		error(FFL, "Error code 4 : The data acquisition is already running.");
		break;
	case 5:
		error(FFL, "Error code 5 : The requested feature is currently not available.");
		break;
	case 6:
		error(FFL, "Error code 6 : One or more parameters are inconsistent.");
		break;
	case 7:
		error(FFL, "Error code 7 : Timeout error.");
		break;
	case 8:
		error(FFL, "Error code 8 : Error while reading data.");
		break;
	case 9:
		error(FFL, "Error code 9 : Error while writing data.");
		break;
	case 10:
		error(FFL, "Error code 10 : No more memory available.");
		break;
	case 11:
		error(FFL, "Error code 11 : Checksum error.");
		break;
	case 12:
		error(FFL, "Error code 12 : A Parameter was given, but none expected.");
		break;
	case 13:
		error(FFL, "Error code 13 : Not enough parameters for executing the command.");
		break;
	case 14:
		error(FFL, "Error code 14 : Unknown command.");
		break;
	case 15:
		error(FFL, "Error code 15 : Command format error.");
		break;
	case 16:
		error(FFL, "Error code 16 : Access denied.");
		break;
	case 17:
		error(FFL, "Error code 17 : Interface is already open.");
		break;
	case 18:
		error(FFL, "Error code 18 : Error while executing a command.");
		break;
	case 19:
		warn(FFL, "Error code 19 : Command execution was aborted by the user.");
		break;
	case 20:
		error(FFL, "Error code 20 : Invalid handle.");
		break;
	case 21:
		error(FFL, "Error code 21 : Device or file not found.");
		break;
	case 22:
		error(FFL, "Error code 22 : Device or file not open.");
		break;
	case 23:
		error(FFL, "Error code 23 : Input/Output Error.");
		break;
	case 24:
		error(FFL, "Error code 24 : Wrong parameter.");
		break;
	case 25:
		error(FFL, "Error code 25 : Index out of bounds.");
		break;
	case 26:
		info(FFL, "Error code 26 : No error, but the command was not completed, yet. Another return message will follow including an error code, if the function was completed.");
		break;
	case 27:
		error(FFL, "Error code 27 : Data overrun.");
		break;
	case 28:
		error(FFL, "Error code 28 : Range error.");
		break;
	case 29:
		error(FFL, "Error code 29 : Axis blocked.");
		break;
	case 30:
		error(FFL, "Error code 30 : File already exists.");
		break;
	case 31:
		error(FFL, "Error code 31 : Error writing to WSG from ROS.");
		break;
	case 32:
		error(FFL, "Error code 32 : Error reading from WSG to ROS.");
		break;

	default:
		log_msg.str("");
		log_msg << "Error code " << error_code << " : Unknown error code.";
		error(FFL, log_msg.str());
		break;
	}
}

void WsgClient::DecodeSysState(float sys_state, bool * Bits)
{
	/**
	 * The system state is returned as a float from the action server. The reason is to save avoid passing a long array
	 * This function decodes that float into an array of 32 bools
	 * Please note, that the function requires a pointer to the beginning of an array declared in the main file.
	 * Please remember to allocate memory for the pointer when it is created, i.e using "<pointer_name> = (bool* <pointer_name>) malloc(32)"
	 */

	debug(FFL, "::DecodeSysState");

	UStuff00 a;

	a.f = sys_state;

	unsigned char mask = 1; // Bit mask

	// Extract the bits
	for (size_t i = 0; i < 8; i++) {
		// Mask each bit in the byte and store it
		Bits[i] = (a.c[0] >> i) & mask;
		Bits[i+8] = (a.c[1] >> i) & mask;
		Bits[i+16] = (a.c[2] >> i) & mask;
		Bits[i+24] = (a.c[3] >> i) & mask;
	}
}

void WsgClient::PrintSysState()
{
	/**
	 * This function is used to print a system state to the screen
	 * Please note, that the system state must be decoded to an array of bools before printing it
	 * Please note, this function will only print the system state flags that are true, hence the errors (for most part)
	 */

	debug(FFL, "::PrintSysState");

	bool * bits;
	bits = (bool*)malloc(32*sizeof(bool));

	int state = GetSysState(bits);

	if (state != 0)
	{
		error(FFL, "Failed to retrieve the system state");
		return;
	}

	log_msg.str("");
	log_msg << "The system state of the gripper: " << endl;
	log_msg << "----------------------------------" << endl;
	if(bits[0] == true) log_msg << "- Fingers referenced" << endl;
	if(bits[1] == true) log_msg << "- The Fingers are currently moving" << endl;
	if(bits[2] == true) log_msg << "- Axis is blocked in negative moving direction" << endl;
	if(bits[3] == true) log_msg << "- Axis is blocked in positive moving direction" << endl;
	if(bits[4] == true) log_msg << "- Negative direction soft limit reached" << endl;
	if(bits[5] == true) log_msg << "- Positive direction soft limit reached" << endl;
	if(bits[6] == true) log_msg << "- Axis stopped" << endl;
	if(bits[7] == true) log_msg << "- Target position reached" << endl;
	if(bits[8] == true) log_msg << "- Overdrive Mode" << endl;
	if(bits[9] == true) log_msg << "- Force Control Mode" << endl;
	if(bits[12] == true) log_msg << "- Fast Stop" << endl;
	if(bits[13] == true) log_msg << "- Temperature Warning" << endl;
	if(bits[14] == true) log_msg << "- Temperature Error" << endl;
	if(bits[15] == true) log_msg << "- Power Error" << endl;
	if(bits[16] == true) log_msg << "- Engine Current Error" << endl;
	if(bits[17] == true) log_msg << "- Finger Fault" << endl;
	if(bits[18] == true) log_msg << "- Command Error" << endl;
	if(bits[19] == true) log_msg << "- A script is currently running" << endl;
	if(bits[20] == true) log_msg << "- Script Error" << endl;

	info(FFL, log_msg.str());

	free(bits);
}

int WsgClient::SendGoal(wsg_communication::GripperGoal goal)
{
	debug(FFL, "::SendGoal");

	//send goal to action server
	debug(FFL, "Sending goal and waiting...");

	//giving goal unique ID number
	goalID++;
	goal.goalID = goalID;

	myGripperActionClient->sendGoalAndWait(goal);

	//Retrieve result
	log_msg.str("");
	log_msg << "Goal: " << goalID << " completed. Getting result...";
	debug(FFL, log_msg.str());
	int state = myGripperActionClient->getResult()->state;

	log_msg.str("");
	log_msg << "Goal: " << goalID << " finished with state: " << state;
	debug(FFL, log_msg.str());

	return state;
}

