/********************************************************************************************
 * WsgClient.hpp - header of class "WsgClient"												*
 * 																							*
 * Part of the "wsg_communication" ROS-package 												*
 * 																							*
 * Created: Jan 2012 																		*
 * 																							*
 * Author: VT4, Department of Mechanical and Manufacturing engineering, Aalborg University	*
 * 																							*
 * Property of VT4 - AAU. All rights reserved, distribution only by permit by the authors 	*
 * 																							*
 * Beta software - absolutely no guarantees are given										*
 ********************************************************************************************
 *
 * This is the header file of the class "WsgClient"
 * This class is intended to be included and used by the action client side, hence to be included in any user program that will communicate
 * 		with the "wsg_command_node".
 * The purpose of this class is to ease the communication between the user program and the "wsg_command_node" (and the gripper).
 *
 * -----HOW TO USE THIS CLASS-----
 * To use this class for communicating with the "wsg_command_node" and the gripper:
 * 	-Place the WsgClient folder (from the root of this package) within the src-folder of the user-package
 * 	-Place the Gripper.action from the action folder within the action folder of the user-package
 *  -Include the WsgClient into user program, should be "#include "WsgClient/WsgClient.hpp"
 *  -Include the WsgData into the user program, should be "#include "WsgClient/WsgData.hpp"
 *  -Remember to have the "genaction()" and "rosbuild_genmsg()" in CMakeLists.txt of user-package
 *	-Remember to add the "src/WsgClient/WsgClient.cpp" to the executable in the CMakeLists.txt
 *
 * Call a function from this class
 * 		Functions requiring a value, i.e. "SetAcc", is straight forward.
 * 		Functions retrieving a value from the gripper needs a pointer to a variable, so please declare a variable and pass it to the function.
 *
 * All functions return an int which is the state of the command. Please reference this to the error codes from "WsgData.hpp".
 * 		For ease, most functions should return 0 for success.
 *
 * Please note, functions that sends parameters to the gripper (i.e. SetAcc) will ensure the integrity of the data.
 * 		If a value is not within the range of that particular parameter, the value will automatically be set to either the maximum or minimum value,
 * 			depending on which of the range limits is exceeded.
 * 		Example: Passing a speed of 700 mm/s will result in the speed being set to maximum (420 mm/s)
 * 		When this problem is encountered, the user will be notified on the screen.
 */

#ifndef WSGCLIENT_HPP_
#define WSGCLIENT_HPP_

#include <actionlib/client/simple_action_client.h>
#include <string>
#include <iostream>
#include "WsgClient/WsgData.hpp"
#include <wsg_communication/GripperAction.h>
#include "LoggerSys.hpp"

using namespace wsg_communication;

union UStuff
{
	float   f;
	unsigned char   c[4];
};

class WsgClient
{

public:

	static WsgClient* GetInstance();

	~WsgClient();

	int InitCheck();										//check if the gripper is online and initialized

	int Init();												//check if the gripper is initialized, if not do initialization.

	int Loop();												//check communication with gripper

	int Disconnect();										//announce disconnect

	int Home();												//home the gripper (initialize)

	int Move(float position, float speed);					//position fingers

	int Stop();												//stop movements

	int FastStop();											//emergency stop

	int AckStop();											//acknowledge a fast stop

	int Grasp(float width, float speed);					//grasp a part, width i approx. width of part

	int Release(float width, float speed);					//release part, width is the release width

	int SetAcc(float acc);									//set acceleration on gripper

	int GetAcc(float * acc);								//get current acceleration on gripper

	int SetForceLimit(float force);							//set force limit on gripper

	int GetForceLimit(float * force);						//get current force limit on gripper

	int SetLimits(float limit_minus, float limit_plus);		//set soft limits on gripper (work envelope)

	int GetLimits(float * limit_minus, float * limit_plus);	//get current soft limits on gripper

	int ClearLimits();										//clear current soft limits on gripper

	int OverdriveOn();										//activate overdrive mode to allow higher grasping force

	int OverdriveOff();										//deactivate overdrive mode

	int TareForce();										//tare the force sensor

	int GetSysState(bool * sys_state_bits);					//get the system state. The system state is returned as an array of 32 bools

	int GetGraspState(wsg_definitions::graspState *state);	//get the grasp state. A state acccording to wsg_data.hpp will be returned

	int GetGraspStatis(int * total, int * no_part, int * lost_part); //get grasping statistics.

	int ResetGraspStatis();									//reset the grasping statistics on the gripper

	int GetWidth(float * width);							//get the current opening width (finger position)

	int GetSpeed(float * speed);							//get the current speed

	int GetForce(float * force);							//get the current force

	int AutoWidth(int mode, int frequency);					//Start automatic publishing of gripper width

	int AutoSpeed(int mode, int frequency);					//Start automatic publishing of gripper speed

	int AutoForce(int mode, int frequency);					//Start automatic publishing of gripper force

	int AutoGraspState(int mode, int frequency);				//Start automatic publishing of gripper grasp state

	int AutoSysState(int mode, int frequency);				//Start automatic publishing of gripper system state

	int CancelAutoWidth();									//Cancel automatic publishing of width

	int CancelAutoSpeed();									//Cancel automatic publishing of speed

	int CancelAutoForce();									//Cancel automatic publishing of force

	int CancelAutoGraspState();								//Cancel automatic publishing of grasp state

	int CancelAutoSysState();								//Cancel automatic publishing of system state

	int StartAutoPublishers(int mode, int frequency, bool width, bool speed, bool force, bool graspState, bool sysState);
															//Starts publishers at the same interval and in the same mode.

	void CancelAutoPublishers();							//Cancel all automatic publishers

	int StartJSPublisher();									//Start joint state publisher

	int StartJSPublisher(int mode, int frequency);			//Start joint state publisher AND width, speed and force publishing (these are needed for the joint state publisher to function properly)

	int StopJSPublisher();									//Stop joint state publisher


	void PrintError(int error_code);						//print an error code to the screen

	void DecodeSysState(float sys_state, bool * Bits);		//decode a system state from a float to an array of 32 bools

	void PrintSysState();									//print a system state to the screen (only prints the errors)

	std::string gripper_object;								//string to hold the current object in the gripper.

private:

	int goalID;

	static WsgClient* pWsgClient;		// instance of class

	WsgClient(); //private constructor to avoid getting more than one instance of the class

	int SendGoal(wsg_communication::GripperGoal goal); //Send ROS goal and receive result state

	//Declare the Action client
	actionlib::SimpleActionClient<GripperAction>* myGripperActionClient;

	//Declare goal object
	GripperGoal myGoal;

	union UStuff00
	{
		float   f;
		unsigned char   c[4];
	};

	struct autoMode
	{
		int frequency;
		int mode;
	};

	autoMode _auto_width;
	autoMode _auto_speed;
	autoMode _auto_force;
	autoMode _auto_grasp;
	autoMode _auto_sys;

};

#endif /* WSGCLIENT_HPP_ */
