#!/usr/bin/env python

import roslib
roslib.load_manifest("little_helper_tools")

import rospy

from skill_template import BaseSkill

import little_helper_interface

# QR World State services and messages
import qr_world_state.msg as qr_msg
import qr_world_state.srv as qr_srv

from math import sqrt


class PickUpBox(BaseSkill):

    def __init__(self):
        BaseSkill.__init__(self)
        #Add pre and postconditions
        rospy.logdebug("-- Instantiating PickUpBox skill")
        rospy.logdebug("Adding preconditions to the skill")
        self.add_precondition("Object known in world model",self.pre_object_known)
        self.add_precondition("Object is a BOX",self.pre_object_is_box)
        self.add_precondition("Object within reach",self.pre_object_within_reach)
        rospy.logdebug("Adding postconditions to the skill")
        self.add_postcondition("Object not at pick-up location",self.post_object_picked)
        self.add_postcondition("Object in gripper",self.post_object_grasped)
        #Connect to ROS stuff
        self.ros_connect()

        self._box = qr_msg.Observation() #the actual box data, copied from the world model
        self._parameter = 0 # the QR ID of the box

    def ros_connect(self):
        #create the little helper object
        self.lhi = little_helper_interface.LittleHelperInterfaceM39(world_state=True,qr_detector=True)

    def usage(self):
        rospy.logfatal("One, and only one, argument must be given, which is the ID (int) of the object to be picked up in the state_model")
        return False

    def run_skill(self,box_id):
        """ box_id is the unique ID of the box to pick up """

        ### INITIAL CHECKS ###
        if not type(box_id)==int:
            return self.usage()
        self._parameter = box_id

        ### PRECONDITION CHECK ###
        #printout
        rospy.logdebug("PickUpBox skill preconditions (%s total):" % len(self.pre)  )
        for n in self.pre:
            n.printout("debug")
        #test them
        rospy.loginfo("--- Testing all preconditions:")
        pre = self.evaluate_all_preconditions()
        rospy.loginfo("Sum of preconditions is " + str(pre))
        if not pre==True:
            rospy.logfatal("There are failed preconditions, quitting")
            return False

        ### EXECUTE ###
        rospy.logdebug("--- Running execute() function on PickUpBox skill:")
        if not self.execute():
            rospy.logfatal("Execution failed, quitting")
            return False

        ### POSTCONDITION CHECKS ###
        #printout
        rospy.logdebug("PickUpBox skill postconditions (%s total):" % len(self.post)  )
        for n in self.post:
            n.printout("debug")
        #test them
        rospy.loginfo("--- Testing all postconditions:")
        post = self.evaluate_all_postconditions()
        rospy.loginfo("Sum of postconditions is " + str(post))
        if not post==True:
            rospy.logfatal("There are failed preconditions, quitting")
            return False

        #Hooray
        return True

    def execute(self):
        #Not really much magic in this code...
        return self.lhi.pick_up_box(self._box.value,back_up=True)

    def pre_object_known(self):
        rospy.logdebug("Testing if box is in world state")
        qr_query = qr_srv.QueryObservationsRequest()
        qr_query.qr_ids = []
        resp = self.lhi.qr_qry_srv.call(qr_query)
        qr_ids = [i.qr_id for i in resp.obs_list]
        if not self._parameter in qr_ids:
            rospy.logerr("Box ID %s not in world state, possible IDs are %s",self._parameter,qr_ids)
            return False
        else:
            box = [i for i in resp.obs_list if i.qr_id==self._parameter]
            self._box = box[0]
            rospy.loginfo("Object ID %s ('%s') is present in world state",self._box.qr_id,self._box.value)
            return True

    def pre_object_is_box(self):
        rospy.logdebug("Testing if the object is a BOX")
        if not self._box.type == "BOX":
            rospy.logerr("Object to pick up is not a BOX, but a %s",self._box.type)
            return False
        else:
            rospy.loginfo("Object to pick up is in fact a BOX")
            return True

    def pre_object_within_reach(self):
        rospy.logdebug("Testing if the object is within reach of the arm")
        #self._box.pose.header.stamp = self.lhi.tl.getLatestCommonTime("calib_lwr_arm_base_link", self._box.pose.header.frame_id)
        pos = self.lhi.transform_pose("calib_lwr_arm_base_link", self._box.pose)
        # distance to shoulder link
        dist = sqrt((pos.pose.position.x)**2 + (pos.pose.position.y)**2 + (pos.pose.position.z)**2)
        if dist > 0.95: #TODO we need better estimate of reach
            rospy.logerr("Object is apparently out of reach (%s m from arm base link)",dist)
            return False
        else:
            rospy.loginfo("Object within workspace (%s m from arm base link)",dist)
            return True

    def post_object_picked(self):
        rospy.logdebug("Testing if box is still present at pick up location")
        qr_codes = self.lhi.detect_qr_codes(add_to_world_state=False)
        box_string = self._box.type + '.' + self._box.value
        if box_string in [i.data for i in qr_codes]: #box detected
            rospy.logerr("The box was detected, so I'm guessing it's not picked up")
            return False
        else:
            rospy.loginfo("Seems like the box is no longer at the previous location - adding the box to the gripper")
            self.lhi.add_box_to_gripper(self._box.value)

            return True

    def post_object_grasped(self):
        #TODO maybe add some stuff that check the force sensors?
        rospy.loginfo("Assuming the box is in the gripper....")
        return True

if __name__=='__main__':
    rospy.init_node("pick_up_box_skill",log_level=rospy.DEBUG)
    rospy.sleep(0.5)
    rospy.loginfo("============================")
    rospy.loginfo("Starting the PickUpBox skill")
    rospy.loginfo("============================")
    skill = PickUpBox()
    skill.run_skill(1)
