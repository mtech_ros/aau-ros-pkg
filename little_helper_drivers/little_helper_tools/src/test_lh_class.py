#!/usr/bin/env python

import roslib; roslib.load_manifest("little_helper_tools")
import rospy
import little_helper_interface as lh_intf


rospy.init_node("test_lwr_arm",log_level=rospy.DEBUG)


# which test to perform ? 
WHICH = 1


# simple test drive, without qr stuff
if WHICH == 1:
    # init intf 
    lh = lh_intf.LittleHelperInterfaceM39()
    rospy.sleep(2)
    # velocity used (note, "vel = 1.0" is fast, but not insane)
    vel = 0.25 
    # gripper before platform
    test_pose = [-125,148,0,-115,-35,-87.5,3.5]
    curr_pose = lh.move_arm(test_pose,"PTP_JOINT_ABS",velocity=vel)
    print "Moved arm to %s"%(curr_pose)
    # move gripper forward
    raw_input("Press to continue")
    forward = [0,0,200,0,0,0]
    curr_pose = lh.move_arm(forward, "LIN_REL_TOOL", velocity=vel)
    print "Moved arm to %s"%(curr_pose)
    # gripper in some park pose
    raw_input("Press to continue")
    park_pose = [475,320,290,90,-90,100] # same as lh.lwr_park_via,
    curr_pose = lh.move_arm(park_pose,"PTP_CART_ABS",velocity=vel)
    print "Moved arm to %s"%(curr_pose)
    # gripper in provided park pose
    raw_input("Press to continue")
    curr_pos = lh.move_arm(lh.lwr_pos_home,"PTP_JOINT_ABS",velocity=vel)
    print "Moved arm to %s, quitting"%(curr_pose)
    exit(0)


# just go in park pose (without qr)
if WHICH == 2:
    lh = lh_intf.LittleHelperInterfaceM39() 
    pos = lh.move_arm(lh.lwr_park_via,"PTP_CART_ABS",velocity=0.25)
    exit(0)

# just go in home pose (without qr)
if WHICH == 3:
    lh = lh_intf.LittleHelperInterfaceM39() 
    curr_pos = lh.move_arm(lh.lwr_pos_home,"PTP_JOINT_ABS",velocity=0.25)
    exit(0)


# pick up green box, using qr detector 
if WHICH == 4: 
    # init intf with qr detector 
    lh = lh_intf.LittleHelperInterfaceM39(qr_detector=True)
    # park arm 
    pos = lh.move_arm(lh.lwr_park_via,"PTP_CART_ABS",velocity=0.25)
    # detect box, and print (just a check)
    qr = lh.detect_qr_codes(add_to_world_state=False)
    print "Found QR codes:",qr
    # pick up 
    lh.pick_up_box("GREEN", back_up=False)
    if 1:
        rospy.sleep(5)
        pos = lh.move_arm(lh.lwr_park_via,"PTP_CART_ABS",velocity=0.25)
    exit(0)
