# --- Somewhat useful functions for the M39 demo ---

""" These are useful coordinates for manipulating boxes
Pick up box from table or shelf (klcalib.pick_up_box2)
- Working position (in front of robot) PTP_JOINT_ABS: [-125,148,0,-115,-35,-87.5,3.5]
(move to the pregrasp pose (z=20cm), and then the grasp pose)
- Lifting position LIN_REL_TOOL: [100,0,0,0,0,0]
- Pull back position LIN_REL_TOOL: [0,0,-200,0,0,0]
(here we back up the platform a little - 30cm)
- Park via position PTP_CART_ABS: [475,320,290,90,-90,100]
- Home/parked position PTP_JOINT_ABS: [-165,125,-35,-115,115,-85,-35]

Put down a box in a shelf (klcalib.put_down_box2)
- Working position PTP_JOINT_ABS: [-126.9,142.5,0,-119.5,-41.33,-89.5,10.8]
(move to preplace pose (y=15cm, x=9.5cm) and then the place pose)
- Depart LIN_REL_TOOL: [0,0,-220,0,0,0]
- Park via position PTP_CART_ABS: [475,320,290,90,-90,100]
- Home/parked position PTP_JOINT_ABS: [-165,135,-10,-115,100,-90,-20]
(and then back up 50cm)

Put down a box on the table (klcalib.place_empty)
- Working position PTP_JOINT_ABS: [-124,143,0,-118,-33,-88,6]
(move to preplace (y=4cm) and place pose)
- Depart LIN_REL_TOOL: [0,0,-220,0,0,0]
- Park via position PTP_CART_ABS: [475,320,290,90,-90,100]
- Home/parked position PTP_JOINT_ABS: [-165,135,-10,-115,100,-90,-20]
(and then back up the platform 1m)
"""

# --- Back up the robot ---
#This guy needs a publisher to cmd_vel topic(pub)
def back_up(pub,vel,dist):
    """ Tell the platform to back up a little ways
    @pub: publisher to cmd_vel
    @vel: velocity in m/s
    @dist: distance in m
    """
    rospy.logdebug("Backing up platform, %s m with %s m/s",dist,vel)
    move = gm.Twist()
    move.linear.x = -vel 
    pub.publish(move)
    rospy.sleep(dist/vel)
    rospy.logdebug("Stopping platform")
    pub.publish(gm.Twist())
    return   

# --- Get the ID of a detected QR code in the world state ---
# needs service for the qr_world_state node (qry_srv)
def get_qr_id(type,value,qry_srv):
    """ Get QR id by type and value
    Returns id, PoseStamped
    """
    rospy.logdebug("Querying all objects of type %s in the world",type)
    qry_obj = qr_srv.QueryObservationsRequest()
    qry_obj.types.append(type.upper())
    qry_obj.values = [str(value).upper()]
    qry_obj.location = []
    qry_obj.in_gripper = []
    resp = qry_srv(qry_obj).obs_list
    if len(resp)==0: 
        rospy.logwarn("No objects that match the query!")
        return -1,-1
    
    # add to gripper
    #print resp_box.obs_list
    return resp[0].qr_id, resp[0].pose

# --- Example of getting pose in LWR frame relative to another pose ---
def get_lwr_pose_putdown(boxpose):
    """
    Put down box with a given pose of the QR code, with specified approach distance
    """
    #create tf stuff
    tl = tf.TransformListener()
    tb = tf.TransformBroadcaster()
    #transform into lwr frame
    boxpose.header.stamp = rospy.Time()
#tl.getLatestCommonTime("/calib_lwr_arm_base_link",qr_pose.header.frame_id)
    working=False
    while working==False:
        try:
            rospy.loginfo("Trying to transform")
            tl.waitForTransform("/calib_lwr_arm_base_link",boxpose.header.frame_id,boxpose.header.stamp,rospy.Duration(1.0))
            boxpose = tl.transformPose("/calib_lwr_arm_base_link", boxpose)
            working = True
        except tf.Exception as e:
            rospy.logwarn("TF error: %s",e)
            working=False
    #create pose to pick
    q = boxpose.pose.orientation
    now = rospy.Time.now()
    tb.sendTransform([boxpose.pose.position.x,boxpose.pose.position.y,boxpose.pose.position.z], 
                     [q.x,q.y,q.z,q.w],
                     now,
                     "/qr_frame",
                     boxpose.header.frame_id)
    rospy.loginfo("Published '/qr_frame'")
    box_goal = gm.PoseStamped()
    box_goal.header.frame_id = "/qr_frame"
    working=False
    while working==False:
        try:
            rospy.loginfo("Trying to transform")
            tl.waitForTransform("/calib_lwr_arm_base_link","/qr_frame",now,rospy.Duration(1.0))
            box_goal.header.stamp = tl.getLatestCommonTime("/calib_lwr_arm_base_link","/qr_frame")
            box_goal.pose.position.x = 0.0 #offset TCP to boxgripper
            box_goal.pose.position.y = -0.320 #offset TCP to boxgripper
            box_goal.pose.position.z = 0.095-0.035 #offset TCP to boxgripper
            box_q = tf.transformations.quaternion_from_euler(math.pi,0.0,math.pi/2.0) #rotation of gripper wrt qr
            box_goal.pose.orientation = gm.Quaternion(box_q[0],box_q[1],box_q[2],box_q[3])
            place_pose = tl.transformPose("/calib_lwr_arm_base_link", box_goal) # full position and orientation, might not work
            #create pregrasp pose
            box_pre = box_goals
            box_pre.pose.position.y += 0.15 #offset and approach distance
            box_goal.pose.position.z = 0.095 #offset TCP to boxgripper
            #same rot as before (see box_q above)
            box_pre.pose.orientation = gm.Quaternion(box_q[0],box_q[1],box_q[2],box_q[3])
            preplace_pose = tl.transformPose("/calib_lwr_arm_base_link", box_pre) # full position and orientation, might not work    
            working=True
        except tf.Exception as e:
            rospy.logwarn("TF error: %s",e)
            working=False
    
    return preplace_pose, place_pose