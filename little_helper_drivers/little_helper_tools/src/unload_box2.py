#!/usr/bin/env python

import roslib
roslib.load_manifest("little_helper_tools")

import rospy
import sys
import numpy as np

from skill_template2 import BaseSkill2

import little_helper_interface

# QR World State services and messages
import qr_world_state.msg as qr_msg
import qr_world_state.srv as qr_srv

import geometry_msgs.msg as gm

from math import sqrt


class UnloadBox2(BaseSkill2):

    def __init__(self,pout):
        BaseSkill2.__init__(self,pout)
        #Add pre and postconditions
        rospy.logdebug("-- Instantiating UnloadBox2 skill")
        rospy.logdebug("Adding preconditions to the skill")
        self.add_precondition("LOC_KNOWN",self.pre_location_known)
        self.add_precondition("LOC_VALID", self.pre_location_is_valid)
        self.add_precondition("OBJ_IN_GRIP", self.pre_object_grasped)
        self.add_precondition("LOC_IN_REACH", self.pre_location_within_reach)

        rospy.logdebug("Adding postconditions to the skill")
        self.add_postcondition("OBJ_EMPTY", self.post_object_emptied)
        self.add_postcondition("GRIP_MOVED", self.post_gripper_away)
        #Connect to ROS stuff
        self.ros_connect()

        self._final_pose = gm.PoseStamped() # the real posestamped place pose (free location to place on)
        self._location = qr_msg.Observation()  #the actual location data, copied from the world model
        self._box_in_gripper = qr_msg.Observation()
        self._parameter = 0  # the QR ID of the location to place on
        
    def ros_connect(self):
        #create the little helper object
        self.lhi = little_helper_interface.LittleHelperInterfaceM39(world_state=True,qr_detector=True)
        
    def usage(self):
        rospy.logfatal("One, and only one, argument must be given, which is the ID (int) of the location to place the box")
        return False

    def check_parameter(self,param):
        if not type(param)==int:
            return self.usage()
        self._parameter = param
        return True

    def execute(self):
        
        feeder_string = self._location.type + "." + self._location.value
        rospy.loginfo("Will attempt to unload a box in %s",feeder_string)
        
        # Find the QR location
        found_location = False
        codes = self.lhi.detect_qr_codes(add_to_world_state=True)
        for qr in codes:
            #skip if it's not the location we're looking for
            if not qr.data.split(".")[0:2] == [self._location.type,self._location.value]:
                continue
            if qr.pose.pose==gm.Pose():
                rospy.logwarn("Invalid pose data for QR code: %s",qr.data)
                continue
            feeder_pose = self.lhi.transform_pose("/calib_lwr_arm_base_link", qr.pose)
            found_location = True
            rospy.logdebug("Detected feeder '%s'",feeder_string)
            break
    
        if not found_location:
            #rospy.logwarn("Deleting '"+feeder_string+"' from world model")
            #del_req = qr_srv.DestroyObservationRequest()
            #del_req.qr_id = self._location.qr_id
            #self.lhi.qr_des_srv.call(del_req)
            return self.execution_error("No location '"+feeder_string+"' detected!")
     
        
        #return
        # Start execution
        rospy.loginfo("Moving arm to ready position")
        #if not self.lhi.move_arm(self.lhi.lwr_park_via,"PTP_CART_ABS",0.50).any(): return self.execution_error("Moving the LWR arm failed")
        pos_work_grasp = np.array([-124,130,0,-119,-33,-77,10]) # For shelf and table
        if not self.lhi.move_arm(pos_work_grasp,"PTP_JOINT_ABS",0.50).any(): return self.execution_error("Moving the LWR arm failed")

        #not entirely accurate, but works
        above, unused_pose = self.lhi.get_lwr_pose_place_table(feeder_pose,approach=0.100)
    
        rospy.loginfo("Unloading box")
        if not self.lhi.move_arm(above.pose,"BASE_LIN_QUAT",0.35).any(): return self.execution_error("Moving the LWR arm failed")
        unload_motion = np.array([0,-50,50,140,0,0]) # 140 degree rotation around tool z axis, and slightly move to the left
        if not self.lhi.move_arm(unload_motion,"LIN_REL_TOOL",0.35).any(): return self.execution_error("Moving the LWR arm failed")
        # set the empty states
        self.lhi.set_empty_state(self._box_in_gripper.qr_id,True)
        self.lhi.set_empty_state(self._location.qr_id,False)
        rospy.loginfo("Retracting box")
        depart = np.array([0,0,-160,-140,0,0]) # retract and rotate tool back
        if not self.lhi.move_arm(depart,"LIN_REL_TOOL",0.40).any(): return self.execution_error("Moving the LWR arm failed")
        #back up
        #rospy.logdebug("Backing up the robot")
        #self.lhi.back_up(0.20,0.30)
        #rospy.sleep(0.5)
        rospy.loginfo("Parking arm through via points")
        #if not self.lhi.move_arm(pos_work_grasp,"PTP_JOINT_ABS",0.40).any(): return self.execution_error("Moving the LWR arm failed")
        #if not self.lhi.move_arm(self.lhi.lwr_park_via,"PTP_CART_ABS",0.50).any(): return self.execution_error("Moving the LWR arm failed")
        if not self.lhi.move_arm(self.lhi.lwr_pos_home,"PTP_JOINT_ABS",0.50).any(): return self.execution_error("Moving the LWR arm failed")
    
        # Hooray!
        return True
            
    def pre_location_known(self):
        rospy.logdebug("Testing if location is in world state")
        qr_query = qr_srv.QueryObservationsRequest()
        qr_query.qr_ids = []
        resp = self.lhi.qr_qry_srv.call(qr_query)
        qr_ids = [i.qr_id for i in resp.obs_list]
        if not self._parameter in qr_ids:
            rospy.logwarn("QR ID %s not in world state, possible IDs are %s",self._parameter,qr_ids)
            return False
        else:
            loc = [i for i in resp.obs_list if i.qr_id==self._parameter]
            self._location = loc[0]
            rospy.loginfo("QR ID %s ('%s') is present in world state",self._location.qr_id,self._location.type+"."+self._location.value)
            return True

    def pre_location_is_valid(self):
        rospy.logdebug("Testing if the location is a valid place location")
        if not self._location.type == "FEEDER":
            rospy.logwarn("Location to unload in is not valid, it is a %s", self._location.type)
            return False
        else:
            rospy.loginfo("Location to unload in is valid, it is a %s", self._location.type)
            return True

    def pre_location_within_reach(self):
        rospy.logdebug("Testing if the location is within reach of the arm")
        #self._location.pose.header.stamp = self.lhi.tl.getLatestCommonTime("calib_lwr_arm_base_link", self._location.pose.header.frame_id)
        pos = self.lhi.transform_pose("calib_lwr_arm_base_link",self._location.pose)
        # distance to shoulder link
        dist = sqrt((pos.pose.position.x)**2 + (pos.pose.position.y)**2 + (pos.pose.position.z)**2)
        if dist > 0.95: #TODO we need better estimate of reach
            rospy.logwarn("Object is apparently out of reach (%s m from base link)",dist)
            return False
        else:
            rospy.loginfo("Object likely within workspace (%s m from base link)",dist)
            return True

    def pre_object_grasped(self):
        #TODO maybe add some stuff that check the force sensors?
        rospy.logdebug("Querying for box in gripper")
        qry_obj = qr_srv.QueryObservationsRequest()
        qry_obj.types.append("BOX")
        qry_obj.values = []
        qry_obj.location = []
        qry_obj.in_gripper = [True] # This field is the important one
        resp = self.lhi.qr_qry_srv(qry_obj).obs_list
        if not len(resp) == 1: 
            rospy.logwarn("According to the world state there is no box in the gripper!")
            return False
        else:
            rospy.loginfo("%s.%s is currently in the gripper",resp[0].type,resp[0].value)
            self._box_in_gripper = resp[0]
            return True

    def post_object_emptied(self):
        rospy.logdebug("Testing if the box is emptied")
        #get the feeder
        qry_obj = qr_srv.QueryObservationsRequest()
        qry_obj.types.append(self._location.type)
        qry_obj.values.append(self._location.value)
        qry_obj.location = []
        feed_resp = self.lhi.qr_qry_srv(qry_obj).obs_list
        self._location = feed_resp[0]
        #get the box
        qry_obj = qr_srv.QueryObservationsRequest()
        qry_obj.in_gripper = [True]
        box_resp = self.lhi.qr_qry_srv(qry_obj).obs_list
        self._box_in_gripper = box_resp[0]
        feeder = not self._location.empty # if the feeder is not empty we're good
        box = self._box_in_gripper.empty # the box must be empty now...
        if feeder and box:
            rospy.loginfo("Seems like %s is empty, and %s is not empty",self._box_in_gripper.type+"."+self._box_in_gripper.value,self._location.type+"."+self._location.value)
        else:
            rospy.logwarn("Somethings wrong - BOX empty state is %s, location empty state is %s",self._box_in_gripper.empty,self._location.empty)
        return feeder and box

    def post_gripper_away(self):
        rospy.logdebug("Testing if the gripper is away from the unload location")
        pos = self.lhi.transform_pose("/box_gripper",self._location.pose)
        # distance to shoulder link
        dist = sqrt((pos.pose.position.x)**2 + (pos.pose.position.y)**2 + (pos.pose.position.z)**2)
        if dist < 0.1:
            rospy.logwarn("Gripper is apparently too close to the unload location (%s m)",dist)
            return False
        else:
            rospy.loginfo("Gripper is away from the unload location (%s m)",dist)
            return True
        

def test_pout(line):
    #only for testing the skill
    print "==== PIPE: " + line

if __name__=='__main__':
    rospy.init_node("unload_box_skill",log_level=rospy.DEBUG)
    rospy.sleep(0.5)
    skill = UnloadBox2(test_pout)
    skill.run_skill(int(sys.argv[1]))
