#!/usr/bin/env python

import roslib
roslib.load_manifest("little_helper_tools")

import rospy

from skill_template import BaseSkill

import little_helper_interface

# QR World State services and messages
import qr_world_state.msg as qr_msg
import qr_world_state.srv as qr_srv

from math import sqrt


class PlaceBox(BaseSkill):

    def __init__(self):
        BaseSkill.__init__(self)
        #Add pre and postconditions
        rospy.logdebug("-- Instantiating PlaceBox skill")
        rospy.logdebug("Adding preconditions to the skill")
        self.add_precondition("Location known in world model",self.pre_location_known)
        self.add_precondition("Location valid for placing", self.pre_location_is_valid)
        self.add_precondition("Object in gripper", self.pre_object_grasped)
        self.add_precondition("Location within reach", self.pre_location_within_reach)

        rospy.logdebug("Adding postconditions to the skill")
        self.add_postcondition("Object at correct location", self.post_object_placed)
        self.add_postcondition("Gripper away from object", self.post_gripper_away)
        self.add_postcondition("No object in gripper", self.post_gripper_empty)
        #Connect to ROS stuff
        self.ros_connect()

        self._location = qr_msg.Observation()  #the actual location data, copied from the world model
        self._parameter = 0  # the QR ID of the location to place on

    def ros_connect(self):
        #create the little helper object
        self.lhi = little_helper_interface.LittleHelperInterfaceM39(world_state=True,qr_detector=True)

    def usage(self):
        rospy.logfatal("One, and only one, argument must be given, which is the ID (int) of the location to place the box")
        return False

    def run_skill(self, location_id):
        """ location_id is the unique QR ID of the location to place on """

        ### INITIAL CHECKS ###
        if not type(location_id)==int:
            return self.usage()
        self._parameter = location_id

        ### PRECONDITION CHECK ###
        #printout
        rospy.logdebug("PlaceBox skill preconditions (%s total):" % len(self.pre)  )
        for n in self.pre:
            n.printout("debug")
        #test them
        rospy.loginfo("--- Testing all preconditions:")
        pre = self.evaluate_all_preconditions()
        rospy.loginfo("Sum of preconditions is " + str(pre))
        if not pre==True:
            rospy.logfatal("There are failed preconditions, quitting")
            return False

        ### EXECUTE ###
        rospy.logdebug("--- Running execute() function on PlaceBox skill:")
        if not self.execute():
            rospy.logfatal("Execution failed, quitting")
            return False

        ### POSTCONDITION CHECKS ###
        #printout
        rospy.logdebug("PlaceBox skill postconditions (%s total):" % len(self.post)  )
        for n in self.post:
            n.printout("debug")
        #test them
        rospy.loginfo("--- Testing all postconditions:")
        post = self.evaluate_all_postconditions()
        rospy.loginfo("Sum of postconditions is " + str(post))
        if not post==True:
            rospy.logfatal("There are failed preconditions, quitting")
            return False

        #Hooray
        return True

    def execute(self):
        #Not really much magic in this code...
        if self._location.type == "SHELF":
            return self.lhi.place_box_shelf(self._location.value, back_up=True)
        elif self._location.type == "TABLE":
            return self.lhi.place_box_table(self._location.value, back_up=True)
        else:
            rospy.logerr("Forgot to do sanity check on parameter - you can't place a box on a %s",self._location.type)
            return False

    def pre_location_known(self):
        rospy.logdebug("Testing if location is in world state")
        qr_query = qr_srv.QueryObservationsRequest()
        qr_query.qr_ids = []
        resp = self.lhi.qr_qry_srv.call(qr_query)
        qr_ids = [i.qr_id for i in resp.obs_list]
        if not self._parameter in qr_ids:
            rospy.logerr("QR ID %s not in world state, possible IDs are %s",self._parameter,qr_ids)
            return False
        else:
            loc = [i for i in resp.obs_list if i.qr_id==self._parameter]
            self._location = loc[0]
            rospy.loginfo("QR ID %s ('%s') is present in world state",self._location.qr_id,self._location.type+"."+self._location.value)
            return True

    def pre_location_is_valid(self):
        rospy.logdebug("Testing if the location is a valid place location")
        if not self._location.type in ["SHELF", "TABLE"]:
            rospy.logerr("Location to place on is not valid, it is a %s", self._location.type)
            return False
        else:
            rospy.loginfo("Location to place on is valid, it is a %s", self._location.type)
            return True

    def pre_location_within_reach(self):
        rospy.logdebug("Testing if the object is within reach of the arm")
        #self._location.pose.header.stamp = self.lhi.tl.getLatestCommonTime("calib_lwr_arm_base_link", self._location.pose.header.frame_id)
        pos = self.lhi.transform_pose("calib_lwr_arm_base_link",self._location.pose)
        # distance to shoulder link
        dist = sqrt((pos.pose.position.x)**2 + (pos.pose.position.y)**2 + (pos.pose.position.z)**2)
        if dist > 0.95: #TODO we need better estimate of reach
            rospy.logerr("Object is apparently out of reach (%s m from base link)",dist)
            return False
        else:
            rospy.loginfo("Object within workspace (%s m from base link)",dist)
            return True

    def pre_object_grasped(self):
        #TODO maybe add some stuff that check the force sensors?
        rospy.loginfo("Assuming the box is in the gripper....")
        return True

    def post_object_placed(self):
        rospy.logdebug("Testing if a box is present at put down location")
        qr_codes = self.lhi.detect_qr_codes(add_to_world_state=True)
        loc_string = self._location.type + '.' + self._location.value
        success = False
        for qr in qr_codes:
                if qr.data == loc_string:
                    place_pose = qr.pose
                    success = True
                    break
        if not success:
            rospy.logerr("QR code %s not detected",loc_string)
            return False
        success = False
        for qr in qr_codes:
            #is it a box?
            if qr.data.split(".")[0]=="BOX":
                # is it on table/shelf? (10cm from vertical axis)
                rospy.logdebug("Found BOX, x dist: %s", abs(qr.pose.pose.position.x-place_pose.pose.position.x))
                if abs(qr.pose.pose.position.x-place_pose.pose.position.x) < 0.10:
                    rospy.logdebug("Found %s on %s", qr.data, loc_string)
                    success = True
                    break
        if success:
            rospy.loginfo("Detected a box on %s",loc_string)
        else:
            rospy.logerr("No box found on %s",loc_string)
        return success

    def post_gripper_away(self):
        rospy.logdebug("Testing if the gripper is away from the place location")
        self._location.pose.header.stamp = self.lhi.tl.getLatestCommonTime("box_gripper", self._location.pose.header.frame_id)
        pos = self.lhi.transform_pose("box_gripper",self._location.pose)
        # distance to shoulder link
        dist = sqrt((pos.pose.position.x)**2 + (pos.pose.position.y)**2 + (pos.pose.position.z)**2)
        if dist < 0.25:
            rospy.logerr("Gripper is apparently too close to the place location (%s m)",dist)
            return False
        else:
            rospy.loginfo("Gripper is away from the place location (%s m)",dist)
            return True


    def post_gripper_empty(self):
        #TODO maybe add some stuff that check the force sensors?
        rospy.loginfo("Assuming the gripper is empty....")
        return True

if __name__=='__main__':
    rospy.init_node("place_box_skill",log_level=rospy.DEBUG)
    rospy.sleep(0.5)
    rospy.loginfo("==============================")
    rospy.loginfo(" Starting the PlaceBox skill")
    rospy.loginfo("==============================")
    skill = PlaceBox()
    skill.run_skill(3)
