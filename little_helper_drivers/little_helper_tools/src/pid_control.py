#!/usr/bin/env python

class PID_controller:
  """
  Discrete PID control
  """

  def __init__(self, P=2.0, I=0.0, D=1.0, D_state=0, I_state=0, I_max=5, I_min=-5):

    self.Kp=P
    self.Ki=I
    self.Kd=D
    self.D_state=D_state
    self.I_state=I_state
    self.I_max=I_max
    self.I_min=I_min

  def update(self,error,current):
    """
    Calculate PID output value for given reference input and feedback
    """
    self.error = error

    # proportional term
    self.P_term = self.Kp * self.error
    
    self.D_term = self.Kd * ( current - self.D_state)
    self.D_state = current

    # Calculate Integral state with limiting
    self.I_state = self.I_state + self.error

    if self.I_state > self.I_max:
      self.I_state = self.I_max
    elif self.I_state < self.I_min:
      self.I_state = self.I_min

    self.I_term = self.I_state * self.Ki

    PID = self.P_term + self.I_term + self.D_term

    return PID

  def setIntegrator(self, I_state):
    self.I_state = I_state

  def setDerivator(self, D_state):
    self.D_state = D_state

  def setKp(self,P):
    self.Kp=P

  def setKi(self,I):
    self.Ki=I

  def setKd(self,D):
    self.Kd=D

  def getError(self):
    return self.error

  def getIntegrator(self):
    return self.I_state

  def getDerivator(self):
    return self.D_state
