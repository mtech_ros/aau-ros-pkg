#!/usr/bin/env python
import roslib
roslib.load_manifest("little_helper_tools")

import rospy

import pid_control
import geometry_msgs.msg as gm

import tf
import math

class Follower():
    
    def __init__(self):
        
        # create PID controllers
        self.lin_pid = pid_control.PID_controller(P=7.0,I=0.0,D=0.0)
        self.ang_pid = pid_control.PID_controller(P=7.0,I=0.0,D=0.0)
        
        #set control rate for the PID controller
        self.control_rate = 10.0 #Hz
        
        # follow parameters
        self.lin_desired = 2.0 #desired distance sensor to person
        self.lin_threshold = 0.10 # ignore if within the threshold
        self.ang_desired = 0.0 #desired angular distance sensor to person
        self.ang_threshold = 0.1 * math.pi/180 #ignore if within the threshold
        
        # velocity limits
        self.vel_limit_lin = 0.3 #m/s
        self.vel_limit_ang = 30.0 * math.pi/180 #rad/s
        
        #create velocity publisher
        self.vel_pub = rospy.Publisher("/cmd_vel",gm.Twist)
        
        
#    def follow(self,head_position,stop=False):
#        """ Follows the person at head_position, which is a geometry_msgs/Point in the base_link frame
#        Set the desired linear and angular distance in the class
#        stop keyword is used for stopping the robot, and resetting the PID controllers
#        """

    def follow(self,x,y):
        """ same as the above, but separation of x and y head position"""
        
        #calculate current distances, assuming we are only looking forward in the x axis
#        this_lin_dist = head_position.x
#        this_ang_dist = math.atan2(head_position.y, head_position.x)

        #sort out far or close coordinates
        #if x>5.0 or x<0.5: x=0
        #if y>2.0 or y<-2.0: y=0

        this_lin_dist = x
        this_ang_dist = math.atan2(y, x)
        
        #print "ang_dist: "+str(this_ang_dist)
        
        #calculate errors
        lin_error = this_lin_dist - self.lin_desired
        ang_error = this_ang_dist - self.ang_desired
        #print "ang_error: "+str(ang_error)
        
        #Calculate control
        #linear
#        if abs(lin_error)>self.lin_threshold:
#            new_lin = self.lin_pid.update(lin_error, this_lin_dist)
#        else:
#            new_lin = 0.0
#        #angular
#        if abs(ang_error)>self.ang_threshold:
#            new_ang = self.ang_pid.update(ang_error, this_ang_dist)
#        else:
#            new_ang = 0.0
        
        new_lin = self.lin_pid.update(lin_error, this_lin_dist)
        new_ang = self.ang_pid.update(ang_error, this_ang_dist)

        #print "new_lin: "+str(new_lin)
        #print "new_ang: "+str(new_ang)
        #calculate velocity
        lin_vel = new_lin/self.control_rate
        ang_vel = new_ang/self.control_rate
        
        #throttle velocity
        if lin_vel>self.vel_limit_lin: 
            print "Linear velocity throttled to %s (was %s)"%(self.vel_limit_lin,lin_vel)
            lin_vel = self.vel_limit_lin
        elif lin_vel<-self.vel_limit_lin: 
            print "Linear velocity throttled to %s (was %s)"%(self.vel_limit_lin,lin_vel)
            lin_vel = -self.vel_limit_lin
#        if ang_vel>self.vel_limit_ang: 
#            print "Angular velocity throttled to %s (was %s)"%(self.vel_limit_ang,ang_vel)
#            ang_vel = self.vel_limit_ang
#        elif ang_vel<-self.vel_limit_ang: 
#            print "Angular velocity throttled to %s (was %s)"%(self.vel_limit_ang,ang_vel)
#            ang_vel = -self.vel_limit_ang
#            
        vel = gm.Twist()
        vel.linear.x = lin_vel
        vel.angular.z = ang_vel
        #publish velocity
#        rospy.loginfo("Sending velocity x: %s, rz: %s",vel.linear.x,vel.angular.z)
        self.vel_pub.publish(vel)
        #wait a little while
        rospy.sleep(1.0/self.control_rate)
        
    def stop_follow(self):
        """ Stops following the human """
        
        # send 0 velocity
 #       rospy.loginfo("Sending velocity x: 0.0, rz: 0.0")
        self.vel_pub.publish(gm.Twist())
        #reset PID controller
        self.lin_pid.setIntegrator(0.0)
        self.lin_pid.setDerivator(0.0)
        self.ang_pid.setIntegrator(0.0)
        self.ang_pid.setDerivator(0.0)
        print "Stopped following"
        return

## Testing code (uncomment publishing above if you want)
if __name__=="__main__":
    rospy.init_node("follow_tester",log_level=rospy.DEBUG)
    rospy.sleep(0.5)
    f = Follower()
    rospy.loginfo("Testing the follower node...")
    rospy.loginfo("Desired x: %s   y: %s",f.lin_desired,f.ang_desired)
    test_time = 2 #sec for each test
    # at desired position
    x = 2.0
    y = 0.0
    rospy.loginfo("Sending x: %s   y: %s",x,y)
    raw_input("Press key to continue")
    i=0
    while i<test_time*f.control_rate:
        f.follow(x,y)
        i += 1
    f.stop_follow()
    print "robot should move forward"
    x = 3.0
    y = 0.0
    rospy.loginfo("Sending x: %s   y: %s",x,y)
    raw_input("Press key to continue")
    i=0
    while i<test_time*f.control_rate:
        f.follow(x,y)
        i += 1
    f.stop_follow()
    print "robot should move backward"
    x = 1.0
    y = 0.0
    rospy.loginfo("Sending x: %s   y: %s",x,y)
    raw_input("Press key to continue")
    i=0
    while i<test_time*f.control_rate:
        f.follow(x,y)
        i += 1
    f.stop_follow()
    print "robot should turn right"
    x = 2.0
    y = -2.0
    rospy.loginfo("Sending x: %s   y: %s",x,y)
    raw_input("Press key to continue")
    i=0
    while i<test_time*f.control_rate:
        f.follow(x,y)
        i += 1
    f.stop_follow()
    print "robot should turn left"
    x = 2.0
    y = 2.0
    rospy.loginfo("Sending x: %s   y: %s",x,y)
    raw_input("Press key to continue")
    i=0
    while i<test_time*f.control_rate:
        f.follow(x,y)
        i += 1
    f.stop_follow()
    print "robot should drive in arc"
    x = 3.0
    y = 1.0
    rospy.loginfo("Sending x: %s   y: %s",x,y)
    raw_input("Press key to continue")
    i=0
    while i<test_time*f.control_rate:
        f.follow(x,y)
        i += 1
    f.stop_follow()
    
    
