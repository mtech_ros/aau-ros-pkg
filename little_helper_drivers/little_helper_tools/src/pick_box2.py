#!/usr/bin/env python

import roslib
roslib.load_manifest("little_helper_tools")

import rospy
import sys

from skill_template2 import BaseSkill2

import little_helper_interface

# QR World State services and messages
import qr_world_state.msg as qr_msg
import qr_world_state.srv as qr_srv

import geometry_msgs.msg as gm

from math import sqrt
import numpy as np


class PickUpBox2(BaseSkill2):

    def __init__(self,pout):
        BaseSkill2.__init__(self,pout)
        #Add pre and postconditions
        rospy.logdebug("-- Instantiating PickUpBox skill")
        rospy.logdebug("Adding preconditions to the skill")
        self.add_precondition("OBJ_KNOWN",self.pre_object_known)
        self.add_precondition("OBJ_BOX",self.pre_object_is_box)
        self.add_precondition("OBJ_IN_REACH",self.pre_object_within_reach)
        rospy.logdebug("Adding postconditions to the skill")
        self.add_postcondition("OBJ_GONE",self.post_object_picked)
        self.add_postcondition("OBJ_IN_GRIP",self.post_object_grasped)
        #Connect to ROS stuff
        self.ros_connect()

        self._box = qr_msg.Observation() #the actual box data, copied from the world model
        self._parameter = 0 # the QR ID of the box
        self._from_rack = False # was the box in the rack

    def ros_connect(self):
        #create the little helper object
        self.lhi = little_helper_interface.LittleHelperInterfaceM39(world_state=True,qr_detector=True)

    def usage(self):
        rospy.logfatal("One, and only one, argument must be given, which is the ID (int) of the object to be picked up in the state_model")
        return False

    def check_parameter(self,param):
        if not type(param)==int:
            return self.usage()
        self._parameter = param
        return True

    def execute(self):
        #Moved from LittleHelperInterface
        rospy.loginfo("Will attempt to pick up a %s box", self._box.value)
        # picking from the rack?
        if self._from_rack: return self.pick_from_rack()
        
        # Detect all QR codes in image
        found_box = False
        codes = self.lhi.detect_qr_codes(add_to_world_state=False)
        for qr in codes:
            #skip if it's not the color we're looking for
            if not qr.data.split(".") == ["BOX", self._box.value]:
                continue
            #skip if it contains invalid pose data
            if qr.pose.pose==gm.Pose():
                rospy.logwarn("Invalid pose data for QR code: %s",qr.data)
                continue
            qr.pose.header.stamp = rospy.Time.now()
            #print qr.pose
            boxpose = self.lhi.transform_pose("/calib_lwr_arm_base_link", qr.pose)
            #print boxpose
            found_box = True
        
        if not found_box:
            rospy.logwarn("Deleting 'BOX."+self._box.value+"' from world model")
            del_req = qr_srv.DestroyObservationRequest()
            del_req.qr_id = self._box.qr_id
            self.lhi.qr_des_srv.call(del_req)
            return self.execution_error("No BOX."+ self._box.value + "detected!")
    
        rospy.loginfo("Moving arm to ready position")
        #if not self.lhi.move_arm(self.lhi.lwr_park_via,"PTP_CART_ABS",0.50).any(): return self.execution_error("Moving the LWR arm failed")
        #self.lhi.move_arm(self.lhi.lwr_park_via,"PTP_CART_ABS",0.50)
        pos_work_grasp = np.array([-124,130,0,-119,-33,-77,10]) # For shelf and table
        if not self.lhi.move_arm(pos_work_grasp,"PTP_JOINT_ABS",0.45).any(): return self.execution_error("Moving the LWR arm failed")
        #print pos

        rospy.loginfo("Grasping box")
        pregrasp_pose, grasp_pose = self.lhi.get_lwr_pose_pickup(boxpose)
        if not self.lhi.move_arm(pregrasp_pose.pose,"BASE_QUAT",0.45).any(): return self.execution_error("Moving the LWR arm failed")
        if not self.lhi.move_arm(grasp_pose.pose,"BASE_LIN_QUAT",0.2).any(): return self.execution_error("Moving the LWR arm failed")
        #return 0 

        ##add it to the gripper
        if not self.lhi.add_box_to_gripper(self._box.value).success: return self.execution_error("Attaching box to gripper failed")
        
        rospy.loginfo("Lifting box")
        #Lift
        depart = np.array([150,0,0,0,0,0])
        if not self.lhi.move_arm(depart,"LIN_REL_TOOL",0.3).any(): return self.execution_error("Moving the LWR arm failed")
        #Retract
        #if not self.lhi.move_arm(np.array([0,0,-150,0,0,0]), "LIN_REL_TOOL", 0.35).any(): return self.execution_error("Moving the LWR arm failed")
        #back up
        #rospy.logdebug("Backing up the robot")
        #self.lhi.back_up(0.20,0.30)
        #rospy.sleep(0.5)
        rospy.loginfo("Parking arm through via points")
        if not self.lhi.move_arm(pos_work_grasp,"PTP_JOINT_ABS",0.50).any(): return self.execution_error("Moving the LWR arm failed")
        #print "Parking arm through via point"
        #if not self.lhi.move_arm(self.lhi.lwr_park_via,"PTP_CART_ABS",0.50).any(): return self.execution_error("Moving the LWR arm failed")
        if not self.lhi.move_arm(self.lhi.lwr_pos_home,"PTP_JOINT_ABS",0.50).any(): return self.execution_error("Moving the LWR arm failed")
       
        return True

    def pick_from_rack(self):
        """modified execute function that picks from the rack"""
        # get the rack the box is at
        rack = self.lhi.box_at_rack(self._box.pose)
        if rack == False: return self.execution_error("Could not determine which rack the box is in")
        # get the locations - in front and above, above and in front of same height
        post2, post1, pre = self.lhi.get_lwr_poses_rack(self._box.pose)
        if False in [post1,post2,pre]: return self.execution_error("Could not get place locations")
        # make sure we're in the right frame
        if not self._box.pose.header.frame_id == "/calib_lwr_arm_base_link":
            self._box.pose = self.lhi.transform_pose("/calib_lwr_arm_base_link",self._box.pose)
        rospy.loginfo("Moving arm to ready position")
        # in front and same height
        if not self.lhi.move_arm(pre.pose,"BASE_QUAT",0.45).any(): return self.execution_error("Moving the LWR arm failed")
        rospy.loginfo("Picking up box")
        # pick location
        self._box.pose.pose.position.y -= 0.005 # to get good grip
        if not self.lhi.move_arm(self._box.pose.pose,"BASE_LIN_QUAT",0.35).any(): return self.execution_error("Moving the LWR arm failed")
        # attach the box to the gripper
        grip_req = qr_srv.AddToGripperRequest()
        grip_req.qr_id = self._box.qr_id
        resp = self.lhi.qr_grip_srv(grip_req)
        # lift
        if not self.lhi.move_arm(post1.pose,"BASE_LIN_QUAT",0.35).any(): return self.execution_error("Moving the LWR arm failed")
        # depart
        if not self.lhi.move_arm(post2.pose,"BASE_QUAT",0.35).any(): return self.execution_error("Moving the LWR arm failed")
        rospy.loginfo("Parking the arm")
        if not self.lhi.move_arm(self.lhi.lwr_pos_home,"PTP_JOINT_ABS",0.50).any(): return self.execution_error("Moving the LWR arm failed")
        return True


    def pre_object_known(self):
        rospy.logdebug("Testing if box is in world state")
        qr_query = qr_srv.QueryObservationsRequest()
        qr_query.qr_ids = []
        qr_query.types = ["BOX"]
        resp = self.lhi.qr_qry_srv.call(qr_query)
        qr_ids = [i.qr_id for i in resp.obs_list]
        if not self._parameter in qr_ids:
            rospy.logwarn("Box ID %s not in world state, possible IDs are %s",self._parameter,qr_ids)
            return False
        else:
            box = [i for i in resp.obs_list if i.qr_id==self._parameter]
            self._box = box[0]
            self._from_rack = "/calib_lwr_arm_base_link" == self._box.pose.header.frame_id
            rospy.loginfo("Object ID %s ('%s.%s') is present in world state",self._box.qr_id,self._box.value,self._box.value)
            return True

    def pre_object_is_box(self):
        rospy.logdebug("Testing if the object is a BOX")
        if not self._box.type == "BOX":
            rospy.logwarn("Object to pick up is not a BOX, but a %s",self._box.type)
            return False
        else:
            rospy.loginfo("Object to pick up is in fact a BOX")
            return True

    def pre_object_within_reach(self):
        rospy.logdebug("Testing if the object is within reach of the arm")
        #self._box.pose.header.stamp = self.lhi.tl.getLatestCommonTime("calib_lwr_arm_base_link", self._box.pose.header.frame_id)
        pos = self.lhi.transform_pose("/calib_lwr_arm_base_link", self._box.pose)
        # distance to shoulder link
        dist = sqrt((pos.pose.position.x)**2 + (pos.pose.position.y)**2 + (pos.pose.position.z)**2)
        if dist > 0.95: #TODO we need better estimate of reach
            rospy.logwarn("Object is apparently out of reach (%s m from arm base link)",dist)
            return False
        else:
            rospy.loginfo("Object within workspace (%s m from arm base link)",dist)
            return True

    def post_object_picked(self):
        if self._from_rack:
            rospy.loginfo("Box picked from rack, so assuming it was okay")
            return True
        rospy.logdebug("Testing if box is still present at pick up location")
        qr_codes = self.lhi.detect_qr_codes(add_to_world_state=False)
        box_string = self._box.type + '.' + self._box.value
        if box_string in [i.data for i in qr_codes]: #box detected
            rospy.logwarn("The box was detected, so I'm guessing it's not picked up")
            # since we already added it to the gripper, we need to revert that
            box_code = [i for i in qr_codes if i.data==box_string]
            #box_id = self.lhi.get_qr_id(self._box.type,self._box.value,no_warning=True)
            #update pose
            req = qr_srv.UpdatePoseRequest(qr_id=self._box.qr_id,pose=box_code[0].pose)
            self.lhi.qr_upd_srv(req)
            return False
        else:
            rospy.loginfo("Seems like the box is no longer at the previous location")
            return True

    def post_object_grasped(self):
        #TODO maybe add some stuff that check the force sensors?
        rospy.logdebug("Querying for box in gripper")
        qry_obj = qr_srv.QueryObservationsRequest()
        qry_obj.types.append("BOX")
        qry_obj.values = []
        qry_obj.location = []
        qry_obj.in_gripper = [True] # This field is the important one
        resp = self.lhi.qr_qry_srv(qry_obj).obs_list
        if not len(resp) == 1: 
            rospy.logwarn("According to the world state there is no box in the gripper!")
            return False
        else:
            rospy.loginfo("%s.%s is currently in the gripper",resp[0].type,resp[0].value)
            return True
        return True
        
    def move_failed(self):
        rospy.logerr("Moving the LWR arm failed!")
        return False


def test_pout(line):
    #only for testing the skill
    print "PIPE: " + line
    
if __name__=='__main__':
    rospy.init_node("pick_up_box_skill",log_level=rospy.DEBUG)
    rospy.sleep(0.5)
    skill = PickUpBox2(test_pout)
    skill.run_skill(int(sys.argv[1]))
