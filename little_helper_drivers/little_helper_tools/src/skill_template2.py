
import roslib; roslib.load_manifest("skill_library_common")

import rospy
import condition


class BaseSkill2:
    """ Base class for all skills.
    """
    
    def __init__(self,pout):
        """This init function should not be edited - instead create custom __init__ for your skill implementation"""
        self.pout = pout
        self.pre = []
        self.post = []
    
    def execute(self):
        """Placeholder function for execution method - do not add anything in this base class. 
        
        Instead fill in in actual implementation of a skill. The execute function should perform the following:
        Check that the parameter is right format
        Create service and action clients, and wait for the servers to be ready
        Validate preconditions
        Execute the actual motion primitives
        Validate postconditions"""
        pass
    
    def check_parameter(self,parameter):
        """Placeholder to check if a parameter is of correct type, when supplied to the skill"""
        pass
    
    def run_skill(self,parameter):
        """ Complete function that executes all the steps in the skill """

        self.start = rospy.Time.now() # The start time

        rospy.loginfo("====================================")
        rospy.loginfo(" Starting the %s skill  ",self.__class__.__name__)
        rospy.loginfo(" Parameter(s): %s        ",parameter)
        rospy.loginfo("====================================")

        ### INITIAL CHECKS ###
        if not self.check_parameter(parameter):
            return self.skill_failed("Parameters are not accurate")

        ### PRECONDITION CHECK ###
        #printout
        rospy.logdebug("%s skill preconditions (%s total):",self.__class__.__name__,len(self.pre))
        for n in self.pre:
            n.printout("debug")
        #test them
        rospy.loginfo("--- Testing all preconditions:")
        pre = self.evaluate_all_preconditions()
        if not pre==True:
            return self.skill_failed("There are failed preconditions!")
        else:
            rospy.loginfo("All preconditions passed")

        ### EXECUTE ###
        rospy.loginfo("--- Running execute() function on %s skill:",self.__class__.__name__)
        if not self.execute():
            return self.skill_failed("Execution failed!")
        else:
            rospy.loginfo("No execution errors in %s",self.__class__.__name__)

        ### POSTCONDITION CHECKS ###
        #printout
        rospy.logdebug("%s skill preconditions (%s total):",self.__class__.__name__,len(self.post))
        for n in self.post:
            n.printout("debug")
        #test them
        rospy.loginfo("--- Testing all postconditions:")
        post = self.evaluate_all_postconditions()
        if not post==True:
            return self.skill_failed("There are failed postconditions!")
        else: 
            rospy.loginfo("All postconditions passed")

        #Hooray
        rospy.loginfo("The %s skill executed as expected in %s!",self.__class__.__name__,(rospy.Time.now()-self.start).to_sec())
        return True
        
    def skill_failed(self,why):
        rospy.logfatal("Skill failed after %ss: %s",(rospy.Time.now()-self.start).to_sec(),why)
        return False #True when debugging
        
    def execution_error(self,error):
        rospy.logerr("Execution error: %s",error)
        return False #True when debugging
    
    def add_precondition(self,description,testfunction):
        """Method to append a precondition"""
        self.pre.append(condition.Condition("Pre",description,testfunction))
        rospy.logdebug("Added Precondition: '%s'",description)
      
    def add_postcondition(self,description,testfunction):
        """Method to append a postcondition"""
        self.post.append(condition.Condition("Post",description,testfunction))
        rospy.logdebug("Added Postcondition: '%s'",description)

    def evaluate_all_preconditions(self):
        rospy.logdebug("Evaluating a total of %s preconditions",len(self.pre))
        passed = True
        for pre in self.pre:
            eval = pre.evaluate()
            if not eval:
                rospy.logerr("Precondition '%s' failed",pre.info)
                self.pout("Precond. failed: " + pre.info)
                rospy.sleep(0.5)
                passed = False
                return False
            else:
                rospy.logdebug("Precondition '%s' passed",pre.info)
        return passed
    
    def evaluate_all_postconditions(self):
        rospy.loginfo("Evaluating a total of %s postconditions",len(self.post))
        passed = True
        for post in self.post:
            eval = post.evaluate()
            if not eval:
                rospy.logerr("Postcondition '%s' failed",post.info)
                self.pout("Postcond. failed: " + post.info);
                rospy.sleep(0.5)
                passed = False
                return False
            else:
                rospy.logdebug("Postcondition '%s' passed",post.info)
        return passed
    
    
