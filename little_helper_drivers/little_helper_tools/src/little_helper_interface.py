#!/usr/bin/env python

import roslib
roslib.load_manifest("little_helper_tools")

import rospy

import numpy as np
import math

import tf
import actionlib
import actionlib_msgs

import fri_communication.msg as fri_msg
import wsg_communication.msg as wsg_msg
import move_base_msgs.msg as move_msg
import geometry_msgs.msg as gm
import sensor_msgs

# QR World State services and messages
import qr_world_state.msg as qr_msg
import qr_world_state.srv as qr_srv
# QR pose detector service and messages
import qr_pose_detector.msg as qr_pose_msg
import qr_pose_detector.srv as qr_pose_srv

class LittleHelperInterface():
    """
    This class enables easy control of the Little Helper, so the user will only have to instantiate this, and call the included helper functions as needed
    """
    
    def __init__(self):
        self.global_timeout = 2.0 #Global timeout for waiting for action servers
        rospy.logdebug("Little Helper is ready for work...")
        
        #create tf stuff
        self._tl = tf.TransformListener()
        
        #set zero tool
        self.set_no_tool()
    
    def move_arm(self, position, motiontype, velocity=0.5):
        """
        moves the LWR arm to a joint or cartesian goal position, depending on the motion type
        """
        
              
        motiondict = {"PTP_JOINT_ABS": 1,  # PTP motion in absolute joint angles
                      "PTP_JOINT_REL": 2,  # PTP motion in relative joint angles
                      "PTP_CART_ABS":  3,  # PTP motion in abolute cartesian coordinate system (BASE)
                      "PTP_CART_REL":  4,  # PTP motion in relative cartesian coordinates (in BASE coordinate system)
                      "LIN_ABS":       5,  # LIN motion in abolute cartesian coordinate system (BASE)
                      "LIN_REL_BASE":  6,  # LIN motion in relative cartesian coordinates (in BASE coordinate system)
                      "LIN_REL_TOOL":  7,  # LIN motion in relative cartesian coordinates (in TOOL coordinate system)
                      "BASE_QUAT":     8,  #same as PTP_CART_ABS, but takes a pose as argument
                      "BASE_LIN_QUAT": 9}  #same as LIN_ABS, but takes a pose as argument
        
        if motiontype not in motiondict:
            print "Motiontype",motiontype,"unknown!"
            return np.array(False)
        
        rospy.logdebug("MOVING ROBOT ARM with motion type %s",motiontype)
        
        #print 'Creating action move_arm_client'
        client = actionlib.SimpleActionClient('move_arm_action', fri_msg.MoveArmAction)
    
        if not client.wait_for_server(rospy.Duration(self.global_timeout)):
            rospy.logwarn("LWR action server not responding after %s seconds",self.global_timeout)
            return np.array(False)
        else:
            rospy.logdebug("LWR action client ready")
                       
               
        motionnumber = motiondict[motiontype]
        
        # Set the move parameters correctly
        if motionnumber == 1:  # PTP_JOINT_ABS
            q = sensor_msgs.msg.JointState()
            q.name = ['lwr_arm_0_joint', 'lwr_arm_1_joint', 'lwr_arm_2_joint', 'lwr_arm_3_joint', 'lwr_arm_4_joint', 'lwr_arm_5_joint', 'lwr_arm_6_joint']
            q.position = [math.radians(position[0]),math.radians(position[1])-math.radians(90.0),math.radians(position[2]),math.radians(position[3]),math.radians(position[4]),math.radians(position[5]),math.radians(position[6]) ]
    
            goal = fri_msg.MoveArmGoal(joint_state=q, KRLMotionType=motionnumber, velocity_scale=velocity, trig_by_contact_threshold=0.0)
        
        elif motionnumber == 2: # PTP_JOINT_REL - compensate for the -90 in joint 2 if relative motion
            q = sensor_msgs.msg.JointState()
            q.name = ['lwr_arm_0_joint', 'lwr_arm_1_joint', 'lwr_arm_2_joint', 'lwr_arm_3_joint', 'lwr_arm_4_joint', 'lwr_arm_5_joint', 'lwr_arm_6_joint']
            q.position = [math.radians(position[0]),math.radians(position[1])-math.radians(90.0),math.radians(position[2]),math.radians(position[3]),math.radians(position[4]),math.radians(position[5]),math.radians(position[6]) ]
    
            goal = fri_msg.MoveArmGoal(joint_state=q, KRLMotionType=motionnumber, velocity_scale=velocity, trig_by_contact_threshold=0.0)
    
        elif motionnumber == 8: # BASE_QUAT PTP motion
            
            #assert position==gm.Pose(), return
            p = position.position
            desiredTgoal = gm.Transform(translation=gm.Vector3(x=p.x, y=p.y, z=p.z),
                                        rotation=position.orientation)
                
            goal = fri_msg.MoveArmGoal(transform=desiredTgoal, KRLMotionType=3, velocity_scale=velocity, trig_by_contact_threshold=0.0)
                
        elif motionnumber == 9: # BASE_QUAT LIN motion
            
            #assert position==gm.Pose(), return
            p = position.position
            desiredTgoal = gm.Transform(translation=gm.Vector3(x=p.x, y=p.y, z=p.z),
                                        rotation=position.orientation)
                
            goal = fri_msg.MoveArmGoal(transform=desiredTgoal, KRLMotionType=5, velocity_scale=velocity, trig_by_contact_threshold=0.0)
        
        
        elif motionnumber >= 3: # The rest of the motion types are in cartesian coordinates
            
            q = tf.transformations.quaternion_from_euler(math.radians(position[3]), math.radians(position[4]), math.radians(position[5]),'rzyx')
            desiredTgoal = gm.Transform(translation=gm.Vector3(x=position[0]/1000.0, y=position[1]/1000.0, z=position[2]/1000.0),
                                        rotation=gm.Quaternion(x=q[0],y=q[1],z=q[2],w=q[3]))
                
            goal = fri_msg.MoveArmGoal(transform=desiredTgoal, KRLMotionType=motionnumber, velocity_scale=velocity, trig_by_contact_threshold=0.0)
        
        else:
            rospy.logerr("Not a valid motiontype: %s",motiontype)
            return np.array(False)
    
    
        # Sends the goal to the action server.
        #print 'Sending goal to server'
        client.send_goal(goal)
    
        # Waits for the server to finish performing the action.
        #print 'Waiting for result'
        if not client.wait_for_result(rospy.Duration.from_sec(60.0)):
            rospy.logerr("Arm not moved after 60s, aborting")
            return np.array(False)
    
        # Get the actual position obtained
        lwrstate = rospy.wait_for_message("/lwr_data", fri_msg.LwrData) # Get a single message from topic
        if motionnumber >= 3: #Cartesian motion
            #msrCartPos
            #msrCartPosFriOffset 
            # This is z,y,-x in m and rad
            t = np.array(lwrstate.msrCartPos) - np.array(lwrstate.cmdCartPosFriOffset)
            #print lwrstate.cmdCartPos
            #print lwrstate.cmdCartPosFriOffset
            # But we want it as xyz in mm and deg
            # TODO: Check with Oluf to see if this is correct
            result = np.array([-t[2]*1000,
                                t[1]*1000,
                                t[0]*1000,
                                t[3]*180/math.pi,
                                t[4]*180/math.pi,
                                t[5]*180/math.pi]) # Not sure about this one
                                #t[5]*180/math.pi-270]) # Not sure about this one
            #print result
        elif motionnumber <= 2:
            t = np.array(lwrstate.msrJntPos) - np.array(lwrstate.cmdJntPosFriOffset)
            result = t # Todo: convert this to degrees
        
    
        # Prints out the result of executing the action
        #return client.get_result()  # A Result
        return result
    
    def set_no_tool(self):
        """Sets the TCP internal on the KUKA controller to be the last link on the actual arm (i.e. no tool)
        """
        client = actionlib.SimpleActionClient('set_tool_action', fri_msg.SetToolAction)
        rospy.sleep(0.1)
        goal = fri_msg.SetToolGoal()
        goal.transform = gm.Transform()
        goal.transform.rotation.w = 1.0 #to get zero rotation...
        goal.cog = gm.Point()
        goal.mass = 0.0
        client.send_goal(goal)
        rospy.sleep(0.1)
    
    def move_gripper(self,width,velocity=20.0):
        """
        Moves the gripper to a specified width [0-110 mm] with optional specified velocity [1-100]
        """
        
        gripper_client = actionlib.SimpleActionClient("/gripper_action",wsg_msg.GripperAction)
        if not gripper_client.wait_for_server(rospy.Duration(self.global_timeout)):
            rospy.logwarn("Gripper action server not responding after %s seconds",self.global_timeout)
        else:
            rospy.logdebug("Gripper action client ready")
        gripper_goal = wsg_msg.GripperGoal()
        
        rospy.logdebug("Moving gripper to width %s with velocity %s",width,velocity)
    
        gripper_goal.goalID = rospy.Time.now().secs #Just use the timestamp for unique command ID
        gripper_goal.CommandID = 0x21 # MOVE command ID
        gripper_goal.speed = velocity
        gripper_goal.width = width
        gripper_client.send_goal(gripper_goal)
        gripper_client.wait_for_result(rospy.Duration(10.0))
        res = gripper_client.get_result()
        if res.state==0:
            rospy.logdebug("Gripper moved")
            return res.wsg_width
        else:
            rospy.logerr("Gripper failed to move, actual position is %s, state is %s",res.wsg_width,res.state)
            return 
    
    def move_base_navigation(self,pose):
        """
        Sends a goal to the navigation, for platform motion
        """
        #print("THE NAV GOAL:"); print("%s"%pose) # @vx 
        #pose.pose.orientation.x = 0   # @vx nav correction - this is nonsense  
        #pose.pose.orientation.y = 0
        #pose.pose.orientation.z = 1
        #print("THE NAV GOOL REFINED:"); print("%s"%pose) # @vx
        #create action client
        nav_client = actionlib.SimpleActionClient('/move_base', move_msg.MoveBaseAction)
        if not nav_client.wait_for_server(rospy.Duration(self.global_timeout)):
            rospy.logwarn("Navigation action server not responding after %s seconds",self.global_timeout)
            return False
        else:
            rospy.logdebug("Navigation action client ready")
        nav_goal = move_msg.MoveBaseGoal()
        # cancel current goal
        rospy.logdebug("Cancelling current navigation goal, too bad if you were going anywhere!")
        nav_client.cancel_all_goals()
        
        rospy.logdebug("Sending navigation goal to platform")
        nav_goal.target_pose = pose
        nav_client.send_goal(nav_goal)
        nav_client.wait_for_result() # wait indefinitely...
        res = nav_client.get_state()
        if not res == actionlib_msgs.msg.GoalStatus.SUCCEEDED:
            rospy.logerr("Apparently did not succeed in navigating, return is %s",res)
            return False
        else: 
            rospy.logdebug("Platform has reached the desired destination")
            return True

    
    def transform_pose(self,target,pose,retry_duration=2.0):
        """ More stable version of tf.transformPose (will keep retrying for retry_duration seconds if it fails) 
        Transforms a PoseStamped into the target frame"""  
        working=False
        start = rospy.Time.now()
        while not working and not rospy.is_shutdown():
            try:
                if rospy.Time.now()-start>rospy.Duration(retry_duration): #stop trying
                    rospy.logerr("Could not transform pose from %s to %s",pose.header.frame_id,target)
                    return False
                rospy.logdebug("Trying to transform from %s to %s",pose.header.frame_id,target)
                pose.header.stamp = self._tl.getLatestCommonTime(target,pose.header.frame_id)
                rospy.logdebug("- Got common timestamp")
                self._tl.waitForTransform(target,pose.header.frame_id,pose.header.stamp,rospy.Duration(1.0))
                rospy.logdebug("- Got transform")
                outpose = self._tl.transformPose(target, pose)
                rospy.logdebug("- Transformed pose")
                working=True
                rospy.logdebug("Done!")
            except Exception as e:
                rospy.logwarn("TF error when trying to transform pose, retrying")
                working=False
                rospy.sleep(0.1)
        return outpose
        
class LittleHelperInterfaceM39(LittleHelperInterface):
    """
    This version of the Little Helper Interface contains useful functions for the M39 demo
    All functions are inherited from LittleHelperInterface
    """
    def __init__(self,world_state=False,qr_detector=False):
        """
        Set keywords to true on instantiation, depending on your needs
        """
        self.global_timeout = 2.0 #Global timeout for waiting for action servers
        
        #old poses
        self.lwr_park_via = [475,320,290,90,-90,100] # via point (cart) for parking the arm
        #self.lwr_pos_home = [-160,125,-35,-115,115,-85,-35] # Home/parked joint pose
        
        #new home position for rack - which doesn't work...
        #self.lwr_pos_home = [-148,136,-12,-101.5,110.5,-100.5,-34]
        #let's try with this one...
        self.lwr_pos_home = [-121.5,149,-45,-101.5,131,-78.5,-47]
        
        #new, bad poses
        #self.lwr_park_via = [-370,255,450,-95,-90,100] # via point (cart) for parking the arm
        #self.lwr_pos_home = [86,110,-24,-106,-90,-105,50] # Home/parked joint pose
        
        #create tf broadcaster and listener
        self._tl = tf.TransformListener()
        self._tb = tf.TransformBroadcaster()
        
        #set zero tool
        #self.set_no_tool()
                
        if world_state:
#            print "Waiting for QR World State service"
#            if not rospy.wait_for_service("/qr_world_state/add_observation", self.global_timeout):
#                rospy.logerr("Could not connect to qr_world_state services, make sure the node is running")
            self.qr_add_srv = rospy.ServiceProxy("/qr_world_state/add_observation",qr_srv.AddObservation)
            self.qr_qry_srv = rospy.ServiceProxy("/qr_world_state/query_observations",qr_srv.QueryObservations)
            self.qr_upd_srv = rospy.ServiceProxy("/qr_world_state/update_pose",qr_srv.UpdatePose)
            self.qr_grip_srv= rospy.ServiceProxy("/qr_world_state/add_to_gripper",qr_srv.AddToGripper)
            self.qr_des_srv = rospy.ServiceProxy("/qr_world_state/destroy_observation",qr_srv.DestroyObservation)
            self.qr_drw_srv = rospy.ServiceProxy("/qr_world_state/draw_world_state",qr_srv.DrawWorldState)
            self.qr_place_srv = rospy.ServiceProxy("/qr_world_state/get_place_locations",qr_srv.GetPlaceLocations)
            self.qr_empty_srv = rospy.ServiceProxy("/qr_world_state/set_empty_state",qr_srv.SetEmptyState)
        if qr_detector:
#            print "Waiting for QR pose detector service"
#            if not rospy.wait_for_service("/qr_pose_detector", self.global_timeout):
#                rospy.logerr("Could not connect to qr_pose_detector service, make sure the node is running")
            self.qr_detect_srv = rospy.ServiceProxy("/qr_pose_detector",qr_pose_srv.DetectQRCodes)
            
        rospy.logdebug("Little Helper is ready for working on the M39 demo...")
        #self._tl.allFramesAsString()
    
    def back_up(self,vel,dist):
        """ Tell the platform to back up a little
        @vel: velocity in m/s
        @dist: distance in m
        """
        pub = rospy.Publisher('/cmd_vel',gm.Twist)
        rospy.sleep(1.0) # so we're sure the publisher is connected
        rospy.logdebug("Backing up platform, %s m with %s m/s",dist,vel)
        move = gm.Twist()
        move.linear.x = -vel 
        pub.publish(move)
        rospy.sleep(dist/vel)
        rospy.logdebug("Stopping platform")
        pub.publish(gm.Twist())
        return
    
    def get_qr_id(self,qtype,qvalue,no_warning=False):
        """ Get QR id by type and value
        Type: "BOX", "SHELF", etc
        Value: "BLUE","1", etc
        Type or Value can be empty string, in which case any type/value is used, can also be array
        Returns QueryObservationResponse
        """
        rospy.logdebug("Querying all observations of type %s and value %s in the world",qtype,qvalue)
        qry_obj = qr_srv.QueryObservationsRequest()
        qry_obj.types = []
        if not qtype=="": 
            if type(qtype)==list: qry_obj.types += qtype
            if type(qtype)==str: qry_obj.types.append(qtype)
        qry_obj.values = []
        if not qvalue=="":
            if type(qvalue)==list: qry_obj.values += qvalue
            if type(qvalue)==str: qry_obj.values.append(qvalue)
        qry_obj.location = []
        qry_obj.in_gripper = []
        resp = self.qr_qry_srv(qry_obj).obs_list
        if len(resp)==0: 
            if not no_warning: rospy.logwarn("No objects that match the query!")
            return -1,-1
        rospy.logdebug("There are %s objects that match the query",len(resp))
        return resp
    
    def add_box_to_gripper(self,boxcolor):
        """
        Adds a box already in the world state to the gripper
        Should be called immediately when grasping the box, to get right coordinates
        """
        #Get id of the box color
        boxid = self.get_qr_id("BOX",boxcolor.upper())[0].qr_id
        # Add to gripper
        rospy.logdebug("Adding box to gripper")
        grip_req = qr_srv.AddToGripperRequest()
        grip_req.qr_id = boxid
        resp = self.qr_grip_srv(grip_req)
        return resp
    
    def remove_from_gripper(self):
        """
        Tells the world state that there is no box in the gripper
        """
        rospy.logdebug("Querying for box in gripper")
        qry_obj = qr_srv.QueryObservationsRequest()
        qry_obj.types.append("BOX")
        qry_obj.values = []
        qry_obj.location = []
        qry_obj.in_gripper = [True] # This field is the important one
        resp = self.qr_qry_srv(qry_obj).obs_list
        if not len(resp) == 1: 
            rospy.logwarn("Tried to detach box from the gripper that wasn't there")
            return False
    
        box = resp[0]    
        obs_req = qr_srv.AddObservationRequest()
        obs_req.type = box.type
        obs_req.value = box.value
        obs_req.empty = [box.empty]
        #obs_req.in_gripper = False # Not necessary (or allowed)
        obs_req.pose = box.pose
        resp = self.qr_add_srv.call(obs_req)
        rospy.logdebug("Detached box from gripper: %s",box.value)
        return True
    
    def set_empty_state(self,qr_id,state):
        req = qr_srv.SetEmptyStateRequest()
        req.qr_id = qr_id
        req.empty = state
        resp = self.qr_empty_srv.call(req)
        return resp.success
    
    
    def get_free_shelf(self,shelfnumber):
        """
        Gets a free shelf, based on information in the world state
        shelfnumber is the desired free shelf
        Returns ID and number of the shelf
        """
        # Query all boxes, so we have their locations
        resp_box = self.get_qr_id("BOX","")
        
        # Do the same for shelfs
        resp_shelf = self.get_qr_id("SHELF","")
        
        #print resp.obs_list
        mx = 0.10 #distance from box to shelf marker
        my = 0.10 #distance from box to shelf marker
        mz = 0.40 #vertical distance from box to shelf marker
        free_id = -1
        free_value = "none"
        
        for shelf in resp_shelf.obs_list:
            shelf.empty = True
            
        for shelf in resp_shelf.obs_list:
            sx = shelf.pose.pose.position.x
            sy = shelf.pose.pose.position.y
            sz = shelf.pose.pose.position.z
            for box in resp_box.obs_list:
                bx = box.pose.pose.position.x
                by = box.pose.pose.position.y
                bz = box.pose.pose.position.z
                #print bz + dist - sz
                dx = np.abs(bx - sx)
                dy = np.abs(by - sy)
                dz = sz - bz
                if dx < mx and dy < my and dz < mz:
                    print "Occupied shelf", shelf.value, box.value
                    shelf.empty = False
    
        print "shelfnumber:",shelfnumber
        for shelf in resp_shelf.obs_list:
            print "Checking shelf:"
            print shelf.qr_id
            print shelf.value
            print shelf.empty
            if shelf.empty == True:
                print "Free shelf:", shelf.qr_id
                if int(shelf.value) == int(shelfnumber): #requested_shelf.split(".")[1]:
                    print "Requested shelf available"
                    free_value = shelf.value
                    return shelf.qr_id, free_value
                else:
                    free_id = shelf.qr_id
                    free_value = shelf.value
    
        return free_id, free_value
    
    def is_location_empty(self,location):
        """
        Determines if a PoseStamped location is occupied by a box
        """
        # Query all boxes, so we have their locations
        boxes = self.get_qr_id("BOX","")
        #thresholds
        mx = 0.10 #distance from box to shelf marker
        my = 0.10 #distance from box to shelf marker
        mz = 0.40 #vertical distance from box to shelf marker
        empty = True
        #print boxes
        for box in boxes:
            boxp = self.transform_pose(location.header.frame_id,box.pose)
            bx = boxp.pose.position.x
            by = boxp.pose.position.y
            bz = boxp.pose.position.z
            dx = np.abs(bx - location.pose.position.x)
            dy = np.abs(by - location.pose.position.y)
            dz = np.abs(bz - location.pose.position.z)
            if dx < mx and dy < my and dz < mz:
                rospy.logdebug("Location is occupied by the %s box",box.value)
                empty = False
                break
        #print "Location is not occupied by box"
        return empty
            
    
    def drive_to_id(self,id, dist=1.0,offset=0.0):
        """
        Queries world model for pose of specific ID, then transforms this into the map frame and sends goal dist in from of the code
        dist: distance in m in front of the QR code
        offset: distance in m right of the qr code (can be negative)
        """
        rospy.logdebug("Driving the robot to QR id %s",id)
        #query world model for qr
        rospy.logdebug("Querying the world for pose")
        qry_req = qr_srv.QueryObservationsRequest()
        qry_req.qr_ids = [id] 
        qry_req.in_gripper = [False]
        resp = self.qr_qry_srv(qry_req)
        if not len(resp.obs_list) == 1:
            rospy.logerr("None or multiple objects match query, quitting")
            print resp.obs_list
            return False
        else:
            qr_pose = resp.obs_list[0].pose
            rospy.logdebug("Got pose in %s frame",qr_pose.header.frame_id)
        #relative goal pose
        goal_rel = gm.Pose()
        goal_rel.position.x =  offset # sideways offset wrt the QR code
        goal_rel.position.z =  dist   # distance in front of the QR code
        g_q = tf.transformations.quaternion_from_euler(0,math.pi/2.0,0) 
        goal_rel.orientation = gm.Quaternion(g_q[0],g_q[1],g_q[2],g_q[3])
        # get the absolute goal pose
        goal_pose = self.relative_pose(qr_pose,goal_rel,"/map",timeout=0.01)
        goal_pose = self.relative_pose(qr_pose,goal_rel,"/map")
        if not goal_pose: return False
        #clean up goal orientation
        
        q = goal_pose.pose.orientation
        rot_ang = tf.transformations.euler_from_quaternion([q.x,q.y,q.z,q.w])
        print "goal pos ang:",rot_ang # @vx - look at this 
        align_q = tf.transformations.quaternion_from_euler(0,0,rot_ang[2])
        goal_pose.pose.orientation = gm.Quaternion(align_q[0],align_q[1],align_q[2],align_q[3])
        #send goal to action server
        return self.move_base_navigation(goal_pose)
        
    def detect_qr_codes(self,add_to_world_state=True):
        """
        Calls the QR pose detector service, and returns the detected QR codes
        Optionally adds the detection to the world state (default True)
        """
        rospy.logdebug("Detecting QR codes")
        req = qr_pose_srv.DetectQRCodesRequest()
        detect_resp = self.qr_detect_srv(req)
        rospy.logdebug("Found %s QR Codes", len(detect_resp.qr_codes))
        all_codes = [c.data for c in detect_resp.qr_codes]
        if add_to_world_state and hasattr(self, 'qr_add_srv'):
            for code in detect_resp.qr_codes:
                codedata = code.data.split(".")
                obs_req = qr_srv.AddObservationRequest()
                obs_req.type = codedata[0]#e.g. "BOX"
                obs_req.value = codedata[1]#e.g."RED"
                if len(codedata) == 3: #do we have a width encoded?
                    obs_req.width = int(codedata[2])
                if code.pose.pose == gm.Pose():
                    rospy.logwarn("Invalid pose data for QR code %s, not adding to world model",code.data)
                    continue
                obs_req.pose = code.pose
                obs_req.pose.header.stamp = rospy.Time.now()
                # assume boxes full or empty
                if codedata[0]=="BOX":
                    #if we saw the workstation, we assume all the boxes are full
                    if "WORKSTATION.RED.800" in all_codes: 
                        obs_req.empty = [False]
                    #if we saw the shelf for empty boxes, we assume all boxes are empty
                    elif "SHELF.BLUE.900" in all_codes:
                        obs_req.empty = [True]
                    #if we didn't see any of the above we have to assume the boxes are empty
                    else:
                        obs_req.empty = [True]
                add_resp = self.qr_add_srv(obs_req)
            #draw the world state every time we add to it
            self.qr_drw_srv(qr_srv.DrawWorldStateRequest())
        return detect_resp.qr_codes
    
    def add_qr_to_world_state(self,codes):
        """Adds (single or list of) detection from qr pose detector to the world state"""
        if type(codes)==list:
            for qr in codes:
                codedata = qr.data.split(".")
                obs_req = qr_srv.AddObservationRequest()
                obs_req.type = codedata[0]#e.g. "BOX"
                obs_req.value = codedata[1]#e.g."RED"
                if len(codedata) == 3: #do we have a width encoded?
                    obs_req.width = int(codedata[2])
                obs_req.pose = qr.pose
                obs_req.pose.header.stamp = rospy.Time.now()
                add_resp = self.qr_add_srv(obs_req)
        elif type(codes)==qr_pose_msg.QRCode:
            codedata = codes.data.split(".")
            obs_req = qr_srv.AddObservationRequest()
            obs_req.type = codedata[0]#e.g. "BOX"
            obs_req.value = codedata[1]#e.g."RED"
            if len(codedata) == 3: #do we have a width encoded?
                obs_req.width = int(codedata[2])
            obs_req.pose = codes.pose
            obs_req.pose.header.stamp = rospy.Time.now()
            add_resp = self.qr_add_srv(obs_req)
        else:
            rospy.logerr("Not a QR code: Type '%s' was supplied",type(codes))
        return
        
      
    def get_place_box_locations(self,surface,surf_pose):
        """
        Gets the place locations for a small box in the LWR frame. Surface should be visible!
        surface: designation of surface, e.g. "TABLE.BLUE"
        surf_pose: detected pose of the surface
        """
        surf = surface.split(".")
        surf_id = self.get_qr_id(surf[0],surf[1],no_warning=True)[0].qr_id
        req = qr_srv.GetPlaceLocationsRequest()
        req.qr_id = surf_id
        req.surface_pose = self.transform_pose("/calib_lwr_arm_base_link",surf_pose)
        req.frame_id = "/calib_lwr_arm_base_link"
        req.obj_width = 0.15
        req.clearance = 0.04
        req.side_clearance = 0.05
        resp = self.qr_place_srv(req)
        rospy.logdebug("Found %s place locations on surface %s, in frame %s",len(resp.places.locations),surface,resp.places.locations[0].header.frame_id)
        return resp.places.locations
        
    
    def relative_pose(self,fixed_pose,relative_pose,output_frame,timeout=5.0):
        """
        Calculates a pose relative to another pose, in a specific frame
        fixed_pose: pose used for the reference (PoseStamped)
        relative_pose: the pose relative to fixed_pose we want to transform (Pose)
        output_frame: frame to transform the calculated relative_pose into (string)
        """
        #transform into output_frame
        fixed_pose.header.stamp = rospy.Time()
        working=False
        start = rospy.Time.now()
        retry_duration = rospy.Duration(timeout)
        while not working:
            try:
                rospy.logdebug("Trying to transform")
                if rospy.Time.now()-start>retry_duration:
                    rospy.logwarn("Could not transform pose within %ss",retry_duration.secs)
                    return False
                self._tl.waitForTransform(output_frame,fixed_pose.header.frame_id,fixed_pose.header.stamp,rospy.Duration(1.0))
                fixed_pose = self._tl.transformPose(output_frame, fixed_pose)
                working = True
            except tf.Exception as e:
                rospy.logwarn("TF error when trying to transform pose, retrying")
                working=False
        #create frame on fixed_pose
        q = fixed_pose.pose.orientation
        now = rospy.Time.now()
        self._tb.sendTransform([fixed_pose.pose.position.x,fixed_pose.pose.position.y,fixed_pose.pose.position.z], 
                         [q.x,q.y,q.z,q.w],
                         now,
                         "/temp_frame",
                         fixed_pose.header.frame_id)
        rospy.logdebug("Published fixed frame")
        #create relative pose in the fixed frame
        rel_pose = gm.PoseStamped()
        rel_pose.header.frame_id = "/temp_frame"
        rel_pose.pose = relative_pose
        #transform it into the output frame
        working=False
        start = rospy.Time.now()
        while not working:
            try:
                if rospy.Time.now()-start>retry_duration:
                    rospy.logwarn("Could not transform pose within %ss",retry_duration.secs)
                    return False
                rospy.logdebug("Waiting for transform to %s",output_frame)
                self._tl.waitForTransform(output_frame,"/temp_frame",now,rospy.Duration(1.0))
                rospy.logdebug("Getting common timestamp")
                rel_pose.header.stamp = self._tl.getLatestCommonTime(output_frame,"/temp_frame")
                rospy.logdebug("Transforming pose")
                out_pose = self._tl.transformPose(output_frame, rel_pose) # full position and orientation, might not work
                rospy.logdebug("Succesfully transformed pose")
                working=True
            except tf.Exception as e:
                rospy.logwarn("TF error when trying to transform pose, retrying")
                working=False
        return out_pose
    
    def get_lwr_pose_pickup(self,boxpose,approach=0.20):
        """
        Calculates approach and grasp pose of the boxgripper, from the supplied boxpose and approach distance (in front of box)
        Returns pregrasp,grasp PoseStamped
        """
        #relative grasp pose
        # small box
        grasp_rel = gm.Pose()
        grasp_rel.position.y =  0.005 # height offset TCP to boxgripper
        grasp_rel.position.z =  0.08 # depth offset TCP to boxgripper
        grasp_rel.position.x = -0.015 # sideways offset (but why?)
        ## Old code (for bigger box) ###
        #grasp_rel.position.y = 0.015 #height offset TCP to boxgripper
        #grasp_rel.position.z = 0.075 #depth offset TCP to boxgripper
        g_q = tf.transformations.quaternion_from_euler(math.pi,0.0,math.pi/2.0) #rotation of gripper wrt box QR
        grasp_rel.orientation = gm.Quaternion(g_q[0],g_q[1],g_q[2],g_q[3])
        # get the absolute grasp pose
        grasp = self.relative_pose(boxpose,grasp_rel,"/calib_lwr_arm_base_link")
        # relative pregrasp pose
        pregrasp_rel = grasp_rel
        pregrasp_rel.position.z += approach
        pregrasp = self.relative_pose(boxpose,pregrasp_rel,"/calib_lwr_arm_base_link")
        
        return pregrasp,grasp
        

    def get_lwr_pose_place_table(self,tablepose,approach=0.15):
        """
        Calculates approach and final pose of the boxgripper, from the supplied tablepose and approach distance (above table)
        Returns preplace,place PoseStamped
        """
        #relative place pose
        # small box
        place_rel = gm.Pose()
        place_rel.position.y =  0.06 + 0.105 +0.005# box + table + adjustment
        # In the height we add distance from Box QR to bottom of box, and from Table QR to table plane
        place_rel.position.z =  0.075-0.01 # depth offset TCP to boxgripper
        p_q = tf.transformations.quaternion_from_euler(math.pi,0.0,math.pi/2.0) #rotation of gripper wrt box QR
        place_rel.orientation = gm.Quaternion(p_q[0],p_q[1],p_q[2],p_q[3])
        # get the absolute place pose
        place = self.relative_pose(tablepose,place_rel,"/calib_lwr_arm_base_link")
        # relative preplace pose
        preplace_rel = place_rel
        preplace_rel.position.y += approach
        preplace = self.relative_pose(tablepose,preplace_rel,"/calib_lwr_arm_base_link")
        
        return preplace,place
        
    def get_lwr_poses_rack(self,rackpose):
        """ finds the three relative poses for picking or placing in the rack at a certain pose 
        Note that the rack pose is the final pose of the gripper, so no adjustments to this pose is needed"""
        rospy.logdebug("Finding pose above rack")
        #pre-place is above and in front of the rack pose
        above_rel = gm.Pose() 
        above_rel.position.x = 0.05
        above = self.relative_pose(rackpose,above_rel,"/calib_lwr_arm_base_link",timeout=0.01)
        above = self.relative_pose(rackpose,above_rel,"/calib_lwr_arm_base_link")
        #print above
        rospy.logdebug("Finding pose above and in front of rack")
        infront_above_rel = gm.Pose()
        infront_above_rel.position.x = 0.05
        infront_above_rel.position.z = -0.26
        infront_above = self.relative_pose(rackpose,infront_above_rel,"/calib_lwr_arm_base_link")
        #print infront_above
        #post-place is in front of the rack
        rospy.logdebug("Finding pose in front of rack")
        infront_rel = gm.Pose() 
        infront_rel.position.z = -0.17
        infront = self.relative_pose(rackpose,infront_rel,"/calib_lwr_arm_base_link")
        #print infront
        return infront_above,above,infront
        
    def box_at_rack(self,boxpose):
        """ Find the rack which the box is currently at, determined based on the boxpose """
        # box location converted
        boxloc = gm.PointStamped()
        boxloc.header.frame_id = boxpose.header.frame_id
#        boxloc.header.stamp = rospy.Time.now()
        boxloc.point = boxpose.pose.position
        # call the service 
        qry_obj = qr_srv.QueryObservationsRequest()
        qry_obj.types.append("RACK")
        qry_obj.values = []
        qry_obj.location = [boxloc]
        qry_obj.search_radius = 0.06
        resp = self.qr_qry_srv(qry_obj).obs_list
        if not len(resp) == 1: 
            rospy.logwarn("None or multiple possible rack locations found")
            return False
        else:
            rospy.logdebug("The box is at 'RACK %s'",resp[0].value)
            return resp[0]
        
    
    def get_lwr_pose_place_shelf(self,shelfpose,approach=0.15):
        """
        Calculates approach and final pose of the boxgripper, from the supplied shelfpose and approach distance (above shelf)
        Returns preplace,place PoseStamped
        """
        #relative place pose
        # small box
        place_rel = gm.Pose()
        # In the height we add distance from Box QR to bottom of box, and from shelf QR to shelf plane
        place_rel.position.y =  0.06 + 0.105 +0.005# box + table + adjustment
        # In depth we use the offset from the LWR tool frame to the boxgripper and the distance in over the shelf
        place_rel.position.z =  0.075 -0.01
        p_q = tf.transformations.quaternion_from_euler(math.pi,0.0,math.pi/2.0) #rotation of gripper wrt box QR
        place_rel.orientation = gm.Quaternion(p_q[0],p_q[1],p_q[2],p_q[3])
        # get the absolute place pose
        place = self.relative_pose(shelfpose,place_rel,"/calib_lwr_arm_base_link")
        # relative preplace pose
        preplace_rel = place_rel
        preplace_rel.position.y += approach
        preplace = self.relative_pose(shelfpose,preplace_rel,"/calib_lwr_arm_base_link")
        
        return preplace,place

    def pick_up_box(self,box_color,back_up=False):
        """
        Picks up a box - it needs to be within reach of the robot, and visible for the camera.
        This will redetect the pose of the box, and pick it up.
        
        box_color: String with color of box, e.g. "RED"
        back_up: whether or not the robot should back up 30cm after lifting the box
        """
        rospy.loginfo("Will attempt to pick up a %s box", box_color)
        # Detect all QR codes in image
        found_box = False
        codes = self.detect_qr_codes(add_to_world_state=False)
        for qr in codes:
            #skip if it's not the color we're looking for or we have invalid pose data
            if not qr.data.split(".") == ["BOX", box_color.upper()]:
                continue
            #skip if it contains invalid pose data
            if qr.pose.pose==gm.Pose():
                print "Invalid pose data for box"
                continue
            qr.pose.header.stamp = rospy.Time.now()
            #print qr.pose
            boxpose = self.transform_pose("/calib_lwr_arm_base_link", qr.pose)
            #print boxpose
            found_box = True
        
        if not found_box:
            rospy.logerr("No %s box detected!", box_color)
            return 
    
        #print "Moving arm..."
        pos = self.move_arm(self.lwr_park_via,"PTP_CART_ABS",0.50)
        pos_work_grasp = np.array([-124,130,0,-119,-33,-77,10]) # For shelf and table
        pos = self.move_arm(pos_work_grasp,"PTP_JOINT_ABS",0.40)
        #print pos

        #print "Grasping box..."
        pregrasp_pose, grasp_pose = self.get_lwr_pose_pickup(boxpose)
        pos = self.move_arm(pregrasp_pose.pose,"BASE_QUAT",0.35)
        pos = self.move_arm(grasp_pose.pose,"BASE_LIN_QUAT",0.1)
        #return 0 

        # Disabled to work with the skills - the box is added after postcondition check that the box is gone
        ##add it to the gripper
        #if hasattr(self,'qr_add_srv'):
        #    self.add_box_to_gripper(box_color)
        
        #print "Lifting box..."
        #Lift
        depart = np.array([130,0,0,0,0,0])
        pos = self.move_arm(depart,"LIN_REL_TOOL",0.15)
        #Retract
        pos = self.move_arm(np.array([0,0,-150,0,0,0]), "LIN_REL_TOOL", 0.35)
        #Back up
        if back_up:
            self.back_up(0.20,0.3)
        #rospy.sleep(1.0)
        #print "Successfully picked up box!"
        pos = self.move_arm(pos_work_grasp,"PTP_JOINT_ABS",0.40)
        #print "Parking arm through via point"
        pos = self.move_arm(self.lwr_park_via,"PTP_CART_ABS",0.50)
        pos = self.move_arm(self.lwr_pos_home,"PTP_JOINT_ABS",0.50)
       
        return True

    
    def place_box_shelf(self,shelfnumber,back_up=False):
        """
        Putting down a box in the gripper at a shelf - it needs to be within reach of the robot, and visible for the camera.
        This will redetect the pose of the shelf, and place the box here.
        
        shelfnumber: String with shelf number, e.g. "1"
        back_up: whether or not the robot should back up 30cm after placing the box
        """
        rospy.loginfo("Will attempt to place a box at shelf %s",shelfnumber)
        # Detect all QR codes in image
        found_shelf = False
        codes = self.detect_qr_codes(add_to_world_state=False)
        for qr in codes:
            #skip if it's not the color we're looking for or we have invalid pose data
            if not qr.data.split(".")==["SHELF",shelfnumber.upper()] and not qr.pose.pose==gm.Pose():
                continue
            shelfpose = self.transform_pose("/calib_lwr_arm_base_link", qr.pose)
            found_shelf = True
        
        if not found_shelf:
            rospy.logerr("No shelf %s detected!",shelfnumber)
            return 
         
        #print "Moving arm..."
        pos_work_grasp = np.array([-124,130,0,-119,-33,-77,10])
        self.move_arm(pos_work_grasp,"PTP_JOINT_ABS",0.40)
    
        preplace_pose, place_pose = self.get_lwr_pose_place_shelf(shelfpose)
        
        #print "Placing the box..."
        self.move_arm(preplace_pose.pose,"BASE_LIN_QUAT",0.1)
        self.move_arm(place_pose.pose,"BASE_LIN_QUAT",0.1)
        
        #if hasattr(self,'qr_grip_srv'):
        #    self.remove_from_gripper()
            
        depart = np.array([0,0,-230,0,0,0])
        #print "Leaving box..."
        #park_via = np.array([-90,45,0,90,0,-45,90]) # TODO: Change this
        self.move_arm(depart,"LIN_REL_TOOL",0.1)

        #print "Successfully placed box!"
        
        if back_up:
            self.back_up(0.20,0.30)
        
        self.move_arm(pos_work_grasp,"PTP_JOINT_ABS",0.40)
        #print "Parking arm through via point"
        self.move_arm(self.lwr_park_via,"PTP_CART_ABS",0.50)
        self.move_arm(self.lwr_pos_home,"PTP_JOINT_ABS",0.50)
        
        return True
    
    def place_box_table(self,tablenumber,back_up=False):
        """
        Putting down a box in the gripper at a table - it needs to be within reach of the robot, and visible for the camera.
        This will redetect the pose of the table, and place the box here.
        
        tablenumber: String with table number, e.g. "1"
        back_up: whether or not the robot should back up 30cm after placing the box
        """
        rospy.loginfo("Will attempt to place a box at TABLE.%s",tablenumber)
        # Detect all QR codes in image
        found_table = False
        codes = self.detect_qr_codes(add_to_world_state=False)
        for qr in codes:
            #skip if it's not the color we're looking for or we have invalid pose data
            if not qr.data.split(".") == ["TABLE",tablenumber.upper()] and not qr.pose.pose==gm.Pose():
                continue
            tablepose = self.transform_pose("/calib_lwr_arm_base_link", qr.pose)
            found_table = True
        
        if not found_table:
            rospy.logerr("No table %s detected!",tablenumber)
            return 
         
        #print "Moving the arm..."
        pos_work_grasp = np.array([-124,130,0,-119,-33,-77,10])
        self.move_arm(pos_work_grasp,"PTP_JOINT_ABS",0.40)
    
        preplace_pose, place_pose = self.get_lwr_pose_place_table(tablepose)
        
        #print "Placing the box..."
        self.move_arm(preplace_pose.pose,"BASE_LIN_QUAT",0.1)
        self.move_arm(place_pose.pose,"BASE_LIN_QUAT",0.1)
        
        #remove box from gripper if we have world state running
        #if hasattr(self,'qr_grip_srv'):
        #    self.remove_from_gripper()
        
        depart = np.array([0,0,-230,0,0,0])
        #print "Leaving box..."
        self.move_arm(depart,"LIN_REL_TOOL",0.1)
        #print "Successfully placed box!"
        
        if back_up:
            self.back_up(0.20,0.30)
        
        self.move_arm(pos_work_grasp,"PTP_JOINT_ABS",0.40)
        #print "Parking arm through via point"
        self.move_arm(self.lwr_park_via,"PTP_CART_ABS",0.50)
        self.move_arm(self.lwr_pos_home,"PTP_JOINT_ABS",0.50)
        
        return True
    

        
    
