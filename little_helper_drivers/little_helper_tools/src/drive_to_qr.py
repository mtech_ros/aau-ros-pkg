#!/usr/bin/env python

import roslib
roslib.load_manifest("little_helper_tools")

import rospy

from skill_template import BaseSkill

import little_helper_interface

# QR World State services and messages
import qr_world_state.msg as qr_msg
import qr_world_state.srv as qr_srv


class DriveToQR(BaseSkill):

    def __init__(self):
        BaseSkill.__init__(self)
        #Add pre and postconditions
        rospy.logdebug("-- Instantiating DriveToQR skill")
        rospy.logdebug("Adding preconditions to the skill")
        self.add_precondition("Location known in world model",self.pre_location_known)
        self.add_precondition("Plan to location available",self.pre_plan_to_location)
        rospy.logdebug("Adding postconditions to the skill")
        self.add_postcondition("Platform at location",self.post_at_location)
        #Connect to ROS stuff
        self.ros_connect()
        #variables
        self._parameter = 0  # the QR ID
        self._dist = 0  #distance from QR code
        self._offset = 0  #offset from QR code
        self._approach = 0  #approach distance
        self._location = qr_msg.Observation()

    def ros_connect(self):
        #create the little helper object
        self.lhi = little_helper_interface.LittleHelperInterfaceM39(world_state=True,qr_detector=True)

    def usage(self):
        rospy.logfatal("Incorrect ID (int) of the location to drive to")
        return False

    def run_skill(self, location_id, dist, offset, approach):
        """ Runs the entire skill
        :param location_id: unique QR ID to drive to
        :param dist: distance in front of the QR code, in m
        :param offset: sideways distance offset of the QR code, in m
        :param approach: approach distance, before driving to final location, in m (distance from final position)
        """

        ### INITIAL CHECKS ###
        if not type(location_id)==int:
            return self.usage()
        self._parameter = location_id
        self._dist = dist
        self._offset = offset
        self._approach = approach

        rospy.loginfo("DriveToQr skill started, with parameters: %s, %s, %s, %s",self._parameter,self._dist,self._offset,self._approach)

        ### PRECONDITION CHECK ###
        #printout
        rospy.logdebug("DriveToQR skill preconditions (%s total):" % len(self.pre)  )
        for n in self.pre:
            n.printout("debug")
        #test them
        rospy.loginfo("--- Testing all preconditions:")
        pre = self.evaluate_all_preconditions()
        rospy.loginfo("Sum of preconditions is " + str(pre))
        if not pre==True:
            rospy.logfatal("There are failed preconditions, quitting")
            return False

        ### EXECUTE ###
        rospy.logdebug("--- Running execute() function on DriveToQR skill:")
        if not self.execute():
            rospy.logfatal("Execution failed, quitting")
            return False

        ### POSTCONDITION CHECKS ###
        #printout
        rospy.logdebug("DriveToQR skill postconditions (%s total):" % len(self.post)  )
        for n in self.post:
            n.printout("debug")
        #test them
        rospy.loginfo("--- Testing all postconditions:")
        post = self.evaluate_all_postconditions()
        rospy.loginfo("Sum of postconditions is " + str(post))
        if not post==True:
            rospy.logfatal("There are failed preconditions, quitting")
            return False

        #Hooray
        return True

    def execute(self):
        #Not really much magic in this code...
        # drive to approach
        rospy.loginfo("Driving to approach QR ID %s", self._parameter)
        if not self.lhi.drive_to_id(self._parameter, dist=self._dist+self._approach, offset=self._offset):
            rospy.logerr("Navigation failed")
            return False
        #drive to final position
        rospy.logerr("Driving to QR id %s", self._parameter)
        if not self.lhi.drive_to_id(self._parameter, dist=self._dist, offset=self._offset):
            rospy.logerr("Navigation failed")
            return False
        #yipee!
        return True

    def pre_location_known(self):
        rospy.logdebug("Testing if location is in world state")
        qr_query = qr_srv.QueryObservationsRequest()
        qr_query.qr_ids = []
        resp = self.lhi.qr_qry_srv.call(qr_query)
        qr_ids = [i.qr_id for i in resp.obs_list]
        if not self._parameter in qr_ids:
            rospy.logerr("QR ID %s not in world state, possible IDs are %s", self._parameter, qr_ids)
            return False
        else:
            loc = [i for i in resp.obs_list if i.qr_id==self._parameter]
            self._location = loc[0]
            rospy.loginfo("QR ID %s ('%s') is present in world state",self._location.qr_id,self._location.type+"."+self._location.value)
            return True

    def pre_plan_to_location(self):
        rospy.logdebug("Testing if a plan can be made to the location")
        #TODO implement this, using the nav_msgs/GetPlan service..
        return True

    def post_at_location(self):
        rospy.logdebug("Testing if a we're at the right location")
        qr_codes = self.lhi.detect_qr_codes(add_to_world_state=True)
        loc_string = self._location.type + '.' + self._location.value
        success = False
        for qr in qr_codes:
                if qr.data == loc_string:
                    qr_pose = qr.pose
                    success = True
                    break
        if not success: # did not find it
            rospy.logerr("QR code %s not detected",loc_string)
            return False
        # compare the detected distance to the QR code to the expected
        #transform to base_link
        qr_pose.header.stamp = self.lhi.tl.getLatestCommonTime("base_link", qr_pose.header.frame_id)
        qr_pose = self.lhi.transform_pose("base_link", qr_pose)
        dist_err = abs(qr_pose.pose.position.x - self._dist)
        offset_err = abs(qr_pose.pose.position.y - self._offset)
        if dist_err > 0.15 or offset_err > 0.15:  # high limits, but ok for postcondition check
            rospy.logerr("Apparently not at right location wrt. the QR code. Dist error: %s, Offset error: %s", dist_err, offset_err)
            return False
        else:
            rospy.loginfo("At right location wrt. the QR code. Dist error: %s, Offset error: %s", dist_err, offset_err)
            return True

if __name__=='__main__':
    rospy.init_node("drive_to_qr_skill",log_level=rospy.DEBUG)
    rospy.sleep(0.5)
    rospy.loginfo("==============================")
    rospy.loginfo(" Starting the DriveToQR skill")
    rospy.loginfo("==============================")
    skill = DriveToQR()
    skill.run_skill(3,1.05,0.1,0.25)
