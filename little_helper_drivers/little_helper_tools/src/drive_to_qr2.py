#!/usr/bin/env python

import roslib
roslib.load_manifest("little_helper_tools")

import rospy
import sys

from skill_template2 import BaseSkill2

import little_helper_interface

# QR World State services and messages
import qr_world_state.msg as qr_msg
import qr_world_state.srv as qr_srv


class DriveToQR2(BaseSkill2):

    def __init__(self,pout):
        BaseSkill2.__init__(self,pout)
        #Add pre and postconditions
        rospy.logdebug("-- Instantiating DriveToQR skill")
        rospy.logdebug("Adding preconditions to the skill")
        self.add_precondition("LOC_KNOWN",self.pre_location_known)
        rospy.logdebug("Adding postconditions to the skill")
        self.add_postcondition("AT_GOAL",self.post_at_location)
        #Connect to ROS stuff
        self.ros_connect()
        #variables
        self._id = 0  # the QR ID
        self._dist = 0  #distance from QR code
        self._offset = 0  #offset from QR code
        self._approach = 0  #approach distance
        self._location = qr_msg.Observation()

    def ros_connect(self):
        #create the little helper object
        self.lhi = little_helper_interface.LittleHelperInterfaceM39(world_state=True,qr_detector=True)

    def usage(self):
        rospy.logfatal("Incorrect parameter - should be list: [ID,distance,offset,approach]")
        return False

    def check_parameter(self,param):
        if not type(param)==list and len(param)==4:
            return self.usage()
        else:
            self._id = int(param[0])
            self._dist = float(param[1])
            self._offset = float(param[2])
            self._approach = float(param[3])
            return True

    def execute(self):
        #Not really much magic in this code, as it os mostyly wrapping around the navigation primitive
        # drive to approach, if specified
        if not self._approach == 0.0:
            rospy.loginfo("Driving to approach QR ID %s", self._id)
            if not self.lhi.drive_to_id(self._id, dist=self._dist+self._approach, offset=self._offset):
                return self.execution_error("Navigation failed")
        #drive to final position
        rospy.loginfo("Driving to QR id %s", self._id)
        if not self.lhi.drive_to_id(self._id, dist=self._dist, offset=self._offset):
            return self.execution_error("Navigation failed")
        #yipee!
        return True

    def pre_location_known(self):
        rospy.logdebug("Testing if location is in world state")
        qr_query = qr_srv.QueryObservationsRequest()
        qr_query.qr_ids = []
        resp = self.lhi.qr_qry_srv.call(qr_query)
        qr_ids = [i.qr_id for i in resp.obs_list]
        if not self._id in qr_ids:
            rospy.logwarn("QR ID %s not in world state, possible IDs are %s", self._id, qr_ids)
            return False
        else:
            loc = [i for i in resp.obs_list if i.qr_id==self._id]
            self._location = loc[0]
            rospy.loginfo("QR ID %s ('%s') is present in world state",self._location.qr_id,self._location.type+"."+self._location.value)
            return True

    def post_at_location(self):
        rospy.logdebug("Testing if a we're at the right location")
        qr_codes = self.lhi.detect_qr_codes(add_to_world_state=True)
        loc_string = self._location.type + '.' + self._location.value
        success = False
        for qr in qr_codes:
                if qr.data.split(".")[0:2] == [self._location.type,self._location.value]:
                    qr_pose = qr.pose
                    success = True
                    break
        if not success: # did not find it
            rospy.logerr("QR code %s not detected",loc_string)
            return False
        # compare the detected distance to the QR code to the expected
        #transform to base_link
        qr_pose = self.lhi.transform_pose("base_link", qr_pose)
        dist_err = abs(qr_pose.pose.position.x - self._dist)
        offset_err = abs(qr_pose.pose.position.y - self._offset)
        if dist_err > 0.15 or offset_err > 0.15:  # high limits, but ok for postcondition check
            rospy.logerr("Apparently not at right location wrt. the QR code. Dist error: %s, Offset error: %s", dist_err, offset_err)
            return False
        else:
            rospy.loginfo("At right location wrt. the QR code. Dist error: %s, Offset error: %s", dist_err, offset_err)
            return True

def test_pout(line):
    #only for testing the skill
    print "PIPE: " + line

if __name__=='__main__':
    rospy.init_node("drive_to_qr_skill",log_level=rospy.DEBUG)
    rospy.sleep(0.5)
    skill = DriveToQR2(test_pout)
    skill.run_skill(sys.argv[1:])
