#!/usr/bin/env python

import roslib
roslib.load_manifest("little_helper_tools")

import rospy
import sys
import numpy as np

from skill_template2 import BaseSkill2

import little_helper_interface

# QR World State services and messages
import qr_world_state.msg as qr_msg
import qr_world_state.srv as qr_srv

import geometry_msgs.msg as gm

from math import sqrt


class PlaceBox2(BaseSkill2):

    def __init__(self,pout):
        BaseSkill2.__init__(self,pout)
        #Add pre and postconditions
        rospy.logdebug("-- Instantiating PlaceBox2 skill")
        rospy.logdebug("Adding preconditions to the skill")
        self.add_precondition("LOC_KNOWN",self.pre_location_known)
        self.add_precondition("LOC_VALID", self.pre_location_is_valid)
        self.add_precondition("OBJ_IN_GRIP", self.pre_object_grasped)
        self.add_precondition("LOC_IN_REACH", self.pre_location_within_reach)

        rospy.logdebug("Adding postconditions to the skill")
        self.add_postcondition("OBJ_PLACED", self.post_object_placed)
        self.add_postcondition("GRIP_MOVED", self.post_gripper_away)
        self.add_postcondition("OBJ_GRASPED", self.post_gripper_empty)
        #Connect to ROS stuff
        self.ros_connect()

        self._final_pose = gm.PoseStamped() # the real posestamped place pose (free location to place on)
        self._location = qr_msg.Observation()  #the actual location data, copied from the world model
        self._parameter = 0  # the QR ID of the location to place on
        
    def ros_connect(self):
        #create the little helper object
        self.lhi = little_helper_interface.LittleHelperInterfaceM39(world_state=True,qr_detector=True)
        
    def usage(self):
        rospy.logfatal("One, and only one, argument must be given, which is the ID (int) of the location to place the box")
        return False

    def check_parameter(self,param):
        if not type(param)==int:
            return self.usage()
        self._parameter = param
        return True

    def execute(self):
        surface_string = self._location.type + "." + self._location.value
        rospy.loginfo("Will attempt to place a box at %s",surface_string)
        
        if self._location.type == "RACK": return self.place_in_rack()
        
        # Find the QR location
        found_location = False
        codes = self.lhi.detect_qr_codes(add_to_world_state=True)
        for qr in codes:
            #skip if it's not the location we're looking for
            if not qr.data.split(".")[0:2] == [self._location.type,self._location.value]:
                continue
            if qr.pose.pose==gm.Pose():
                rospy.logwarn("Invalid pose data for QR code: %s",qr.data)
                continue
            surface_pose = self.lhi.transform_pose("/calib_lwr_arm_base_link", qr.pose)
            found_location = True
            rospy.logdebug("Detected surface '%s'",surface_string)
            break
    
        if not found_location:
            #rospy.logwarn("Deleting '"+surface_string+"' from world model")
            #del_req = qr_srv.DestroyObservationRequest()
            #del_req.qr_id = self._location.qr_id
            #self.lhi.qr_des_srv.call(del_req)
            return self.execution_error("No location '"+surface_string+"' detected!")
     
        # Find free space on the location - this should probably be a precondition check...
        locs = self.lhi.get_place_box_locations(surface_string,surface_pose)
        found_empty = False
        for l in locs:
            if self.lhi.is_location_empty(l):
                rospy.logdebug("Found empty location on %s",surface_string)
                self._final_pose = l #save the place location
                found_empty = True
                break
        if not found_empty: 
            return self.execution_error("No empty locations on "+surface_string+"!")
        preplace_pose, place_pose = self.lhi.get_lwr_pose_place_table(self._final_pose,approach=0.150)
        #rospy.sleep(10.0)

        #print preplace_pose
        #print place_pose
        #return
        # Start execution
        rospy.loginfo("Moving arm to ready position")
        #if not self.lhi.move_arm(self.lhi.lwr_park_via,"PTP_CART_ABS",0.50).any(): return self.execution_error("Moving the LWR arm failed")
        pos_work_grasp = np.array([-124,130,0,-119,-33,-77,10]) # For shelf and table
        if not self.lhi.move_arm(pos_work_grasp,"PTP_JOINT_ABS",0.50).any(): return self.execution_error("Moving the LWR arm failed")

        preplace_pose, place_pose = self.lhi.get_lwr_pose_place_table(self._final_pose,approach=0.150)
    
        rospy.loginfo("Putting down box")
        if not self.lhi.move_arm(preplace_pose.pose,"BASE_LIN_QUAT",0.45).any(): return self.execution_error("Moving the LWR arm failed")
        if not self.lhi.move_arm(place_pose.pose,"BASE_LIN_QUAT",0.2).any(): return self.execution_error("Moving the LWR arm failed")
    
        #remove box from gripper if we have world state running
        if hasattr(self.lhi,'qr_grip_srv'):
            if not self.lhi.remove_from_gripper(): return self.execution_error("Unable to detach box from gripper")
    
        depart = np.array([0,0,-160,0,0,0])
        if not self.lhi.move_arm(depart,"LIN_REL_TOOL",0.35).any(): return self.execution_error("Moving the LWR arm failed")
        lift = np.array([140,0,0,0,0,0])
        if not self.lhi.move_arm(lift,"LIN_REL_TOOL",0.45).any(): return self.execution_error("Moving the LWR arm failed")
        #back up
        #rospy.logdebug("Backing up the robot")
        #self.lhi.back_up(0.20,0.30)
        #rospy.sleep(0.5)
        rospy.loginfo("Parking arm through via points")
        #if not self.lhi.move_arm(pos_work_grasp,"PTP_JOINT_ABS",0.40).any(): return self.execution_error("Moving the LWR arm failed")
        #if not self.lhi.move_arm(self.lhi.lwr_park_via,"PTP_CART_ABS",0.50).any(): return self.execution_error("Moving the LWR arm failed")
        if not self.lhi.move_arm(self.lhi.lwr_pos_home,"PTP_JOINT_ABS",0.50).any(): return self.execution_error("Moving the LWR arm failed")
    
        # Hooray!
        return True
        
    def place_in_rack(self):
        """ modified execute function that places a box in the rack - i.e. no sensing """
        # find free rack
        rospy.loginfo("Placing in the rack")
        rack = self.find_free_rack()
        if rack:
            self._location = rack
        else:
            return self.execution_error("No empty rack")
        # make sure we're in the right frame
        if not self._location.pose.header.frame_id == "/calib_lwr_arm_base_link":
            self._location.pose = self.lhi.transform_pose("/calib_lwr_arm_base_link",self._location.pose)
        # get the locations - in front and above, above and in front of same height
        pre1, pre2, post = self.lhi.get_lwr_poses_rack(self._location.pose)
        if False in [pre1,pre2,post]: return self.execution_error("Could not get place locations")
        rospy.loginfo("Moving arm to ready position")
        # in front and above
        if not self.lhi.move_arm(pre1.pose,"BASE_QUAT",0.45).any(): return self.execution_error("Moving the LWR arm failed")
        rospy.loginfo("Putting down box")
        # above
        if not self.lhi.move_arm(pre2.pose,"BASE_QUAT",0.35).any(): return self.execution_error("Moving the LWR arm failed")
        # place
        if not self.lhi.move_arm(self._location.pose.pose,"BASE_LIN_QUAT",0.35).any(): return self.execution_error("Moving the LWR arm failed")
        # detach the box from the gripper
        qry_obj = qr_srv.QueryObservationsRequest()
        qry_obj.types.append("BOX")
        qry_obj.in_gripper = [True] # This field is the important one
        resp = self.lhi.qr_qry_srv(qry_obj).obs_list
        if not len(resp) == 1: return self.execution_error("Tried to detach box from the gripper that wasn't there")
        box = resp[0]    
        upd_req = qr_srv.UpdatePoseRequest()
        upd_req.qr_id = box.qr_id
        upd_req.pose = self.lhi.transform_pose("/calib_lwr_arm_base_link",self._location.pose)
        upd_req.frame_lock = True # keep in static robot frame
        resp = self.lhi.qr_upd_srv.call(upd_req)
        # depart
        if not self.lhi.move_arm(post.pose,"BASE_LIN_QUAT",0.35).any(): return self.execution_error("Moving the LWR arm failed")
        rospy.loginfo("Parking the arm")
        if not self.lhi.move_arm(self.lhi.lwr_pos_home,"PTP_JOINT_ABS",0.50).any(): return self.execution_error("Moving the LWR arm failed")
        return True
    
    def find_free_rack(self):
        """ started function for finding free rack """
        rospy.loginfo("Finding a free rack")
        # get the rack locations
        qry_rack = qr_srv.QueryObservationsRequest()
        qry_rack.types = ["RACK"]
        racks = self.lhi.qr_qry_srv(qry_rack).obs_list
#        print racks
        free_rack = False
        for rack in racks:
            desired_rack = rack.qr_id == self._location.qr_id
            # determine if location is occupied:
            qry_obj = qr_srv.QueryObservationsRequest()
#            print type(rack.pose)
#            print type(qry_obj.location)
            qry_obj.types = ["BOX"]
            rack_loc = gm.PointStamped()
            rack_loc.header.frame_id = rack.pose.header.frame_id
#            rack_loc.header.stamp = rospy.Time.now()
            rack_loc.point = rack.pose.pose.position
            qry_obj.location = [rack_loc]
            qry_obj.search_radius = 0.1
            boxes = self.lhi.qr_qry_srv(qry_obj).obs_list
#            print boxes
            if len(boxes)==0:
                if desired_rack:
                    rospy.logdebug("Rack %s is free!",rack.value)
                    return rack
                else:
                    free_rack = rack               
        return free_rack
            
    def pre_location_known(self):
        rospy.logdebug("Testing if location is in world state")
        qr_query = qr_srv.QueryObservationsRequest()
        qr_query.qr_ids = []
        resp = self.lhi.qr_qry_srv.call(qr_query)
        qr_ids = [i.qr_id for i in resp.obs_list]
        if not self._parameter in qr_ids:
            rospy.logwarn("QR ID %s not in world state, possible IDs are %s",self._parameter,qr_ids)
            return False
        else:
            loc = [i for i in resp.obs_list if i.qr_id==self._parameter]
            self._location = loc[0]
            rospy.loginfo("QR ID %s ('%s') is present in world state",self._location.qr_id,self._location.type+"."+self._location.value)
            return True

    def pre_location_is_valid(self):
        rospy.logdebug("Testing if the location is a valid place location")
        if self._location.type == "BOX":
            rospy.logwarn("Location to place on is not valid, it is a %s", self._location.type)
            return False
        else:
            rospy.loginfo("Location to place on is valid, it is a %s", self._location.type)
            return True

    def pre_location_within_reach(self):
        rospy.logdebug("Testing if the location is within reach of the arm")
        #self._location.pose.header.stamp = self.lhi.tl.getLatestCommonTime("calib_lwr_arm_base_link", self._location.pose.header.frame_id)
        pos = self.lhi.transform_pose("calib_lwr_arm_base_link",self._location.pose)
        # distance to shoulder link
        dist = sqrt((pos.pose.position.x)**2 + (pos.pose.position.y)**2 + (pos.pose.position.z)**2)
        if dist > 0.95: #TODO we need better estimate of reach
            rospy.logwarn("Object is apparently out of reach (%s m from base link)",dist)
            return False
        else:
            rospy.loginfo("Object likely within workspace (%s m from base link)",dist)
            return True

    def pre_object_grasped(self):
        #TODO maybe add some stuff that check the force sensors?
        rospy.logdebug("Querying for box in gripper")
        qry_obj = qr_srv.QueryObservationsRequest()
        qry_obj.types.append("BOX")
        qry_obj.values = []
        qry_obj.location = []
        qry_obj.in_gripper = [True] # This field is the important one
        resp = self.lhi.qr_qry_srv(qry_obj).obs_list
        if not len(resp) == 1: 
            rospy.logwarn("According to the world state there is no box in the gripper!")
            return False
        else:
            rospy.loginfo("%s.%s is currently in the gripper",resp[0].type,resp[0].value)
            return True

    def post_object_placed(self):
        rospy.logdebug("Testing if a box is present at put down location")
        if self._location.type == "RACK":
            rospy.loginfo("Place location is a RACK, assuming that it was placed correctly")
            return True
        qr_codes = self.lhi.detect_qr_codes(add_to_world_state=True)
        loc_string = self._location.type + '.' + self._location.value
        success = False
        place_pose = self.lhi.transform_pose("/camera_rgb_optical_frame",self._final_pose)
        for qr in qr_codes:
            #is it a box?
            if qr.data.split(".")[0]=="BOX":
                # is it on table/shelf? (10cm from vertical axis, x in camera_rgb_optical_frame)
                rospy.logdebug("Found BOX, x dist: %s", abs(qr.pose.pose.position.x-place_pose.pose.position.x))
                if abs(qr.pose.pose.position.x-place_pose.pose.position.x) < 0.10:
                    rospy.logdebug("Found %s on %s", qr.data, loc_string)
                    success = True
                    break
        if success:
            rospy.loginfo("Detected a box on the empty location on %s",loc_string)
        else:
            rospy.logwarn("No box found on the empty location on %s",loc_string)
        return success

    def post_gripper_away(self):
        rospy.logdebug("Testing if the gripper is away from the place location")
        pos = self.lhi.transform_pose("/box_gripper",self._location.pose)
        # distance to shoulder link
        dist = sqrt((pos.pose.position.x)**2 + (pos.pose.position.y)**2 + (pos.pose.position.z)**2)
        if dist < 0.1:
            rospy.logwarn("Gripper is apparently too close to the place location (%s m)",dist)
            return False
        else:
            rospy.loginfo("Gripper is away from the place location (%s m)",dist)
            return True

    def post_gripper_empty(self):
        #TODO maybe add some stuff that check the force sensors?
        rospy.loginfo("Assuming the gripper is empty....")
        return True
        

def test_pout(line):
    #only for testing the skill
    print "==== PIPE: " + line

if __name__=='__main__':
    rospy.init_node("place_box_skill",log_level=rospy.DEBUG)
    rospy.sleep(0.5)
    skill = PlaceBox2(test_pout)
    skill.run_skill(int(sys.argv[1]))
